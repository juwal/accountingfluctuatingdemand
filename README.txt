To open a file with ".nlogo", download NetLogo software from the following link:



https://ccl.northwestern.edu/netlogo/download.shtml



The model was developed with NetLogo version 5.2, the use of an earlier or later version may lead to malfunction.



A complete description of the model and its files are available in the "Info" tab of the NetLogo model.


The optimization model needs the Gurobi solver to function. The solver can bedownloaded  from the following link:



http://www.gurobi.com/index