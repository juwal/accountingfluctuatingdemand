; Please write to: julien.walzberg@polymtl.ca with any questions or comments.

; Abbreviations:
;  -Electricity = elec
;  -Behavior(s) = bhvr
;  -Rebound effect = RE
;  -Subject to = s.t.
;  -Stove and oven = s&o, fridge = ref, freezer = fre, dishwasher = dish,
;    clothe washer = cwah, dryer = dry, other load = otha, lightnings = light,
;    surface heating = sheat, water heating = wheat, surface cooling = scool.
;  -ht = human toxicity (line 0), re = respiratory effect (line 1),
;    ir = ionizing radiation (line 2), old = ozone layer depletion (line 3),
;    po = photochemical oxidation (line 4), ae = aquatic ecotoxicity (line 5),
;    te = terrestrial ecotoxicity (line 6),
;    tan = terrestrial acidification, nutrification (line 7),
;    lo = land occupation (line 8), cc = climate change (line 9),
;    nre = non-renewable energy (line 10), rme = raw material extraction (line 11),
;    hh = human health (line 12), eq = ecosystem quality (line 13),
;    res = ressource (line 14), ss = single score (line 15)
;  - O = optimization

;-----------------------------------------------------------------------------------;

extensions [ matrix csv ]

; Globals variables: inputs data, outputs reporters (impacts reporters etc.),
globals [ nt nd nw ny %acc hour_of_day hourly_cost hourly_cost2 costbis impact_consum
ht_impactbis ht_default_impactbis ht_default_impact_building
re_impactbis re_default_impactbis re_default_impact_building ir_impactbis
ir_default_impactbis ir_default_impact_building old_impactbis old_default_impactbis
old_default_impact_building po_impactbis po_default_impactbis
po_default_impact_building ae_impactbis ae_default_impactbis
ae_default_impact_building te_impactbis te_default_impactbis
te_default_impact_building tan_impactbis tan_default_impactbis
tan_default_impact_building lo_impactbis lo_default_impactbis
lo_default_impact_building cc_impactbis cc_default_impactbis
cc_default_impact_building nre_impactbis nre_default_impactbis
nre_default_impact_building rme_impactbis rme_default_impactbis
rme_default_impact_building hh_impactbis hh_default_impactbis
hh_default_impact_building eq_impactbis eq_default_impactbis
eq_default_impact_building res_impactbis res_default_impactbis
res_default_impact_building ss_impactbis ss_default_impactbis
ss_default_impact_building ht_impact_tot re_impact_tot old_impact_tot po_impact_tot
ae_impact_tot te_impact_tot tan_impact_tot cc_impact_tot hh_impact_tot eq_impact_tot
ss_impact_tot hh_impact_tot2 eq_impact_tot2 ss_impact_tot2
ht_impact_tot2 re_impact_tot2 old_impact_tot2 po_impact_tot2
ae_impact_tot2 te_impact_tot2 tan_impact_tot2 cc_impact_tot2
bass_p show_link? nticks tot_hdegree-day tot_cdegree-day
tot_cdegree-day_toronto tot_hdegree-day_toronto HDD_% CDD_% costbis2
electric_consumption electric_consumption2 min_cons min_cost min_impact
p_bhvr_winter p_bhvr_summer bhvr_matrix hourly_SB_cons cumul_hourly_SB_cons
hourly_SB_cost cumul_hourly_SB_cost hourly_SB_impact cumul_hourly_SB_impact
att_discrepancy diff_discrepancy P_sbhvr_avg shift SB_default_cons SB_default_cost
SB_default_impact_ht SB_default_impact_re SB_default_impact_ir SB_default_impact_old
SB_default_impact_po SB_default_impact_ae SB_default_impact_te SB_default_impact_tan
SB_default_impact_lo SB_default_impact_cc SB_default_impact_nre
SB_default_impact_rme SB_default_impact_hh SB_default_impact_eq
SB_default_impact_res SB_default_impact_ss SB_cons SB_cost
SB_impact_ht SB_impact_re SB_impact_ir SB_impact_old SB_impact_po
SB_impact_ae SB_impact_te SB_impact_tan SB_impact_lo SB_impact_cc SB_impact_nre
SB_impact_rme SB_impact_hh SB_impact_eq SB_impact_res SB_impact_ss
SB_RE_impact_ht SB_RE_impact_re SB_RE_impact_old SB_RE_impact_po
SB_RE_impact_ae SB_RE_impact_te SB_RE_impact_tan SB_RE_impact_cc
SB_RE_impact_hh SB_RE_impact_eq SB_RE_impact_ss hh_app_distribution
daily_wd_freq daily_we_freq hourly_proba yellowguy dep_app e_peak i_peak c_peak
re_peak max_delay global_econst global_default_econst ht_passive0_impacttot
ht_passive0_impact ht_passive0_defaultimpact passive0_econst passive0_defaulteconst
passive0_freqcons passive0_defaultfreqcons re_passive0_impacttot re_passive0_impact
re_passive0_defaultimpact ir_passive0_impact ir_passive0_defaultimpact
old_passive0_impacttot old_passive0_impact old_passive0_defaultimpact
po_passive0_impacttot po_passive0_impact po_passive0_defaultimpact
ae_passive0_impacttot ae_passive0_impact ae_passive0_defaultimpact
te_passive0_impacttot te_passive0_impact te_passive0_defaultimpact
tan_passive0_impacttot tan_passive0_impact tan_passive0_defaultimpact
lo_passive0_impact lo_passive0_defaultimpact cc_passive0_impacttot
cc_passive0_impact cc_passive0_defaultimpact nre_passive0_impact
nre_passive0_defaultimpact rme_passive0_impact rme_passive0_defaultimpact
ht_yellowguy_impacttot ht_yellowguy_impact ht_yellowguy_defaultimpact
yellowguy_econst yellowguy_defaulteconst yellowguy_freqcons
yellowguy_defaultfreqcons re_yellowguy_impacttot re_yellowguy_impact
re_yellowguy_defaultimpact ir_yellowguy_impact ir_yellowguy_defaultimpact
old_yellowguy_impacttot old_yellowguy_impact old_yellowguy_defaultimpact
po_yellowguy_impacttot po_yellowguy_impact po_yellowguy_defaultimpact
ae_yellowguy_impacttot ae_yellowguy_impact ae_yellowguy_defaultimpact
te_yellowguy_impacttot te_yellowguy_impact te_yellowguy_defaultimpact
tan_yellowguy_impacttot tan_yellowguy_impact tan_yellowguy_defaultimpact
lo_yellowguy_impact lo_yellowguy_defaultimpact cc_yellowguy_impacttot
cc_yellowguy_impact cc_yellowguy_defaultimpact nre_yellowguy_impact
nre_yellowguy_defaultimpact rme_yellowguy_impact rme_yellowguy_defaultimpact
%passives %stalwarts %epicures %frugals mix_ontario ef_mix tech_matrix
xf_impact2002 xf_endpoint_impact2002 single_score_impact2002 functional_unit
t_daily_lcia t_daily_lcia_hh t_daily_lcia_eq t_daily_lcia_res t_daily_lcia_ss
i_factor_value number-rewired clustering-coefficient infinity connex?
average-path-length random_eq_clustering-coefficient random_eq_average-path-length
small_world_coeff neighbor_distrib lattice_eq_clustering-coefficient
lattice_eq_average-path-length w_smallworld_coeff feedback_frequency_value
r_tot_edges lat_tot_edges g_tot_edges dont_go? number_edges n_type_appliances
time_simul PV_output PV_daily_prod microfit_price cumul_lci_vector
cumul_lcia_vector cumul_detailed_lci xf_matrix cumul_detailed_lcia
xf_impact2002_lcia detailed_lcia_cc detailed_lcia_hh detailed_lcia_eq
detailed_lcia_res default_cc_art3 xf_avrg_art3 ]

; Household agent types are taken from (Valocchi 1007):
; Passive Ratepayers – Consumers who are relatively uninvolved with decisions related
; to energy usage and uninterested in taking (or unable to take)
; responsibility for these decisions
breed [ passives passive ]

; Frugal Goal Seekers – Consumers who are willing to take modest action to address
; specific goals or needs related to energy usage, but are constrained
; in what they are able to do because disposable income is limited
breed [ frugals frugal ]

; Energy Epicures – High-usage consumers who have little or no desire for
; conservation or active? involvement in energy control;
; these consumers are more likely to own a large number of high-consumption devices
; for gaming, computing or entertainment
breed [ epicures epicure ]

; Energy Stalwarts – Consumers who have specific goals or needs related to energy
; usage and have both the income and desire to act on those goals.
breed [ stalwarts stalwart ]

; Processes agent are directly taken from ecoinvent and represent system elementary
; processes (not unit) (see info tab for more information)
breed [ processes process ]

; Directed links "economic_flows" represent output economic flows of processes
directed-link-breed [ economic_flows economic_flow ]

; Undirected links "neighbor_links" represent relationships between households
; Network can be tuned by user but using the random-gamma primitive to draw each
; agent's number of neighbors is believed to give close to real-world social network
; (with both scale-free and small-world characteristics)
undirected-link-breed [ neighbor_links neighbor_link ]

; All households agents variables, breeds:
passives-own [ ht_h_impact ht_default_impact re_h_impact re_default_impact
ir_h_impact ir_default_impact old_h_impact old_default_impact po_h_impact
po_default_impact ae_h_impact ae_default_impact te_h_impact te_default_impact
tan_h_impact tan_default_impact lo_h_impact lo_default_impact cc_h_impact
cc_default_impact nre_h_impact nre_default_impact rme_h_impact rme_default_impact
n_bhvr_max ]

frugals-own [ ht_h_impact ht_default_impact re_h_impact re_default_impact
ir_h_impact ir_default_impact old_h_impact old_default_impact po_h_impact
po_default_impact ae_h_impact ae_default_impact te_h_impact te_default_impact
tan_h_impact tan_default_impact lo_h_impact lo_default_impact cc_h_impact
cc_default_impact nre_h_impact nre_default_impact rme_h_impact rme_default_impact
n_bhvr_max ]

epicures-own [ ht_h_impact ht_default_impact re_h_impact re_default_impact
ir_h_impact ir_default_impact old_h_impact old_default_impact po_h_impact
po_default_impact ae_h_impact ae_default_impact te_h_impact te_default_impact
tan_h_impact tan_default_impact lo_h_impact lo_default_impact cc_h_impact
cc_default_impact nre_h_impact nre_default_impact rme_h_impact rme_default_impact
n_bhvr_max ]

stalwarts-own [ ht_h_impact ht_default_impact re_h_impact re_default_impact
ir_h_impact ir_default_impact old_h_impact old_default_impact po_h_impact
po_default_impact ae_h_impact ae_default_impact te_h_impact te_default_impact
tan_h_impact tan_default_impact lo_h_impact lo_default_impact cc_h_impact
cc_default_impact nre_h_impact nre_default_impact rme_h_impact rme_default_impact
n_bhvr_max ]

processes-own [ process_id grid_recipe elementary_flows ]

; For all turtles:
turtles-own [ default_econs ht_live_impact_tot re_live_impact_tot
old_live_impact_tot po_live_impact_tot ae_live_impact_tot te_live_impact_tot
tan_live_impact_tot cc_live_impact_tot hh_live_impact_tot eq_live_impact_tot
ss_live_impact_tot ht_live_rebound_tot re_live_rebound_tot old_live_rebound_tot
po_live_rebound_tot ae_live_rebound_tot te_live_rebound_tot tan_live_rebound_tot
cc_live_rebound_tot impact_reduction rebound default_impact live_default_cost
savings freqcons econst consum cost default_cost ht_live_impact
ht_live_default_impact re_live_impact re_live_default_impact ir_live_impact
ir_live_default_impact old_live_impact old_live_default_impact po_live_impact
po_live_default_impact ae_live_impact ae_live_default_impact te_live_impact
te_live_default_impact tan_live_impact tan_live_default_impact lo_live_impact
lo_live_default_impact cc_live_impact cc_live_default_impact nre_live_impact
nre_live_default_impact rme_live_impact rme_live_default_impact hh_live_impact
hh_live_default_impact eq_live_impact eq_live_default_impact res_live_impact
res_live_default_impact ss_live_impact ss_live_default_impact live_cost
default_freqcons live_econs live_default_econs n_bhvr active? cumul_cons
P_sbhvr cumul_cost cumul_impact bhvr_index bhvr_index2 bhvr1 bhvr2 bhvr3 bhvr4 bhvr5
give_up? h_smarthome_cons cumul_h_smarthome_cons appliances appliances_default
appliances_default2 shift_bhvr econs_inter econs_inter_default h_impact_inter
h_impact_inter_default cost_inter cost_inter_default re_impact_inter
re_impact_inter_default bhvr_sheat bhvr_scool turtle_vision degree_node
node-clustering-coefficient distance-from-other-turtles PV_surplus
default_PV_surplus live_econs_lci ]

; Variables for links:
; Big part of Network construction procedures are taken from the small-World model
; (see below) in the NetLogo Library. Reference of the model: "Wilensky, U. (2005).
; NetLogo Small Worlds model. http://ccl.northwestern.edu/netlogo/models/SmallWorlds.
; Center for Connected Learning and Computer-Based Modeling, Northwestern University,
; Evanston, IL.
neighbor_links-own[ rewired? ]

to setup ; Initialize model
reset-timer
clear-all
reset-ticks
if random_seed? [ random-seed 190413 ]
if P_sbhvr_stalwarts = 1 [ set P_sbhvr_stalwarts 0.99999 ]
if not agent_decision_process? [ set P_sbhvr_stalwarts 0.99999 set P_conform 0
set feedback_frequency 1 ] ; Allows to unbride model from agents' decision process
if feedback_frequency = 1 [ set feedback_frequency_value 1 ] ; hourly feedback
if feedback_frequency = 2 [ set feedback_frequency_value 24 ] ; daily feedback
if feedback_frequency = 3 [ set feedback_frequency_value 168 ] ; weekly feedback
if feedback_frequency = 4 [ set feedback_frequency_value 720 ] ; monthly feedback
set infinity 999999
set connex? false
set dont_go? false
create_turtles
if K_rgraph > (nt - 1) [ set K_rgraph (nt - 1) ]
social-network
if agent_decision_process? [
  initial_innovators
  ask one-of turtles with [ color = yellow ][ set yellowguy who ] ]
setup_sb_model
set hour_of_day 0
set nd 0
set nw 0
set ny 0
if timer > 2000 [
  set gamma_alpha 1
  set gamma_lambda 0.05
  setup ] ; Setup procedure loop: simulation restart w/ standard gamma parameters
end

to go
ifelse dont_go? [ setup ][ ; If the Network is not well defined
  set nticks ticks         ; simulation restart
  if ticks = 0 [
    temperature_cost_PV
    setup_freqcons_lcia_loadshift
    tick
    set nticks ticks ] ; Prepare the simulation
; Account for the number of days, number of years:
  set nd int (nticks / 24)
  set nw int (nticks / 168)
  if nd = 30 [ set time_simul timer ]
  if nd = 365 [ STOP ]
; Standard simulation done over a year (input data collected for 1 year)
  social_interactions
  electric_consumption_profile
  report_outputs
  temperature_cost_PV
  week_end
  setup_freqcons_lcia_loadshift
  tick ] ; Add up one tick at the end of the procedure
end

; PROCEDURE CALLED IN SETUP (IN CALLING ORDER)
to create_turtles ; Create households agents
if proportion_agent_type = 1 [  ; USA (Ontario)
 set %passives 45
 set %stalwarts 27
 set %epicures 14
 set %frugals 14 ]
if proportion_agent_type = 2 [  ; Japan
 set %passives 40
 set %stalwarts 54
 set %epicures 3
 set %frugals 3 ]
if proportion_agent_type = 3 [  ; UK
 set %passives 56
 set %stalwarts 22
 set %epicures 11
 set %frugals 11 ]
; Distributions of the population are coming from (Valocchi 2007) (see info tab)
set att_discrepancy 0.71
set  P_sbhvr_avg (((P_sbhvr_stalwarts / (1 - P_sbhvr_stalwarts))
  * exp (-1 * att_discrepancy)) / (((P_sbhvr_stalwarts / (1 - P_sbhvr_stalwarts))
  * exp (-1 * att_discrepancy)) + 1)) ; From Kaiser et al (2010) (see info tab)
create-passives (%passives * turtle_multiplier) [
  set color blue
  set size 0.5
  set shape "person"
  set xcor random-xcor
  set ycor random-ycor
  ifelse agent_decision_process? [ set n_bhvr 0 ][ set n_bhvr 1 ]
  ifelse agent_decision_process? [ set n_bhvr_max (n_bhvr_epicures + 1) ]
    [ set n_bhvr_max 5 ]
  set P_sbhvr P_sbhvr_avg ; From Kaiser et al (2010)
  ifelse agent_decision_process? [ set active? false ][ set active? true ]
  set give_up? false ]
create-frugals (%frugals * turtle_multiplier) [
  set color magenta
  set size 0.5
  set shape "person"
  set xcor random-xcor
  set ycor random-ycor
  ifelse agent_decision_process? [ set n_bhvr 0 ][ set n_bhvr 1 ]
  ifelse agent_decision_process? [ set n_bhvr_max (n_bhvr_epicures + 1) ]
    [ set n_bhvr_max 5 ]
  set P_sbhvr P_sbhvr_stalwarts
  ifelse agent_decision_process? [ set active? false ][ set active? true ]
  set give_up? false ]
create-epicures (%epicures * turtle_multiplier) [
  set color (brown + 1)
  set size 0.5
  set shape "person"
  set xcor random-xcor
  set ycor random-ycor
  ifelse agent_decision_process? [ set n_bhvr 0 ][ set n_bhvr 1 ]
  ifelse agent_decision_process? [ set n_bhvr_max n_bhvr_epicures ]
    [ set n_bhvr_max 5 ]
  set P_sbhvr P_sbhvr_avg
  ifelse agent_decision_process? [ set active? false ][ set active? true ]
  set give_up? false ]
create-stalwarts (%stalwarts * turtle_multiplier) [
  set color green
  set size 0.5
  set shape "person"
  set xcor random-xcor
  set ycor random-ycor
  ifelse agent_decision_process? [ set n_bhvr 0 ][ set n_bhvr 1 ]
  ifelse agent_decision_process? [ set n_bhvr_max (n_bhvr_epicures + 2) ]
    [ set n_bhvr_max 5 ]
  set P_sbhvr P_sbhvr_stalwarts
  ifelse agent_decision_process? [ set active? false ][ set active? true ]
  set give_up? false ]
  set nt count turtles with [ color != grey ]
end

to social-network ; Social network represent the number of relationshsips per agent
let i 0           ; see info tab for more information
let j 0
let k 0
while [ k < nt ][ ; Place the household agents in the "world"
  ask turtle k [
    set xcor i
    set ycor j ]
  set i (i + 1)
  set k (k + 1)
  if ((i / 32) = int (i / 32) AND (i != 0)) [ set j (j - 1) set i 0 ] ]
; The Network can be built according to the Watts-Strogatz algorithm or simply using
; the random-gamma primitive. For both, some Network Characteristics are computed
; (e.g. small-world coefficients: s=(C/C_rand)/(L/L_rand), w=(L_rand/L)-(C/C_latt))
set neighbor_distrib [  ]
set w_smallworld_coeff 15
ifelse gamma_distrib? [
  ask neighbor_links [ die ]
  let list_rg_number [  ]
  while [ sum list_rg_number < nt * 2 ][
    let a 0
    set list_rg_number [  ]
    while [ a < nt ][
      let dice-gamma random-gamma gamma_alpha gamma_lambda
      while [ dice-gamma > nt ] [
        set dice-gamma random-gamma gamma_alpha gamma_lambda ]
      set list_rg_number lput dice-gamma list_rg_number
      set a (a + 1) ] ]
  ask turtles with [ color != grey ][
    create-neighbor_link-with one-of other turtles with [ color != grey ]
    let n_neighbors count neighbor_link-neighbors
    create-neighbor_links-with n-of max (list 0
    (item who list_rg_number - n_neighbors)) other turtles with [ color != grey ]
    set n_neighbors count neighbor_link-neighbors ]
  let copy_rewiring_prob rewiring_prob
  set rewiring_prob 1
  let loop_protection2 0
  while [ not connex? ][
    rewire_procedure ; Build equivalent random network to calculate s and w coeff
    set loop_protection2 (loop_protection2 + 1)
    if loop_protection2 > 20 [ set dont_go? true
    output-print "Random equivalent not connected, consider not using results"
      stop ] ]
  let avrg_neigh [  ]
  ask turtles with [ color != grey ] [ set avrg_neigh
    lput (count neighbor_link-neighbors) avrg_neigh ]
  set avrg_neigh (sum (avrg_neigh) / nt)
  while [ avrg_neigh mod 2 != 0 ][
    set avrg_neigh [  ]
    ask  one-of turtles with [ color != grey ]
      [ create-neighbor_link-with one-of other turtles with [ color != grey ] ]
    ask turtles with [ color != grey ] [
     set avrg_neigh lput (count neighbor_link-neighbors) avrg_neigh ]
    set avrg_neigh (sum (avrg_neigh) / nt) ]
  set r_tot_edges count neighbor_links
  find-path-lengths
  let num-connected-pairs sum [ length remove infinity
    (remove 0 distance-from-other-turtles) ] of turtles
  set average-path-length (sum [ sum distance-from-other-turtles ] of turtles)
    / (num-connected-pairs)
  find-clustering-coefficient
  set random_eq_clustering-coefficient clustering-coefficient
  set random_eq_average-path-length average-path-length
  ask neighbor_links [ die ]
  set K_rgraph (r_tot_edges / nt)
  set K_rgraph (K_rgraph * 2)
  set number_edges r_tot_edges
  wire-them ; Build equivalent regular lattice to calculate s and w coeff
  let esp_sq [  ]
  ask turtles with [ color != grey ][ set esp_sq lput (count neighbor_link-neighbors)
    esp_sq ]
  let esperance (sum esp_sq / nt)^ 2
  set esp_sq (sum (map * esp_sq esp_sq) / nt)
  ifelse esp_sq - esperance = 0 [ output-print "Regular lattice successfully built" ]
    [ output-print "Regular lattice not successfully built" ]
  set lat_tot_edges count neighbor_links
  find-path-lengths
  set num-connected-pairs sum [ length remove infinity
    (remove 0 distance-from-other-turtles) ] of turtles
  set average-path-length (sum [ sum distance-from-other-turtles ] of turtles)
    / (num-connected-pairs)
  find-clustering-coefficient
  set lattice_eq_clustering-coefficient clustering-coefficient
  set lattice_eq_average-path-length average-path-length
  ask neighbor_links [ die ]
  set rewiring_prob copy_rewiring_prob
  let copy_avrg_neigh avrg_neigh
  set connex? false
  let loop_protection3 0
  while [ not connex? ][
    ask turtles with [ color != grey ][
      let n_neighbors count neighbor_link-neighbors
      create-neighbor_links-with n-of max (list 0
      (item who list_rg_number - n_neighbors)) other turtles with [ color != grey ]
      set n_neighbors count neighbor_link-neighbors ]
    set avrg_neigh [  ]
    ask turtles with [ color != grey ] [ set avrg_neigh lput
    (count neighbor_link-neighbors) avrg_neigh ]
    set avrg_neigh (sum (avrg_neigh) / nt)
    set g_tot_edges count neighbor_links
    while [ g_tot_edges != r_tot_edges ][
    ifelse g_tot_edges > r_tot_edges [ ask n-of (g_tot_edges - r_tot_edges)
    neighbor_links [ die ] set g_tot_edges count neighbor_links ][
      ask  n-of min (list nt (r_tot_edges - g_tot_edges))
      turtles with [ color != grey ]
      [ create-neighbor_link-with one-of other turtles with [ color != grey ] ] ]
    set g_tot_edges count neighbor_links ]
    graph_connex??
    set loop_protection3 (loop_protection3 + 1)
    if loop_protection3 > 20 [ set dont_go? true
    output-print "Gamma network not connected, consider not using results"
      stop ] ] ; Build the network with the gamma-primitive
  find-path-lengths
  set num-connected-pairs sum [ length remove infinity
    (remove 0 distance-from-other-turtles) ] of turtles
  set average-path-length (sum [ sum distance-from-other-turtles ] of turtles)
    / (num-connected-pairs)
  find-clustering-coefficient
  if connex? [ set small_world_coeff (clustering-coefficient
    / random_eq_clustering-coefficient) / (average-path-length
    / random_eq_average-path-length)
    set w_smallworld_coeff ((random_eq_average-path-length / average-path-length) -
    (clustering-coefficient / lattice_eq_clustering-coefficient)) ]
  ifelse small_world_coeff > 1 and ln nt < number_edges and
  (number_edges * 2) < nt ^ 2
    [ output-print "The network is a small world" ][ output-print
    "The network is not a small world" ]
  if w_smallworld_coeff > 0 and ln nt < number_edges and
  (number_edges * 2) < nt ^ 2 [
    ifelse w_smallworld_coeff > 0.5
      [ output-print "The network is close to a random network" ]
      [ output-print "The network has short path lengths" ] ]
  if w_smallworld_coeff < 0 and ln nt < number_edges and
  (number_edges * 2) < nt ^ 2 [
    ifelse w_smallworld_coeff < -0.5
      [ output-print "The network is close to a regular network" ]
      [ output-print "The network has high clustering coefficient" ] ]
  if w_smallworld_coeff > 1 or w_smallworld_coeff < -1 [ set dont_go? true ] ][
  ifelse K_rgraph > (nt / 2) [
     ask turtles with [color != grey] [
       create-neighbor_links-with n-of (count turtles with [ color != grey ] - 1)
       other turtles with [ color != grey ] ] ][
; Shorter way to get bigger graph when we know they cannot qualify as small-world
    set number_edges (K_rgraph * nt)
    let copy_rewiring_prob rewiring_prob
    ifelse SmallWorldNetwork? and K_rgraph > 3 [ ; K_rgraph = 10 and
      let compteur 0                             ; rewiring_prob = 0.5 work well
      while [ small_world_coeff <= 1  and compteur < 5 ][
        set rewiring_prob 1
        network_charasteristics
        set random_eq_clustering-coefficient clustering-coefficient
        set random_eq_average-path-length average-path-length
        set rewiring_prob copy_rewiring_prob
        ask neighbor_links [ die ]
        network_charasteristics
        if connex? [ set small_world_coeff (clustering-coefficient
          / random_eq_clustering-coefficient)
          / (average-path-length / random_eq_average-path-length) ]
        ifelse small_world_coeff > 1 [ output-print "The network is a small world" ]
          [ output-print "The network is not a small world" ]
        set compteur (compteur + 1) ] ][
      set rewiring_prob 1
      network_charasteristics ; Build equivalent random network to calculate s coeff
      set random_eq_clustering-coefficient clustering-coefficient
      set random_eq_average-path-length average-path-length
      set rewiring_prob copy_rewiring_prob
      ask neighbor_links [ die ]
      network_charasteristics ]
    if connex? [ set small_world_coeff (clustering-coefficient
        / random_eq_clustering-coefficient) / (average-path-length
       / random_eq_average-path-length)
      set w_smallworld_coeff ((random_eq_average-path-length / average-path-length) -
      (clustering-coefficient / lattice_eq_clustering-coefficient)) ]
    ifelse small_world_coeff > 1 and ln nt < number_edges and
    (number_edges * 2) < nt ^ 2
      [ output-print "The network is a small world" ][ output-print
      "The network is not a small world" ]
    if w_smallworld_coeff > 0 and ln nt < number_edges and
    (number_edges * 2) < nt ^ 2 [
      ifelse w_smallworld_coeff > 0.5
        [ output-print "The network is close to a random network" ]
        [ output-print "The network has short path lengths" ] ]
    if w_smallworld_coeff < 0 and ln nt < number_edges and
    (number_edges * 2) < nt ^ 2 [
      ifelse w_smallworld_coeff < -0.5
        [ output-print "The network is close to a regular network" ]
        [ output-print "The network has high clustering coefficient" ] ]
    if w_smallworld_coeff > 1 or w_smallworld_coeff < -1 [ set dont_go? true ] ] ]
; Build network with the Watts-Strogatz algorithm
ask turtles with [ color != grey ][ set show_link? true
  set neighbor_distrib lput (count neighbor_link-neighbors) neighbor_distrib ]
end

to rewire_procedure ; Wilensky, U. (2005). NetLogo Small Worlds model.
ask neighbor_links [ set rewired? false ]
set number-rewired 0
ask neighbor_links [
  if random-float 1 < rewiring_prob [
    let node1 end1
    if [ count neighbor_link-neighbors ] of end1 < (nt - 1) [
      let node2 one-of turtles with [ color != grey and (self != node1)
      and (not neighbor_link-neighbor? node1) ]
      ask node1 [ create-neighbor_link-with node2 [ set color (pink - 3)
      set rewired? true ] ]
      set rewired? true ] ]
  if rewired? [ die ] ]
graph_connex??
end

to find-path-lengths ; Wilensky, U. (2005). NetLogo Small Worlds model.
                     ; (Floyd Warshall algorithm)
ask turtles with [ color != grey ]       ; Reset the distance list
[ set distance-from-other-turtles [  ] ] ; distance lists
let i 0
let j 0
let k 0
let node1 one-of turtles with [ color != grey ]
let node2 one-of turtles with [ color != grey ]
let node-count nt
while [ i < node-count ][    ; Initialize the distance lists
  set j 0
  while [ j < node-count ][
  set node1 turtle i
  set node2 turtle j
  ifelse (i = j) [  ; Zero from a node to itself
    ask node1 [ set distance-from-other-turtles lput 0
      distance-from-other-turtles ] ][
    ifelse ([ neighbor_link-neighbor? node1 ] of node2) [
      ask node1 [ set distance-from-other-turtles lput 1
        distance-from-other-turtles ] ][  ; Distance = 1 from a node to its
                                          ; neighbor (unweighted network)
      ask node1 [ set distance-from-other-turtles lput infinity
        distance-from-other-turtles ] ] ] ; Distance = infinite to everyone else
  set j j + 1 ]
  set i i + 1 ]
set i 0
set j 0
let dummy 0
while [ k < node-count ][
  set i 0
  while [ i < node-count ][
    set j 0
    while [ j < node-count ][
    set dummy ((item k [ distance-from-other-turtles ] of turtle i) +
        (item j [ distance-from-other-turtles ] of turtle k))
        ; Alternate path length
    if dummy < (item j [ distance-from-other-turtles ] of turtle i) [
              ; Is the alternate path shorter?
      ask turtle i [ set distance-from-other-turtles replace-item j
      distance-from-other-turtles dummy ] ]
  set j j + 1 ]
  set i i + 1 ]
set k k + 1 ]
end

to find-clustering-coefficient ; Wilensky, U. (2005). NetLogo Small Worlds model.
ifelse all? turtles with [ color != grey ] [ count neighbor_link-neighbors <= 1 ]
  [ set clustering-coefficient 0 ][  ; (Clustering coefficient undefined)
  let total 0
  ask turtles with [ color != grey and count neighbor_link-neighbors <= 1 ]
    [ set node-clustering-coefficient "undefined" ]
  ask turtles with [ color != grey and count neighbor_link-neighbors > 1 ][
    let hood neighbor_link-neighbors
    set node-clustering-coefficient (2 * count neighbor_links with
    [ in-neighborhood? hood ] / ((count hood) * (count hood - 1)))
    set total total + node-clustering-coefficient ]
  set clustering-coefficient (total / count turtles with
    [ color != grey and count link-neighbors > 1 ]) ] ; Average clustering coeff
end

to wire-them
  ; Wilensky, U. (2005). NetLogo Small Worlds model.
  ; Iterate over total number of links desired divided by number of turtles
let i 1
while [ i < (number_edges / (nt - 1)) ][
  let n 0   ; Iterate over the turtles
  while [ n < nt ] [
  ; Make one edge with the next neighbor
    let target_neighbor (n + i) mod nt
    ask turtle n [ create-neighbor_link-with turtle target_neighbor ]
    set n (n + 1) ]
  set i (i + 1) ]
end

to graph_connex??
ask turtle 0 [ set color white ]
let z2 0
while [ z2 < (2 * (turtle_multiplier * (%passives + %frugals + %stalwarts
  + %epicures))) ][
  ask turtles with [ color != grey and color != white ][
    let condition_color count neighbor_link-neighbors with [ color = white ]
    if condition_color > 0 [ set color white ] ]
  set z2 (z2 + 1) ]
ifelse count turtles with [ color = white ] = nt [ output-print "The graph is connex"
  set connex? true ][ output-print "The network is not connex" ]
ask passives [ set color blue ]  ; Simple "viral" procedure  to test connectivity
ask frugals [ set color magenta ]
ask epicures [ set color (brown + 1) ]
ask stalwarts [ set color green ]
end

to network_charasteristics
ifelse K_rgraph < 4 [ rewire_procedure ][
  set connex? false
  while [ not connex? ][
    wire-them
    let esp_sq [  ]
    ask turtles with [ color != grey ][ set esp_sq lput
      (count neighbor_link-neighbors) esp_sq ]
    let esperance (sum esp_sq / nt)^ 2
    set esp_sq (sum (map * esp_sq esp_sq) / nt)
    ifelse esp_sq - esperance = 0 [ output-print
      "Regular lattice successfully built" ]
      [ output-print "Regular lattice not successfully built" ]
    set lat_tot_edges count neighbor_links
    find-path-lengths
    let num-connected-pairs sum [ length remove infinity
      (remove 0 distance-from-other-turtles) ] of turtles
    set average-path-length (sum [ sum distance-from-other-turtles ] of turtles)
      / (num-connected-pairs)
    find-clustering-coefficient
    set lattice_eq_clustering-coefficient clustering-coefficient
    set lattice_eq_average-path-length average-path-length
    graph_connex??
    if not connex? and K_rgraph > 4 [ connecting_graph ]
    rewire_procedure ] ]
if connex? [
  find-path-lengths
  let num-connected-pairs sum [ length remove infinity (remove 0
    distance-from-other-turtles) ] of turtles
  set average-path-length (sum [ sum distance-from-other-turtles ] of turtles)
    / (num-connected-pairs)
  find-clustering-coefficient ]
end

to connecting_graph
if K_rgraph > 1 [
  ask turtle 0 [ set color red ]
  let z2 0
  while [ z2 < (2 * (turtle_multiplier * (%passives + %frugals + %stalwarts
    + %epicures))) ][
    ask turtles with [ color != grey and color != red ][
      let condition_color count neighbor_link-neighbors with [ color = red ]
      if condition_color > 0 [ set color red ] ]
  set z2 (z2 + 1) ]
  while [ count turtles with [ color = red ] != (turtle_multiplier *
    (%passives + %frugals + %stalwarts + %epicures)) ][
    let signature1 (count turtles * 2)
  let signature2 (count turtles * 2)
  let signature3 (count turtles * 2)
  let signature4 (count turtles * 2)
    ask min-one-of turtles with [ color != red and color != grey ] [ who ] [
      let link_of_interest one-of my-neighbor_links
      ask link_of_interest [ set signature1 end1 set signature2 end2 ]
      ask link_of_interest [ die ] ]
    ask min-one-of turtles with [ color = red ] [ who ][
      let link_of_interest2 one-of my-neighbor_links
      ask link_of_interest2 [ set signature3 end1 set signature4 end2 ]
      ask link_of_interest2 [ die ] ]
    ask signature3 [ create-neighbor_link-with signature1 ]
    ask signature1 [ set color red ]
    ask signature4 [ create-neighbor_link-with signature2 ]
    ask signature2 [ set color red ]
    let z3 0
    while [ z3 < (2 * (turtle_multiplier * (%passives + %frugals + %stalwarts
    + %epicures))) ][
      ask turtles with [ color != grey and color != red ][
        let condition_color count neighbor_link-neighbors with [ color = red ]
        if condition_color > 0 [ set color red ] ]
    set z3 (z3 + 1) ] ]
  if count turtles with [ color = red ] = nt [ output-print "The network is connex"
    ask passives [ set color blue ]
  ask frugals [ set color magenta ]
  ask epicures [ set color (brown + 1) ]
  ask stalwarts [ set color green ] ] ]
end

to-report in-neighborhood? [  hood  ]
report ( member? end1 hood and member? end2 hood )
end

to initial_innovators ; Both frugals and stalwarts can be early adopters
                      ; values of bass_p cannot be higher than
            ; (# Stalwarts + # frugals) / population (see info tab)
set bass_p 0.03
let dice random-float 1
let stalwart_adopt (bass_p * dice)
let pop nt
let n_stalwarts_adopters round (stalwart_adopt * pop)
if n_stalwarts_adopters = 0 [
  set n_stalwarts_adopters 1 ]
let cs count stalwarts
let cf count frugals
let nsa n_stalwarts_adopters
if nsa > cs [ set nsa cs ]
ask n-of nsa stalwarts [
  set n_bhvr (n_bhvr + 1)
  set color yellow
  set active? true ]
let adopt round (bass_p * pop)
let n_frugals_adopters (adopt - n_stalwarts_adopters)
if n_frugals_adopters > cf [ set n_frugals_adopters cf ]
if n_frugals_adopters >= 0 [
  ask n-of n_frugals_adopters frugals [
  set n_bhvr (n_bhvr + 1)
  set active? true
  set color yellow ] ]
set %acc round ((count turtles with [ color = yellow ] / nt) * 100)
end

to setup_sb_model
upload_inputs
set t_daily_lcia matrix:make-constant 12 24 0
setup_ef ; Setup emission factors (build the initial LCA processes tree
         ; and prepare the LCIA procedure)
; Distribution of appliances:
let i 0
while [ i < 23 ][
  let first_fuel matrix:get hh_app_distribution 0 i  ; Appliance row (fuel type)
  let second_fuel matrix:get hh_app_distribution 1 i ; Appliance row (2nd fuel type)
  let app_index  matrix:get hh_app_distribution 2 i
  let app_distrib (round (matrix:get hh_app_distribution 3 i * nt))
  let app_energy matrix:get hh_app_distribution 4 i ; From Natural Resources Canada
                      ; (2016). "Comprehensive Energy Use Database." (see info tab).
  let app_dependency matrix:get hh_app_distribution 5 i
  ifelse app_dependency < 25 [
    set dep_app app_dependency  ; Can only store one appliance with a dependent
; (otherwise: create as much storing variable as needed) (25 = independant appliance)
    ifelse second_fuel = 15 [
      ifelse app_distrib <= nt [
      ask n-of app_distrib turtles with [ color != grey AND
      (reduce + matrix:get-column appliances app_index) = 0 ][
        matrix:set appliances first_fuel app_index app_energy
        matrix:set appliances first_fuel (app_index + 1)
      (matrix:get hh_app_distribution 4 (i + 1)) ] ][
; Dependent appliances must be written in the csv file in the column
; directly next to the appliance it is dependent of
      ask n-of nt turtles with [ color != grey ][
        matrix:set appliances first_fuel app_index app_energy
        matrix:set appliances first_fuel (app_index + 1)
      (matrix:get hh_app_distribution 4 (i + 1)) ]
      ask n-of (app_distrib - nt) turtles with [ color != grey ][
        matrix:set appliances first_fuel app_index app_energy
        matrix:set appliances first_fuel (app_index + 1)
      (matrix:get hh_app_distribution 4 (i + 1)) ] ] ][
      ask turtles with [ color != grey AND
      (reduce + matrix:get-column appliances app_index < (app_energy / 2)) ][
        matrix:set appliances first_fuel app_index (app_energy / 2)
        matrix:set appliances second_fuel app_index (app_energy / 2)
; Assumption: both fuel contribute equally to energy use (A1)
        matrix:set appliances first_fuel (app_index + 1)
      (matrix:get hh_app_distribution 4 (i + 1)) ] ]
; Assumption: dependent appliance only has one fuel
; (in our case cwash and dryer has both only one fuel)
    set i (i + 1)  ][
    ifelse second_fuel = 15 [
      ifelse (app_distrib <= nt)[
        ask n-of app_distrib turtles with [ color != grey AND
      (reduce + matrix:get-column appliances app_index) = 0 ][
          matrix:set appliances first_fuel app_index app_energy ] ][
        ask n-of nt turtles with [ color != grey ][
          matrix:set appliances first_fuel app_index app_energy ]
        ask n-of (app_distrib - nt) turtles with [ color != grey ][
          matrix:set appliances first_fuel app_index (matrix:get appliances
        first_fuel app_index + app_energy) ] ] ][
        ask turtles with [ color != grey AND
      (reduce + matrix:get-column appliances app_index < (app_energy / 2)) ][
          matrix:set appliances first_fuel app_index (app_energy / 2)
          matrix:set appliances second_fuel app_index (app_energy / 2) ] ] ] ; (A1)
    set i (i + 1) ]
ask one-of turtles with [ color != grey ][
  set n_type_appliances item 1 matrix:dimensions appliances ]
; Assumption 3kW rooftop solar PV + 8 kWh battery (used twice in the day and
; surplus are sold)
if PV&Battery_scenario != 1 [ ask turtles with [ color != grey ][
  ifelse PV_daily_prod < 8 [ matrix:set appliances 0 (n_type_appliances - 1)
    (-365 * PV_daily_prod) ][ matrix:set
    appliances 0 (n_type_appliances - 1) (-365 * 8)
    set PV_surplus matrix:make-constant 1 24 ((PV_daily_prod - 8) / 24
      * -1 * microfit_price)
    set default_PV_surplus matrix:make-constant 1 24 ((PV_daily_prod - 8) / 24
      * -1 * microfit_price)
    ] ] ]

ask turtles with [ color != grey ][
  set appliances_default matrix:copy appliances
  set appliances_default2 matrix:copy appliances ]
; Computation of total heating proportion per day:
ifelse constant_weather? [
  set HDD_% matrix:from-row-list csv:from-file "inputFiles/HDD_blank.csv"
  set CDD_% matrix:from-row-list csv:from-file "inputFiles/CDD_blank.csv"
  set PV_output matrix:from-row-list csv:from-file "inputFiles/PV_output_Toronto.csv" ][
  ifelse up_north? [ set HDD_% matrix:from-row-list csv:from-file "inputFiles/HDD_2014_TB.csv"
    set CDD_% matrix:from-row-list csv:from-file "inputFiles/CDD_2014_TB.csv"
; For more information on solar PV modelization see the info tab.
    set PV_output matrix:from-row-list csv:from-file "inputFiles/PV_output_ThunderBay.csv" ][
    set HDD_% matrix:from-row-list csv:from-file "inputFiles/HDD_2014.csv"
    set CDD_% matrix:from-row-list csv:from-file "inputFiles/CDD_2014.csv"
    set PV_output matrix:from-row-list csv:from-file "inputFiles/PV_output_Toronto.csv" ] ]
set HDD_% matrix:times (1 / tot_hdegree-day) HDD_%
set HDD_% matrix:times (tot_hdegree-day / tot_hdegree-day_toronto) HDD_%
set CDD_% matrix:times (1 / tot_cdegree-day) CDD_%
set CDD_% matrix:times (tot_cdegree-day / tot_cdegree-day_toronto) CDD_%
; Assumption: average Ontarian energy consumption represents correctly
; the energy consumption in the Toronto area (see info tab).
set shift 74438 ; (Alphanumeric character: SHIFT = 74438)
set hourly_SB_cons matrix:get-row hourly_SB_cons 0
set cumul_hourly_SB_cons matrix:get-row cumul_hourly_SB_cons 0
set hourly_SB_cost matrix:get-row hourly_SB_cost 0
set cumul_hourly_SB_cost matrix:get-row cumul_hourly_SB_cost 0
set hourly_SB_impact matrix:get-row hourly_SB_impact 0
set cumul_hourly_SB_impact matrix:get-row cumul_hourly_SB_impact 0
; For output of all agents elec consumption, cost and (climate change only) impact
set diff_discrepancy 0.57 ; From Kaiser et al. (see info tab)
set SB_default_cons 0   ; For output...
set SB_default_cost 0   ; .
set SB_default_impact_ht 0 ; .
set SB_default_impact_re 0
set SB_default_impact_ir 0
set SB_default_impact_old 0
set SB_default_impact_po 0
set SB_default_impact_ae 0
set SB_default_impact_te 0
set SB_default_impact_tan 0
set SB_default_impact_lo 0
set SB_default_impact_cc 0
set SB_default_impact_nre 0
set SB_default_impact_rme 0
set SB_default_impact_hh 0
set SB_default_impact_eq 0
set SB_default_impact_res 0
set SB_default_impact_ss 0
set SB_cons 0           ; .
set SB_cost 0
set SB_impact_RE 0      ; ...for output
set SB_impact_ht 0
set SB_impact_re 0
set SB_impact_ir 0
set SB_impact_old 0
set SB_impact_po 0
set SB_impact_ae 0
set SB_impact_te 0
set SB_impact_tan 0
set SB_impact_lo 0
set SB_impact_cc 0
set SB_impact_nre 0
set SB_impact_rme 0
set SB_impact_hh 0
set SB_impact_eq 0
set SB_impact_res 0
set SB_impact_ss 0
set SB_RE_impact_ht 0
set SB_RE_impact_re 0
set SB_RE_impact_old 0
set SB_RE_impact_po 0
set SB_RE_impact_ae 0
set SB_RE_impact_te 0
set SB_RE_impact_tan 0
set SB_RE_impact_cc 0
set SB_RE_impact_hh 0
set SB_RE_impact_eq 0
set SB_RE_impact_ss 0
set global_econst matrix:make-constant n_type_appliances 24 0
set global_default_econst matrix:make-constant n_type_appliances 24 0
set microfit_price 0.0549
set detailed_lcia_cc []
set detailed_lcia_hh []
set detailed_lcia_eq []
set detailed_lcia_res []
; Both for output but allow a better granularity (consumption per appliances)
ask turtles with [ color != grey ] [
  set savings 0 ; Will allow RE calculation
  set ht_live_rebound_tot 0  ; For output (will be aggregated for the whole building)
  set re_live_rebound_tot 0  ; .
  set old_live_rebound_tot 0 ; .
  set po_live_rebound_tot 0  ; .
  set ae_live_rebound_tot 0
  set te_live_rebound_tot 0
  set tan_live_rebound_tot 0
  set cc_live_rebound_tot 0
  set ht_live_impact_tot 0
  set re_live_impact_tot 0
  set old_live_impact_tot 0
  set po_live_impact_tot 0
  set ae_live_impact_tot 0
  set te_live_impact_tot 0
  set tan_live_impact_tot 0
  set cc_live_impact_tot 0
  set hh_live_impact_tot 0
  set eq_live_impact_tot 0
  set ss_live_impact_tot 0 ; ..for output (will be aggregated for the whole building)
  set cumul_cons 0   ; Keep track of cumulative values
  set cumul_cost 0   ; .
  set cumul_impact 0 ; .
  set bhvr_index 15   ; Bhvr index start at 15 as default value: allows agents
  set bhvr_index2 15  ; to limit their 1st bhvr choice during roulette wheel process
  set bhvr1 15        ; 15 or 25 are treated as equivalent to "NaN"
  set bhvr2 15        ; anywhere in the code
  set bhvr3 15
  set bhvr4 15
  set bhvr5 15
  set shift_bhvr matrix:from-row-list [ [ 15 15 15 15 15 ] ] ; appliances s.t. shift
  set bhvr_sheat 1 ; Used for weather accountability: default consumption
  set bhvr_scool 1 ; can be retrieved and both default and current consumption
                   ; can be computed allowing accountability of weather.
  set turtle_vision matrix:make-constant 3 24 0 ; Prepare simplistic
                                                ; vision optimization model.
  set degree_node 0 ] ; Used in the social-network procedure
; Preparation of the go procedure:
ask turtles with [ color != grey ][
 set econst matrix:make-constant n_type_appliances 24 0
 set default_econs matrix:make-constant n_type_appliances 24 0
 let j 0
 while [ j < 24 ][
   matrix:set-column econst j
     (map * matrix:get-row appliances 0 matrix:get-column freqcons j)
   matrix:set-column default_econs j (map * matrix:get-row appliances_default2 0
     matrix:get-column default_freqcons j)
   set j (j + 1) ]
  set econst matrix:times (1 / 365) econst ; Transform annual to daily demand, econst
  set default_econs matrix:times (1 / 365) default_econs ; in kW (demand or load)
  let k 0
  while [ k < n_type_appliances ][
    matrix:set-row ht_h_impact k (map * matrix:get-row t_daily_lcia 0
      matrix:get-row econst k)
    matrix:set-row ht_default_impact k (map * matrix:get-row t_daily_lcia 0
      matrix:get-row default_econs k)  ; human toxicity in kgC2H3Cleq
    matrix:set-row re_h_impact k (map * matrix:get-row t_daily_lcia 1
      matrix:get-row econst k)
    matrix:set-row re_default_impact k (map * matrix:get-row t_daily_lcia 1
      matrix:get-row default_econs k)  ; respiratory effect in kg PM2.5eq
    matrix:set-row ir_h_impact k (map * matrix:get-row t_daily_lcia 2
      matrix:get-row econst k)
    matrix:set-row ir_default_impact k (map * matrix:get-row t_daily_lcia 2
      matrix:get-row default_econs k)  ; ionizing radiation in Bq C-14eq
    matrix:set-row old_h_impact k (map * matrix:get-row t_daily_lcia 3
      matrix:get-row econst k)
    matrix:set-row old_default_impact k (map * matrix:get-row t_daily_lcia 3
      matrix:get-row default_econs k)  ; ozone layer depletion in kgCFC11eq
    matrix:set-row po_h_impact k (map * matrix:get-row t_daily_lcia 4
      matrix:get-row econst k)
    matrix:set-row po_default_impact k (map * matrix:get-row t_daily_lcia 4
      matrix:get-row default_econs k)  ; photochemical oxidation in kgC2H4eq
    matrix:set-row ae_h_impact k (map * matrix:get-row t_daily_lcia 5
      matrix:get-row econst k)
    matrix:set-row ae_default_impact k (map * matrix:get-row t_daily_lcia 5
      matrix:get-row default_econs k)  ; aquatic ecotoxicity in kg TEG water
    matrix:set-row te_h_impact k (map * matrix:get-row t_daily_lcia 6
      matrix:get-row econst k)
    matrix:set-row te_default_impact k (map * matrix:get-row t_daily_lcia 6
      matrix:get-row default_econs k)  ; terrestrial ecotoxicity in kg TEG soil
    matrix:set-row tan_h_impact k (map * matrix:get-row t_daily_lcia 7
      matrix:get-row econst k)
    matrix:set-row tan_default_impact k (map * matrix:get-row t_daily_lcia 7
      matrix:get-row default_econs k)  ; terrestrial acidification,
                                       ; nutrification in kg So2eq
    matrix:set-row lo_h_impact k (map * matrix:get-row t_daily_lcia 8
      matrix:get-row econst k)
    matrix:set-row lo_default_impact k (map * matrix:get-row t_daily_lcia 8
      matrix:get-row default_econs k)  ; land occupation in m2org.arable
    matrix:set-row cc_h_impact k (map * matrix:get-row t_daily_lcia 9
      matrix:get-row econst k)
    matrix:set-row cc_default_impact k (map * matrix:get-row t_daily_lcia 9
      matrix:get-row default_econs k)  ; climate change in kgCO2eq )
    matrix:set-row nre_h_impact k (map * matrix:get-row t_daily_lcia 10
      matrix:get-row econst k)
    matrix:set-row nre_default_impact k (map * matrix:get-row t_daily_lcia 10
      matrix:get-row default_econs k)  ; non-renewable energy in MJ primary
    matrix:set-row rme_h_impact k (map * matrix:get-row t_daily_lcia 11
      matrix:get-row econst k)
    matrix:set-row rme_default_impact k (map * matrix:get-row t_daily_lcia 11
      matrix:get-row default_econs k)  ; raw material extraction in MJ surplus
    ifelse price_scenario != 3 [
      matrix:set-row cost k (map * matrix:get-row hourly_cost 0
        matrix:get-row econst k)
      matrix:set-row default_cost k (map * matrix:get-row hourly_cost 0
        matrix:get-row default_econs k) ][  ; in $ (hourly_cost: $/kW, econst: kW)
      matrix:set-row cost k (map * matrix:get-row hourly_cost2 0
        matrix:get-row econst k)
      matrix:set-row default_cost k (map * matrix:get-row hourly_cost2 0
        matrix:get-row default_econs k) ]
    set k (k + 1) ]
  set rebound (matrix:times impact_consum matrix:transpose consum) ] ; For RE
set passive0_econst [ econst ] of turtle 0
set passive0_defaulteconst [ default_econs ] of turtle 0
set yellowguy_econst [ econst ] of turtle yellowguy
set yellowguy_defaulteconst [ default_econs ] of turtle yellowguy
set passive0_freqcons [ freqcons ] of turtle 0
set passive0_defaultfreqcons [ default_freqcons ] of turtle 0
set yellowguy_freqcons [ freqcons ] of turtle yellowguy
set yellowguy_defaultfreqcons [ default_freqcons ] of turtle yellowguy
; Allows to follow two agents individually (1 passive, 1 either frugal or stalwart)
if goal = 4 [  ; Optimize impact w/ RE
  if re_impact_goal = 0 [ set impact_goal 0 ]
  if re_impact_goal = 1 [ set impact_goal 1 ]
  if re_impact_goal = 2 [ set impact_goal 3 ]
  if re_impact_goal = 3 [ set impact_goal 4 ]
  if re_impact_goal = 4 [ set impact_goal 5 ]
  if re_impact_goal = 5 [ set impact_goal 6 ]
  if re_impact_goal = 6 [ set impact_goal 7 ]
  if re_impact_goal = 7 [ set impact_goal 9 ]
  if re_impact_goal = 8 [ set impact_goal 12 ]
  if re_impact_goal = 9 [ set impact_goal 13 ]
  if re_impact_goal = 10 [ set impact_goal 15 ] ] ; Keep coherence
if individual_vision = 1 [
  ask turtles with [color != grey] [ matrix:set-row turtle_vision (vision - 1)
    matrix:get-row matrix:make-constant 1 24 1 0 ] ]
; Prepare a simplistic optimization model where vision is set to the category with
; highest impact score in points. WARNING ! Be careful when using this option and
; goal = 4 as ecoinvent and openIO-Canada elementary flows are unbalanced.
; If you want to have meaningful results consider set RE2ndAssumption? ON.
end

; This important procedure is where all exogenous inputs are
; "uploaded" in the model. You can change inputs by creating your own
; csv files. For instance XF from impact 2002+ could be replaced by XF from recipe.
; See info tab form more info.
to upload_inputs ; Get all model inputs from csv files
ifelse not RE2ndAssumption? [ set xf_impact2002 matrix:from-row-list
  csv:from-file "inputFiles/xf_impact2002.csv" ][
; All XF of impact 2002+ corresponding to ecoinvent elementary flows of
; technology matrix A
  set xf_impact2002 matrix:from-row-list
  csv:from-file "inputFiles/xf_impact2002_RE2ndAssumption.csv" ]
; Only XF of impact 2002+ corresponding to openIO-Canada elementary flows
set xf_endpoint_impact2002 matrix:from-row-list
  csv:from-file "inputFiles/xf_endpoint_impact2002.csv"
set single_score_impact2002 matrix:from-row-list
  csv:from-file "inputFiles/single_score_impact2002.csv"
; Endpoint converted lcia
set xf_impact2002_lcia matrix:copy xf_impact2002
let count_xf_impact2002_lcia 0
let current_xf_enpoint "NaN"
while [ count_xf_impact2002_lcia < 2755 ][
  ifelse count_xf_impact2002_lcia < 175 [ set current_xf_enpoint 1 ][
    ifelse count_xf_impact2002_lcia < 868 [ set current_xf_enpoint
      matrix:get xf_endpoint_impact2002 0 5 ][
      ifelse count_xf_impact2002_lcia < 898 [ set current_xf_enpoint
        matrix:get xf_endpoint_impact2002 0 8 ][
        ifelse count_xf_impact2002_lcia < 917 [ set current_xf_enpoint
          matrix:get xf_endpoint_impact2002 0 7 ][
          ifelse count_xf_impact2002_lcia < 1596 [ set current_xf_enpoint
            matrix:get xf_endpoint_impact2002 0 6 ][
  ifelse count_xf_impact2002_lcia < 2233 [ set current_xf_enpoint
    matrix:get xf_endpoint_impact2002 0 0 ][
    ifelse count_xf_impact2002_lcia < 2357 [ set current_xf_enpoint
      matrix:get xf_endpoint_impact2002 0 2 ] [
      ifelse count_xf_impact2002_lcia < 2451 [ set current_xf_enpoint
        matrix:get xf_endpoint_impact2002 0 3 ][
        ifelse count_xf_impact2002_lcia < 2687 [ set current_xf_enpoint
          matrix:get xf_endpoint_impact2002 0 4 ][
          ifelse count_xf_impact2002_lcia < 2715 [ set current_xf_enpoint
            matrix:get xf_endpoint_impact2002 0 1 ][
            ifelse count_xf_impact2002_lcia < 2748 [ set current_xf_enpoint
              matrix:get xf_endpoint_impact2002 0 10 ][
              set current_xf_enpoint
              matrix:get xf_endpoint_impact2002 0 11 ]]]]]]]]]]]
  matrix:set xf_impact2002_lcia count_xf_impact2002_lcia 0 (current_xf_enpoint *
    matrix:get xf_impact2002_lcia count_xf_impact2002_lcia 0)
  set count_xf_impact2002_lcia (count_xf_impact2002_lcia + 1) ]
set mix_ontario matrix:from-row-list csv:from-file "inputFiles/mix_ontario.csv"
; Temporally desagregated matrix of the mix: line i = state of the mix @ time-step i
; hence, temporal resolution = hour. Column j = process j.
set ef_mix matrix:from-row-list csv:from-file "inputFiles/ef_mix.csv"
; Elementary flows: line i = flow of substance i. Column j = process j
if mix_scenario = 1 [
  set tech_matrix matrix:from-row-list csv:from-file "inputFiles/tech_matrix.csv" ]
if mix_scenario = 2 [
  set tech_matrix matrix:from-row-list csv:from-file "inputFiles/tech_matrix_constant_mix.csv"
  set mix_ontario matrix:from-row-list csv:from-file "inputFiles/mix_ontario_constant.csv" ]
if mix_scenario = 3 [
  set tech_matrix matrix:from-row-list csv:from-file "inputFiles/tech_matrix_constant_mix.csv"
  set mix_ontario matrix:from-row-list csv:from-file "inputFiles/mix_ontario_marginal.csv" ]
; Technology matrix (@ time-step = None) --> General recipe for the mix or constant
; mix recipe to create edges between processes (amounts vary as a function of time).
ask turtles with [ color != grey ][
  set appliances matrix:from-row-list csv:from-file "inputFiles/appliances_elec_consump.csv"
; Matrix 5x11: column are appliances (stove&oven, fridge, freezer, dishwasher,
; clothe washer, dryer, other load (TC, computer etc.), light, space heating,
; water heating, space cooling and line are ;different type of energy
; (electric, gaz, oil, wood, coal/propane)
  set appliances_default matrix:from-row-list
    csv:from-file "inputFiles/appliances_elec_consump.csv"
  set appliances_default2 matrix:from-row-list
    csv:from-file "inputFiles/appliances_elec_consump.csv"
  set freqcons matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
  set default_freqcons matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
  set ht_h_impact matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
  set re_h_impact matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
  set ir_h_impact matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
  set old_h_impact matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
  set po_h_impact matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
  set ae_h_impact matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
  set te_h_impact matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
  set tan_h_impact matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
  set lo_h_impact matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
  set cc_h_impact matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
  set nre_h_impact matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
  set rme_h_impact matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
  set ht_default_impact matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
  set re_default_impact matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
  set ir_default_impact matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
  set old_default_impact matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
  set po_default_impact matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
  set ae_default_impact matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
  set te_default_impact matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
  set tan_default_impact matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
  set lo_default_impact matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
  set cc_default_impact matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
  set nre_default_impact matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
  set rme_default_impact matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
  set cost matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
  set default_cost matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
; "initial_values.csv" = a 11x24 matrix with zeros (not strictly necessary)
  set rebound matrix:from-row-list csv:from-file "inputFiles/iv_rebound.csv"
; iv_rebound is a file with 238 zeroes
  if rebound_scenario = 1 [
    set consum matrix:from-row-list csv:from-file "inputFiles/consumption_mix.csv" ]
  if rebound_scenario = 2 [
    set consum matrix:from-row-list csv:from-file "inputFiles/consumption_airtravel.csv" ]
  if rebound_scenario = 3 [
    set consum matrix:from-row-list csv:from-file "inputFiles/consumption_airtravel_2.csv" ]
  if rebound_scenario = 4 [
    set consum matrix:from-row-list csv:from-file "inputFiles/consumption_airtravel_3.csv" ]
; % of expenditure in each categories of impact for an average Ontario houshold
; (except electricity) (Statistic Canada 2014) e.g. category names in IO tables:
; M53C0 M31C0  M7200  M53D0 F2000  ... M31D0, rebound_scenario = 2: Air Travel only
; rebound_scenario = 2: financial product only (see info tab)
  set h_smarthome_cons matrix:from-row-list csv:from-file "inputFiles/initial_values.csv" ; Used
  set h_smarthome_cons matrix:get-row h_smarthome_cons 0 ; in social-interaction part
  set cumul_h_smarthome_cons matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
  set cumul_h_smarthome_cons matrix:get-row cumul_h_smarthome_cons 0
  set PV_surplus matrix:make-constant 1 24 0
set default_PV_surplus matrix:make-constant 1 24 0 ]
ifelse all_electric? [ set hh_app_distribution matrix:from-row-list
  csv:from-file "inputFiles/hh_n&E_app_all_elec.csv" ][
  set hh_app_distribution matrix:from-row-list csv:from-file "inputFiles/hh_n&E_app.csv" ]
; Convention hh_n&E_app : appliances are organized by fuel order :
; electric > gaz > oil > wood > other > wood/electric > wood/oil > gaz/electric
; > oil/electric, hence if an appliance is concerned by two column the first one
; contain values for electric appliance and the second one for gaz appliances.
; Annual consumption for average hh  broken down by appliances (kWh)
; (see info tab for more info). Appliance abbreviations for the rest of the code:
; Stove and oven = s&o, fridge = ref, freezer = fre, dishwasher = dish,
; clothe washer = cwah, dryer = dry, other load = otha, lightnings = light,
; surface heating = sheat, water heating = wheat, surface cooling = scool.
ifelse constant_weather? [
  let HDD matrix:from-row-list csv:from-file "inputFiles/HDD_blank.csv"
  let CDD matrix:from-row-list csv:from-file "inputFiles/CDD_blank.csv"
  let HDD_toronto matrix:from-row-list csv:from-file "inputFiles/HDD_blank.csv"
  let CDD_toronto matrix:from-row-list csv:from-file "inputFiles/CDD_blank.csv"
  set tot_hdegree-day reduce + matrix:get-row HDD 0
  set tot_cdegree-day reduce + matrix:get-row CDD 0
  set tot_hdegree-day_toronto reduce + matrix:get-row HDD 0
  set tot_cdegree-day_toronto reduce + matrix:get-row CDD 0 ][
  ifelse up_north? [
    let HDD matrix:from-row-list csv:from-file "inputFiles/HDD_2014_TB.csv"
    let CDD matrix:from-row-list csv:from-file "inputFiles/CDD_2014_TB.csv"
    let HDD_toronto matrix:from-row-list csv:from-file "inputFiles/HDD_2014.csv"
    let CDD_toronto matrix:from-row-list csv:from-file "inputFiles/CDD_2014.csv"
    set tot_hdegree-day reduce + matrix:get-row HDD 0
    set tot_cdegree-day reduce + matrix:get-row CDD 0
    set tot_hdegree-day_toronto reduce + matrix:get-row HDD 0
    set tot_cdegree-day_toronto reduce + matrix:get-row CDD 0 ][
    let HDD matrix:from-row-list csv:from-file "inputFiles/HDD_2014.csv"
    let CDD matrix:from-row-list csv:from-file "inputFiles/CDD_2014.csv"
    let HDD_toronto matrix:from-row-list csv:from-file "inputFiles/HDD_2014.csv"
    let CDD_toronto matrix:from-row-list csv:from-file "inputFiles/CDD_2014.csv"
    set tot_hdegree-day reduce + matrix:get-row HDD 0
    set tot_cdegree-day reduce + matrix:get-row CDD 0
    set tot_hdegree-day_toronto reduce + matrix:get-row HDD 0
    set tot_cdegree-day_toronto reduce + matrix:get-row CDD 0 ] ]
; Heating and cooling degree days (baseline = 18 degree celcius)
; calculated from Thunder Bay (with "TB" in name) and Toronto weather stations data.
ifelse price_scenario = 2 [ set hourly_cost matrix:from-row-list
  csv:from-file "inputFiles/electricity_prices_constant.csv"
  set hourly_cost2 matrix:from-row-list
  csv:from-file "inputFiles/electricity_prices_constant.csv" ][
  set hourly_cost matrix:from-row-list
  csv:from-file "inputFiles/electricity_prices_winter0.csv"
  set hourly_cost2 matrix:from-row-list
  csv:from-file "inputFiles/electricity_prices_winter0.csv" ]
; From (Ontario Energy Board 2013-2014) (from 01/04/13 to 30/04/13) (see info tab).
set impact_consum matrix:from-row-list csv:from-file "inputFiles/impact_consum_all.csv"
; From (Lesage 2012 (Open IO-Canada online tool)) (see info tab).
set p_bhvr_winter matrix:from-row-list csv:from-file "inputFiles/P_bhvr_winter.csv"
set p_bhvr_summer matrix:from-row-list csv:from-file "inputFiles/P_bhvr_summer.csv"
; Roulette wheel probabilities (see info tab)
if bhvr_set = 1 [
  set bhvr_matrix matrix:from-row-list csv:from-file "inputFiles/bhvr_matrix.csv" ]
if bhvr_set = 2 [
  set bhvr_matrix matrix:from-row-list csv:from-file "inputFiles/bhvr_matrix2.csv" ]
if bhvr_set = 3 [
  set bhvr_matrix matrix:from-row-list csv:from-file "inputFiles/bhvr_matrix3.csv" ]
if bhvr_set = 4 [
  set bhvr_matrix matrix:from-row-list csv:from-file "inputFiles/bhvr_matrix4.csv" ]
; Bhvr_matrix1 = empirical data, assumption and optimization model
; --> automatic peak shaving, occupant peak shaving and energy conservation.
; Bhvr_matrix2 = modeler choice of behaviors and optimization model
; --> automatic peak shaving, occupant peak shaving and energy conservation
; as decided by modeler.
; Bhvr_matrix3 = empirical data and assumption
; --> no peak shaving but occupant energy conservation.
; Bhvr_matrix4 = optimization model
; --> automatic peak shaving but no occupant energy conservation
; (100% independant optimization model: set agent_decision_process? off)
; See info tab for more details.
set hourly_SB_cons matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
set cumul_hourly_SB_cons matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
set hourly_SB_cost matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
set cumul_hourly_SB_cost matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
set hourly_SB_impact matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
set cumul_hourly_SB_impact matrix:from-row-list csv:from-file "inputFiles/initial_values.csv"
; A 11x24 matrix with zeros
set hourly_proba matrix:from-row-list csv:from-file "inputFiles/Data_prob.csv"
; A 11x1000 matrix containing hours of utilization of each appliances, (used as
; a dice (appliance-dice)) based on hourly probability from (Paatero 2006)
; (see info tab).
set daily_wd_freq matrix:from-row-list csv:from-file "inputFiles/daily_wd_freq.csv"
set daily_we_freq matrix:from-row-list csv:from-file "inputFiles/daily_we_freq.csv"
; Frequencies of utilization of each appliances during the day
set xf_avrg_art3 matrix:from-column-list csv:from-file "inputFiles/xf_avg_art3.csv"
end

to setup_ef
set functional_unit matrix:submatrix matrix:make-identity
  (item 0 matrix:dimensions tech_matrix) 0 0 (item 0 matrix:dimensions tech_matrix) 1
create-processes (item 0 matrix:dimensions tech_matrix) [
  set color grey
  set size 0.5
  set shape "square"
  set xcor 31
  set ycor -36 ]
let i (31 - round((item 0 matrix:dimensions tech_matrix)^(1 / 2)))
let j (-36 + round((item 0 matrix:dimensions tech_matrix)^(1 / 2)))
let k nt
let p 0
while [ p < count turtles with [ color = grey ] ][
  ask turtle k [   ; Place processes agents in the world
    set xcor i     ; and gives all their attributes to processes
    set ycor j
    set process_id p
    set grid_recipe matrix:submatrix mix_ontario 0 p
      (item 0 matrix:dimensions mix_ontario) (p + 1)
    set elementary_flows matrix:submatrix ef_mix 0 p
      (item 0 matrix:dimensions ef_mix) (p + 1)
    let link_process matrix:get-column tech_matrix p
    let r 0
    while [ r < item 0 matrix:dimensions tech_matrix ][
      let lp_value item r link_process
      if (lp_value < 0)[ create-economic_flow-from process (nt + r)
        [ set thickness (abs lp_value / 10) ] ] ; First process tree
      set r (r + 1) ] ]                            ; but not updated in go
   set i (i + 1)
   set k (k + 1)
   set p (p + 1)
   if ((i / 32) = int (i / 32) AND (i != 0)) [ set j (j - 1)
     set i (31 - round((item 0 matrix:dimensions tech_matrix)^(1 / 2))) ] ]
end

; PROCEDURE CALLED IN GO (IN CALLING ORDER)
to temperature_cost_PV
; Account for temperature and cost variability during the year
if (nticks / 24) = int (ticks / 24) [
  ask turtles with [ color != grey ][
    let HDD_inter matrix:get HDD_% 0 nd ; Data from statistic Canada (see info tab)
    let bhvr_value bhvr_sheat ; Allows for weather accounting of sheat and scool
    let sheat_inter matrix:submatrix appliances_default 0 8 5 9
    set sheat_inter matrix:times (HDD_inter * 365) sheat_inter
; Rationale: all annual demand divided by 365 but already accounted for sheat/scool
    let sheat_inter2 matrix:times bhvr_value sheat_inter
    matrix:set-column appliances 8 matrix:get-column sheat_inter2 0
    matrix:set-column appliances_default2 8 matrix:get-column sheat_inter 0
    let CDD_inter matrix:get CDD_% 0 nd
    let bhvr_value2 bhvr_scool
    let scool_inter matrix:submatrix appliances_default 0 10 5 n_type_appliances
    set scool_inter matrix:times (CDD_inter * 365) scool_inter
    let scool_inter2 matrix:times bhvr_value2 scool_inter
    matrix:set-column appliances 10 matrix:get-column scool_inter2 0
    matrix:set-column appliances_default2 10 matrix:get-column scool_inter 0
; Update the daily production of solar PV, sell the surplus of production
; (production higher than the battery capacity (8 kWh))
    set PV_daily_prod matrix:get PV_output 0 nd
    if PV&Battery_scenario != 1 [
      ifelse PV_daily_prod < 8 [ matrix:set appliances 0 (n_type_appliances - 1)
        (-365 * PV_daily_prod)
        if PV&Battery_scenario = 2 [ matrix:set appliances_default2 0
          (n_type_appliances - 1) (-365 * PV_daily_prod) ] ][ matrix:set
        appliances 0 (n_type_appliances - 1) (-365 * 8)
        set PV_surplus matrix:make-constant 1 24 ((PV_daily_prod - 8) / 24
          * -1 * microfit_price)
        set default_PV_surplus matrix:make-constant 1 24 0
        if PV&Battery_scenario = 2 [ matrix:set appliances_default2 0
          (n_type_appliances - 1) (-365 * 8)
          set default_PV_surplus matrix:make-constant 1 24
          ((PV_daily_prod - 8) / 24 * -1 * microfit_price)
          ] ] ] ] ]

if nd = 30 AND price_scenario != 2 [
  set hourly_cost matrix:from-row-list
    csv:from-file "inputFiles/electricity_prices_summer.csv" ]  ; Electricity prices
if nd = 214 AND price_scenario != 2 [    ; according to Ontario energy board
  set hourly_cost matrix:from-row-list
    csv:from-file "inputFiles/electricity_prices_winter.csv" ]
;if nd = 148 [ set microfit_price 0.0396 ] ; according to IESO (26/08/13)
end

to setup_freqcons_lcia_loadshift
; Drawing of electricity demand profiles, shift of demand through time
; and computation of lcia are done for a all day (24 ticks)
if (nticks / 24) = int (ticks / 24) [
  setup_daily_lcia
  setup_freqcons
  setup_loadshift
  ask turtles with [color != grey] [
  set turtle_vision matrix:make-constant 3 24 0 ] ]
end

to setup_daily_lcia
; Every 24 hours emissions factors are determined in function of mix composition
; (hourly defined). The temporal vectors contained in process agents are used.
set t_daily_lcia matrix:make-constant 12 24 0
let q2 "value"
let cond_ntick "condition"
ifelse nticks = 0 [
  set q2 nticks
  set cond_ntick nticks ][
  set q2 nticks - 1
  set cond_ntick nticks - 1 ]
let q3 0
while [ q2 < (cond_ntick + 24) ][
  let current_tech_matrix matrix:copy tech_matrix
  let s 0
  while [ s < item 0 matrix:dimensions tech_matrix ][
    ask process (nt + s) [
      let processes_linked my-in-economic_flows
      if (any? processes_linked = true)[
        let linked_processes [ end1 ] of processes_linked
        foreach linked_processes [ ask ? [ matrix:set current_tech_matrix
          process_id s (-1 * matrix:get grid_recipe q2 0) ] ] ] ]
  set s (s + 1) ] ; Re-do the technology matrix according to the state
                  ; of the hourly mix for all time-step (hour) of the current day.
                  ; The state of the hourly mix is "contained" in each grid_recipe
                  ; vectors (owned by processes). Notice that those vectors could
                  ; come directly, in "real-time" from an external source
                  ; (e.g. utility website).
  let g_vector (matrix:times ef_mix
    (matrix:inverse current_tech_matrix) functional_unit) ; LCI : g = BA-1f
  let lcia_vector (matrix:map * g_vector xf_impact2002)
; LCIA: both elementary flows and xf factors vectors are aligned together (e.g.
; elementary flow "CO2, air,  high population density" is at line 1 in ef_mix vectors
; and so its characterization factor (line 1 in xf_impact2002).
; Moreover if an elementary flow has xf in several impact categories
; (such as sulfur dioxide) its value is duplicated in the necessary lines in ef_mix.
; This part let us access the lci and lcia in detail:
  if inventory_detail? [
    let detailed_lci matrix:make-constant 2755 item 0 matrix:dimensions tech_matrix 1
    let scale_factor matrix:times matrix:inverse current_tech_matrix functional_unit
    let counter_det_lci 0
    while [ counter_det_lci < item 0 matrix:dimensions tech_matrix ][
      matrix:set-column detailed_lci counter_det_lci map [? *
        matrix:get scale_factor counter_det_lci 0 ]
        matrix:get-column detailed_lci counter_det_lci
      set counter_det_lci (counter_det_lci + 1) ]
    set detailed_lci (matrix:map * ef_mix detailed_lci)
    ask turtles with [ color != grey ][ set live_econs_lci
       (reduce + matrix:get-column default_econs q3) ]
    let electric_consumption_lci (sum [ live_econs_lci ] of turtles with [ color != grey ])
    set cumul_lci_vector matrix:plus cumul_lci_vector
    matrix:times electric_consumption_lci g_vector
    set cumul_lcia_vector matrix:plus cumul_lcia_vector
    matrix:times electric_consumption_lci lcia_vector
    set cumul_detailed_lci matrix:plus cumul_detailed_lci
    matrix:times electric_consumption_lci detailed_lci
    set xf_matrix matrix:make-constant 2755 item 0 matrix:dimensions tech_matrix 1
    let counter_xf_matrix 0
    while [ counter_xf_matrix < item 0 matrix:dimensions tech_matrix ][
      matrix:set-column xf_matrix counter_xf_matrix
      matrix:get-column xf_impact2002_lcia 0
      set counter_xf_matrix (counter_xf_matrix + 1) ]
    set cumul_detailed_lcia (matrix:map * cumul_detailed_lci xf_matrix)
    let counter_detailed_lcias 0
    set detailed_lcia_cc []
    set detailed_lcia_hh []
    set detailed_lcia_eq []
    set detailed_lcia_res []
    while [ counter_detailed_lcias < 16 ][
      set detailed_lcia_cc lput (matrix:get-column matrix:submatrix cumul_detailed_lcia 0
      counter_detailed_lcias 175 (counter_detailed_lcias + 1) 0 ) detailed_lcia_cc
      set detailed_lcia_hh lput (matrix:get-column matrix:submatrix cumul_detailed_lcia 1596
      counter_detailed_lcias 2715 (counter_detailed_lcias + 1) 0 ) detailed_lcia_hh
      set detailed_lcia_eq lput (matrix:get-column matrix:submatrix cumul_detailed_lcia 175
      counter_detailed_lcias 1596 (counter_detailed_lcias + 1) 0 ) detailed_lcia_eq
      set detailed_lcia_res lput (matrix:get-column matrix:submatrix cumul_detailed_lcia 2715
      counter_detailed_lcias 2755 (counter_detailed_lcias + 1) 0 ) detailed_lcia_res
      set counter_detailed_lcias (counter_detailed_lcias + 1) ] ]
; here we compute the impact values:
  let cc_impact sum matrix:get-column (matrix:submatrix lcia_vector 0 0 175 1) 0
  ; climate change
  let ae_impact sum matrix:get-column (matrix:submatrix lcia_vector 175 0 868 1) 0
  ; aquatic ecotox
  let lo_impact sum matrix:get-column (matrix:submatrix lcia_vector 868 0 898 1) 0
  ; land occupation
  let tan_impact sum matrix:get-column (matrix:submatrix lcia_vector 898 0 917 1) 0
  ; Terrestrial acidification and nutrification
  let te_impact sum matrix:get-column (matrix:submatrix lcia_vector 917 0 1596 1) 0
  ; Terrestrial ecotox
  let ht_impact sum matrix:get-column (matrix:submatrix lcia_vector 1596 0 2233 1) 0
  ; Human toxicity
  let ir_impact sum matrix:get-column (matrix:submatrix lcia_vector 2233 0 2357 1) 0
  ; ionizing radiation
  let old_impact sum matrix:get-column (matrix:submatrix lcia_vector 2357 0 2451 1) 0
  ; ozone layer depletion
  let po_impact sum matrix:get-column (matrix:submatrix lcia_vector 2451 0 2687 1) 0
  ; photochemical oxidation
  let re_impact sum matrix:get-column (matrix:submatrix lcia_vector 2687 0 2715 1) 0
  ; Respiratory effects
  let rme_impact sum matrix:get-column (matrix:submatrix lcia_vector 2715 0 2748 1) 0
  ; Ressources mineral extraction
  let nre_impact sum matrix:get-column (matrix:submatrix lcia_vector 2748 0 2755 1) 0
  ; Non-renewable energy
  let lcia_values matrix:make-constant 12 1 0
  matrix:set lcia_values 0 0 ht_impact    ; Set up the lcia value matrix of hour q3
  matrix:set lcia_values 1 0 re_impact    ; start with human health categories:
  matrix:set lcia_values 2 0 ir_impact    ; .
  matrix:set lcia_values 3 0 old_impact   ; .
  matrix:set lcia_values 4 0 po_impact    ; Human Health...
  matrix:set lcia_values 5 0 ae_impact    ; ...then ecosystem quality:
  matrix:set lcia_values 6 0 te_impact    ; .
  matrix:set lcia_values 7 0 tan_impact   ; .
  matrix:set lcia_values 8 0 lo_impact    ; Ecosystem Quality...
  matrix:set lcia_values 9 0 cc_impact    ; ...Climate Change...
  matrix:set lcia_values 10 0 nre_impact  ; Ressources...
  matrix:set lcia_values 11 0 rme_impact  ; Ressources.
  matrix:set-column t_daily_lcia q3 matrix:get-column lcia_values 0
  set q2 (q2 + 1)
  set q3 (q3 + 1) ]
; This part of the code is in the GO procedure in order to recalculate
; emissions factors. For instance if the Technology matrix structure is changed,
; emissions factors can then be recalculated. It is interesting in order
; to model consequential LCA based on endogeneous parameters (e.g. adoption rate)
; (e.g. IF 90% agents adopt 3 bhvr --> delete column 4 tech matrix (coal).
; Or for instance add a column with individuals solar or battery if households
; buy them. We can also add more unit process to specify the process oil
; in order to acount for the type of oil (heavy or light) etc. Ideally the whole
; ecoinvent database should be implemented to allow more detailed LCAs.
end

to setup_freqcons
; Draw households' electricity demand profiles (see info tab)
let no_draw 0
ask turtles with [ color != grey ][
  set freqcons matrix:times 0 freqcons
  set default_freqcons matrix:times 0 default_freqcons
  let i 0
  while [ i < n_type_appliances ][
    ifelse i != dep_app [
      ifelse random-float 1 < (matrix:get daily_wd_freq 0 i
        - int matrix:get daily_wd_freq 0 i) [ ; Chance to use the appliance that day
        set no_draw (int matrix:get daily_wd_freq 0 i + 1) ][
        set no_draw (int matrix:get daily_wd_freq 0 i) ]
      ifelse matrix:get hh_app_distribution 6 i = 1 [
; Cold applianes and surface heating are randomly turn off (with "appliance-dice")
; those appliances considered used almost hourly
        matrix:set-row freqcons i matrix:get-row matrix:make-constant 1 24 1 0
        matrix:set-row default_freqcons i
          matrix:get-row matrix:make-constant 1 24 1 0
        let hour_drawn_index random round ((1000 / (24 - no_draw)))
; Not a pure random for all three numbers because we don't want twice the same hour.
; It is actually closer to reality as off cycle (for cold appliances and sheat)
; are usually equally distributed along the day (see below). Implicit assumption:
; cold appliances and surface heating are independant appliances.
        let j 0
        while [ j < (24 - no_draw) ][
          let hour_drawn (matrix:get hourly_proba i (hour_drawn_index
            + (j * (round (1000 / (24 - no_draw)) - 1))) - 1)
          matrix:set freqcons i hour_drawn (matrix:get freqcons i hour_drawn - 1)
          matrix:set default_freqcons i hour_drawn
            (matrix:get default_freqcons i hour_drawn - 1)
          set j (j + 1) ] ][
        let j 0
        while [ j < no_draw ][
          let hour_drawn_index random 1000
          let hour_drawn (matrix:get hourly_proba i hour_drawn_index - 1)
          matrix:set freqcons i hour_drawn (matrix:get freqcons i hour_drawn + 1)
          matrix:set default_freqcons i hour_drawn ; For the other appliance
            (matrix:get default_freqcons i hour_drawn + 1) ; randomly turn on
; Set up the hour of use of the battery. Assumption: the 8kWh are used during 2
; successive hours, as mentioned above the rest is sold under microfit program
          set j (j + 1) ] ] ][                           ; (with "appliance-dice")
      ifelse random-float 1 < (matrix:get daily_wd_freq 0 i
        - int matrix:get daily_wd_freq 0 i) [
        set no_draw (int matrix:get daily_wd_freq 0 i + 1) ][
        set no_draw (int matrix:get daily_wd_freq 0 i) ]
      let j 0
      while [ j < no_draw ][
        let hour_drawn_index random 1000
        let hour_drawn (matrix:get hourly_proba i hour_drawn_index - 1)
        matrix:set freqcons i hour_drawn (matrix:get freqcons i hour_drawn + 1)
        matrix:set default_freqcons i hour_drawn
          (matrix:get default_freqcons i hour_drawn + 1)
        if random-float 1 < (2 / 3) [  ; 2/3 chance dry used when cwash is (Paatero)
          ifelse hour_drawn < 22 [ set max_delay 3 ][ set max_delay 0 ]
; Assumption: maximum delay of 2 hours between cwash and dry, hence we assume
; dependant appliance start 0, 1  or 2 hours later.
          let delay_dep_app random max_delay
          matrix:set freqcons (i + 1) (hour_drawn + delay_dep_app)
            (matrix:get freqcons (i + 1) (hour_drawn + delay_dep_app) + 1)
          matrix:set default_freqcons (i + 1) (hour_drawn + delay_dep_app)
            (matrix:get default_freqcons (i + 1) (hour_drawn + delay_dep_app) + 1) ]
        set j (j + 1) ]
      set i (i + 1) ] ; Skip the line of dryer as it is dependent of washing machine
    set i (i + 1) ]
  set econs_inter matrix:times (matrix:submatrix
  appliances 0 0 1 n_type_appliances) freqcons
  set econs_inter_default matrix:times
    (matrix:submatrix appliances 0 0 1 n_type_appliances) freqcons
; Households "default profile" are drawn, they are used to compute impact
; reductions due to energy conservation and "peak shaving".
; The rest of the procedure prepare the load-shifting algorithm:
  ifelse impact_goal < 12 [
    set h_impact_inter (matrix:map * matrix:submatrix
      t_daily_lcia impact_goal 0 (impact_goal + 1) 24 econs_inter)
    set h_impact_inter_default (matrix:map * matrix:submatrix
      t_daily_lcia impact_goal 0 (impact_goal + 1) 24 econs_inter)
    set i_peak matrix:submatrix t_daily_lcia impact_goal 0 (impact_goal + 1) 24 ][
    if impact_goal = 12 [
      set t_daily_lcia_hh (matrix:plus (matrix:times
      matrix:get xf_endpoint_impact2002 0 0 matrix:submatrix t_daily_lcia 0 0 1 24)
      (matrix:times matrix:get xf_endpoint_impact2002 0 1
      matrix:submatrix t_daily_lcia 1 0 2 24)
      (matrix:times matrix:get xf_endpoint_impact2002 0 2
      matrix:submatrix t_daily_lcia 2 0 3 24)
      (matrix:times matrix:get xf_endpoint_impact2002 0 3
      matrix:submatrix t_daily_lcia 3 0 4 24)
      (matrix:times matrix:get xf_endpoint_impact2002 0 4
      matrix:submatrix t_daily_lcia 4 0 5 24))
      set h_impact_inter (matrix:map * t_daily_lcia_hh econs_inter)
      set h_impact_inter_default (matrix:map * t_daily_lcia_hh econs_inter)
      set i_peak t_daily_lcia_hh ]
    if impact_goal = 13 [
      set t_daily_lcia_eq (matrix:plus (matrix:times
      matrix:get xf_endpoint_impact2002 0 5 matrix:submatrix t_daily_lcia 5 0 6 24)
      (matrix:times matrix:get xf_endpoint_impact2002 0 6
      matrix:submatrix t_daily_lcia 6 0 7 24)
      (matrix:times matrix:get xf_endpoint_impact2002 0 7
      matrix:submatrix t_daily_lcia 7 0 8 24)
      (matrix:times matrix:get xf_endpoint_impact2002 0 8
      matrix:submatrix t_daily_lcia 8 0 9 24))
      set h_impact_inter (matrix:map * t_daily_lcia_eq econs_inter)
      set h_impact_inter_default (matrix:map * t_daily_lcia_eq econs_inter)
      set i_peak t_daily_lcia_eq ]
    if impact_goal = 14 [
      set t_daily_lcia_res (matrix:plus (matrix:times matrix:get
      xf_endpoint_impact2002 0 10 matrix:submatrix t_daily_lcia 10 0 11 24)
      (matrix:times matrix:get xf_endpoint_impact2002 0 11
      matrix:submatrix t_daily_lcia 11 0 12 24))
      set h_impact_inter (matrix:map * t_daily_lcia_res econs_inter)
      set h_impact_inter_default (matrix:map * t_daily_lcia_res econs_inter)
      set i_peak t_daily_lcia_res ]
    if impact_goal = 15 [
      set t_daily_lcia_hh (matrix:plus (matrix:times
      matrix:get xf_endpoint_impact2002 0 0 matrix:submatrix t_daily_lcia 0 0 1 24)
      (matrix:times matrix:get xf_endpoint_impact2002 0 1
      matrix:submatrix t_daily_lcia 1 0 2 24)
      (matrix:times matrix:get xf_endpoint_impact2002 0 2
      matrix:submatrix t_daily_lcia 2 0 3 24)
      (matrix:times matrix:get xf_endpoint_impact2002 0 3
      matrix:submatrix t_daily_lcia 3 0 4 24)
      (matrix:times matrix:get xf_endpoint_impact2002 0 4
      matrix:submatrix t_daily_lcia 4 0 5 24))
      set t_daily_lcia_eq (matrix:plus (matrix:times
      matrix:get xf_endpoint_impact2002 0 5 matrix:submatrix t_daily_lcia 5 0 6 24)
      (matrix:times matrix:get xf_endpoint_impact2002 0 6
      matrix:submatrix t_daily_lcia 6 0 7 24)
      (matrix:times matrix:get xf_endpoint_impact2002 0 7
      matrix:submatrix t_daily_lcia 7 0 8 24)
      (matrix:times matrix:get xf_endpoint_impact2002 0 8
      matrix:submatrix t_daily_lcia 8 0 9 24))
      set t_daily_lcia_res (matrix:plus (matrix:times matrix:get
      xf_endpoint_impact2002 0 10 matrix:submatrix t_daily_lcia 10 0 11 24)
      (matrix:times matrix:get xf_endpoint_impact2002 0 11
      matrix:submatrix t_daily_lcia 11 0 12 24))
      ifelse individual_vision = 0 [
        set t_daily_lcia_ss (matrix:plus
        (matrix:times t_daily_lcia_hh matrix:get single_score_impact2002 0 0
        matrix:get single_score_impact2002 vision 0) (matrix:times t_daily_lcia_eq
        matrix:get single_score_impact2002 0 1 matrix:get
        single_score_impact2002 vision 1) (matrix:times ((matrix:times
        matrix:submatrix t_daily_lcia 9 0 10 24
        matrix:get single_score_impact2002 0 2) matrix:+
        (matrix:times t_daily_lcia_res matrix:get single_score_impact2002 0 3))
        matrix:get single_score_impact2002 vision 2)) ][
; Set vision according to egalitarian, hierarchist or individualist perspectives.
        set t_daily_lcia_ss (matrix:plus (matrix:map * (matrix:times t_daily_lcia_hh
        matrix:get single_score_impact2002 0 0)
        matrix:submatrix turtle_vision 0 0 1 24)
        (matrix:map * (matrix:times t_daily_lcia_eq matrix:get
        single_score_impact2002 0 1) matrix:submatrix turtle_vision 1 0 2 24)
        (matrix:map * ((matrix:times matrix:submatrix t_daily_lcia 9 0 10 24
        matrix:get single_score_impact2002 0 2)
        matrix:+ (matrix:times t_daily_lcia_res matrix:get
        single_score_impact2002 0 3)) matrix:submatrix turtle_vision 2 0 3 24)) ]
; Set vision according to simplistic O model.
      set h_impact_inter (matrix:map * t_daily_lcia_ss econs_inter)
      set h_impact_inter_default (matrix:map * t_daily_lcia_ss econs_inter)
      set i_peak t_daily_lcia_ss ] ]
  ifelse price_scenario != 3 [
    set c_peak hourly_cost ][
    set c_peak hourly_cost2 ]
  let re_ec_def matrix:times (matrix:submatrix
  appliances_default2 0 0 1 n_type_appliances) default_freqcons
  let re_ec matrix:times (matrix:submatrix
  appliances 0 0 1 n_type_appliances) freqcons
  let re_savings 15
  ifelse price_scenario != 3 [
    set re_savings (matrix:map * hourly_cost (matrix:map - re_ec_def re_ec)) ][
    set re_savings (matrix:map * hourly_cost2 (matrix:map - re_ec_def re_ec)) ]
  let impact_rebound matrix:times impact_consum matrix:transpose consum
; In kgCO2/$ (impact_consum in $/kWh and consum is a ratio)
  ifelse re_impact_goal < 8 [
    set impact_rebound (matrix:get impact_rebound re_impact_goal 0) ][
    if re_impact_goal = 8 [
      set impact_rebound (matrix:get impact_rebound 0 0 *
      matrix:get xf_endpoint_impact2002 0 0) + (matrix:get impact_rebound 1 0
      * matrix:get xf_endpoint_impact2002 0 1) + (matrix:get impact_rebound 2 0
      * matrix:get xf_endpoint_impact2002 0 3) + (matrix:get impact_rebound 3 0
      * matrix:get xf_endpoint_impact2002 0 4) ] ; human health
    if re_impact_goal = 9 [
      set impact_rebound (matrix:get impact_rebound 4 0 *
      matrix:get xf_endpoint_impact2002 0 5) + (matrix:get impact_rebound 5 0
      * matrix:get xf_endpoint_impact2002 0 6) + (matrix:get impact_rebound 6 0
      * matrix:get xf_endpoint_impact2002 0 7) ] ; ecosystem quality
    if re_impact_goal = 10 [
      ifelse individual_vision = 0 [
      set impact_rebound ((((matrix:get impact_rebound 0 0 *
      matrix:get xf_endpoint_impact2002 0 0) + (matrix:get impact_rebound 1 0
      * matrix:get xf_endpoint_impact2002 0 1) + (matrix:get impact_rebound 2 0
      * matrix:get xf_endpoint_impact2002 0 3) + (matrix:get impact_rebound 3 0
      * matrix:get xf_endpoint_impact2002 0 4)) * matrix:get
      single_score_impact2002 0 0 * matrix:get single_score_impact2002 vision 0)
      + (((matrix:get impact_rebound 4 0 * matrix:get xf_endpoint_impact2002 0 5)
      + (matrix:get impact_rebound 5 0 * matrix:get xf_endpoint_impact2002 0 6)
      + (matrix:get impact_rebound 6 0 * matrix:get xf_endpoint_impact2002 0 7))
      * matrix:get single_score_impact2002 0 1 * matrix:get
      single_score_impact2002 vision 1) + (matrix:get impact_rebound 7 0
      * matrix:get single_score_impact2002 0 2
      * matrix:get single_score_impact2002 vision 2)) ][
      set impact_rebound ((matrix:times (((matrix:get impact_rebound 0 0 *
      matrix:get xf_endpoint_impact2002 0 0) + (matrix:get impact_rebound 1 0
      * matrix:get xf_endpoint_impact2002 0 1) + (matrix:get impact_rebound 2 0
      * matrix:get xf_endpoint_impact2002 0 3) + (matrix:get impact_rebound 3 0
      * matrix:get xf_endpoint_impact2002 0 4)) * matrix:get
      single_score_impact2002 0 0) matrix:submatrix turtle_vision 0 0 1 24)
      matrix:+ (matrix:times (((matrix:get impact_rebound 4 0
      * matrix:get xf_endpoint_impact2002 0 5)
      + (matrix:get impact_rebound 5 0 * matrix:get xf_endpoint_impact2002 0 6)
      + (matrix:get impact_rebound 6 0 * matrix:get xf_endpoint_impact2002 0 7))
      * matrix:get single_score_impact2002 0 1)
      matrix:submatrix turtle_vision 1 0 2 24) matrix:+
      (matrix:times (matrix:get impact_rebound 7 0 * matrix:get
      single_score_impact2002 0 2) matrix:submatrix turtle_vision 2 0 3 24)) ] ] ]
; Use this configuration preferentially with RE2ndAssumption? ON.
  ifelse individual_vision = 0 [
    set re_impact_inter (matrix:map + h_impact_inter
    (matrix:times impact_rebound re_savings))
    set re_impact_inter_default (matrix:map + h_impact_inter
    (matrix:times impact_rebound re_savings)) ][
    set re_impact_inter (matrix:map + h_impact_inter
    (matrix:times impact_rebound re_savings))
    set re_impact_inter_default (matrix:map + h_impact_inter
    (matrix:times impact_rebound re_savings)) ] ]
set e_peak matrix:make-constant 1 24 0
set i_peak matrix:make-constant 1 24 0
set c_peak matrix:make-constant 1 24 0
set re_peak matrix:make-constant 1 24 0
; Will allow load shifting or "peak shaving" according to goal 1 and 4
; Both will attend to diminish peaks. Beware that with goal 4 we diminish
; Impact peaks but not necesseraly the impact itself
; For goal 2 and 3 we simply find the worst period to consume electricity
let m 0
while [ m < nt ][
  ask turtle m [
    set e_peak (matrix:map + e_peak econs_inter) ]
    set m (m + 1) ]
if price_scenario = 3 [
  let real_cost_data (matrix:map * e_peak hourly_cost)
  let d_average_price (sum matrix:get-row real_cost_data 0
  / sum matrix:get-row e_peak 0)
  let d_median_cons median matrix:get-row e_peak 0
  let linear_param d_average_price / d_median_cons
  let diff_cons matrix:minus e_peak d_median_cons
  set hourly_cost2 matrix:plus (matrix:times linear_param diff_cons) d_average_price
  set c_peak (matrix:map * hourly_cost2 e_peak)
  let prop_factor (sum matrix:get-row c_peak 0 / sum matrix:get-row real_cost_data 0)
  set hourly_cost2 matrix:times hourly_cost2  (1 / prop_factor)
  set c_peak (matrix:map * hourly_cost2 e_peak) ]
end

to setup_loadshift
; Change households' electricity demand profiles by shifting part of the load
; (peak shaving) (see info tab). Below is the simple optimization procedure which
; allow peak shaving. As choices are made individually by turtles without any
; specific order, the optimization procedure is similar to a greedy algorithm.
; See below for the procedure and info tab for more details.
; For goal 1 and 4 it is the temporal variability (of electricity consumption
; or impact with RE) that is minimized while for goal 2 and 3 we minimize the
; amount of metric itself (cost or impact)
ask turtles with [ color != grey AND n_bhvr != 0 ][
  let app_index_shift matrix:copy shift_bhvr
  let n 0
  while [ n < 11 ][
    let app_index_value 15
    ifelse n < 5 [
      set app_index_value matrix:get app_index_shift 0 n ][
      ifelse n < 7 [
        set app_index_value (n - 4) ][
        set app_index_value (n + 1) ] ]
    if app_index_value != 15 [
      let sorted_list_inter [ ]
      set i_peak matrix:submatrix t_daily_lcia impact_goal
        0 (impact_goal + 1) 24
      set re_peak matrix:submatrix t_daily_lcia impact_goal
        0 (impact_goal + 1) 24
      if goal = 1 [ set sorted_list_inter sort-by > matrix:get-row e_peak 0 ]
      if goal = 2 [ set sorted_list_inter sort-by > matrix:get-row i_peak 0 ]
      if goal = 3 [ set sorted_list_inter sort-by > matrix:get-row c_peak 0 ]
      if goal = 4 [ set sorted_list_inter sort-by > matrix:get-row re_peak 0 ]
  ; Allow turtles to access the global kW, $, CO2 etc. hourly profile
  ; (of current day) prepared at the precedent procedure. Based on those
  ; information turtles can apply an optimization procedure.
      let max_freqcons first sorted_list_inter
      let position_max 25
      if goal = 1 [ set position_max position max_freqcons matrix:get-row e_peak 0 ]
      if goal = 2 [ set position_max position max_freqcons matrix:get-row i_peak 0 ]
      if goal = 3 [ set position_max position max_freqcons matrix:get-row c_peak 0 ]
      if goal = 4 [ set position_max position max_freqcons matrix:get-row re_peak 0 ]
  ; Find peak
      let freq_value matrix:get freqcons app_index_value position_max
      if freq_value = 0 [
        if reduce + (matrix:get-row freqcons app_index_value) != 0 [
          let i 1
          let j 1
          let position_max3 position_max
          while [ freq_value = 0 ][
            let right_freq? false
            let left_freq? false
            if (position_max3 + i) < 24 [
              set freq_value matrix:get freqcons app_index_value (position_max3 + i)
              set position_max (position_max3 + i)
              if freq_value != 0 [ set right_freq? true ]
              set i (i + 1) ] ; Goes to right and left of peak to find a 1 value
            if (position_max3 - j) >= 0 [
              if freq_value = 0 [
                set freq_value matrix:get freqcons app_index_value
                (position_max3 - j) set position_max (position_max3 - j) ]
              if matrix:get freqcons app_index_value (position_max3 - j) != 0 [
                set left_freq? true ]
              set j (j + 1) ]
            if left_freq? AND right_freq? [
              ifelse (who / 2) = int (who / 2) [
                set freq_value matrix:get freqcons app_index_value
                (position_max3 + (i - 1))
                set position_max (position_max3 + (i - 1)) ][
                set freq_value matrix:get freqcons app_index_value
                (position_max3 - (j - 1))
                set position_max (position_max3 - (j - 1)) ] ] ] ] ]
  ; By convention if position max is 0 and surounded by two value !=0,
  ; the model choose value on the left if who turtle odd and on the right if who is
  ; even (e.g. if odd 0 0 1 0 1 0 0 --> new position_max 0 0 1 0). This prevent again
  ; the optimization procedure to get a global optimum.
      if freq_value > 1 [ set freq_value 1 ]
      let load 25
      if goal = 1 [ set load (matrix:get appliances 0 app_index_value * freq_value) ]
      let shift_scenario matrix:get hh_app_distribution 7 app_index_value
      let lower_limit 0 ; shift_scenario gives appliances constraints
      let upper_limit 0
      ifelse (position_max > 0) AND (position_max < 23) [
        if shift_scenario = 1 [ set lower_limit 1 set upper_limit 2 ]
        if shift_scenario = 2 [ set lower_limit 0 set upper_limit 2 ]
        if shift_scenario = 3 [ set lower_limit position_max
          set upper_limit (24 - position_max) ] ][
        if position_max < 1 [
          if shift_scenario = 1 [ set lower_limit 0 set upper_limit 2 ]
          if shift_scenario = 2 [ set lower_limit 0 set upper_limit 2 ]
          if shift_scenario = 3 [ set lower_limit position_max
            set upper_limit (24 - position_max) ] ]
        if position_max > 22 [
          if shift_scenario = 1 [ set lower_limit 1 set upper_limit 1 ]
          if shift_scenario = 2 [ set lower_limit 0 set upper_limit 1 ]
          if shift_scenario = 3 [ set lower_limit position_max
            set upper_limit (24 - position_max) ] ] ] ; Either -1/+1 +1 or +undefined
      let hour_of_concern 25
      if goal = 1 [
      set hour_of_concern matrix:submatrix e_peak 0 (position_max - lower_limit) 1
      (position_max + upper_limit) ]
      if goal = 2 [
      set hour_of_concern matrix:submatrix i_peak 0 (position_max - lower_limit) 1
      (position_max + upper_limit) ]
      if goal = 3 [
      set hour_of_concern matrix:submatrix c_peak 0 (position_max - lower_limit) 1
      (position_max + upper_limit) ]
      if goal = 4 [
      set hour_of_concern matrix:submatrix re_peak 0 (position_max - lower_limit) 1
      (position_max + upper_limit) ]
  ; Hence, hour_of_concern is a range of hours including position max which depends
  ; on constraints that each appliances are subject to, and depends on the O metric.
      if goal = 2 [
        ifelse impact_goal < 12 [
          set i_factor_value matrix:submatrix t_daily_lcia impact_goal
          (position_max - lower_limit) (impact_goal + 1)
          (position_max + upper_limit) ][
          if impact_goal = 12 [
            set i_factor_value matrix:submatrix t_daily_lcia_hh 0
            (position_max - lower_limit) 1 (position_max + upper_limit) ]
          if impact_goal = 13 [
            set i_factor_value matrix:submatrix t_daily_lcia_eq 0
            (position_max - lower_limit) 1 (position_max + upper_limit) ]
          if impact_goal = 14 [
            set i_factor_value matrix:submatrix t_daily_lcia_res 0
            (position_max - lower_limit) 1 (position_max + upper_limit) ]
          if impact_goal = 15 [
            set i_factor_value matrix:submatrix t_daily_lcia_ss 0
            (position_max - lower_limit) 1 (position_max + upper_limit) ] ]
        set load matrix:times
        (matrix:get appliances 0 app_index_value * freq_value) i_factor_value
        set load i_factor_value ]
      if goal = 3 [
        let c_factor_value 15
        ifelse price_scenario != 3 [
          set c_factor_value matrix:submatrix hourly_cost 0
          (position_max - lower_limit) 1 (position_max + upper_limit) ][
          set c_factor_value matrix:submatrix hourly_cost2 0
          (position_max - lower_limit) 1 (position_max + upper_limit) ]
        set load matrix:times (matrix:get appliances 0 app_index_value * freq_value)
        c_factor_value
        set load c_factor_value ]
      let re_ec_def 15
      let re_ec 15
      let re_savings 15
      let impact_rebound 15
      if goal = 4 [
        set i_factor_value matrix:submatrix t_daily_lcia impact_goal
        (position_max - lower_limit) (impact_goal + 1)
        (position_max + upper_limit)
        let current_cost item position_max matrix:get-row hourly_cost 0
        set re_savings matrix:minus current_cost hourly_cost
        set impact_rebound matrix:times impact_consum matrix:transpose consum
        set impact_rebound (matrix:get impact_rebound re_impact_goal 0)
        let re_factor_value matrix:submatrix
        (matrix:times impact_rebound re_savings) 0 (position_max - lower_limit) 1
        (position_max + upper_limit)
        set load (matrix:map + i_factor_value re_factor_value) ]
  ; Build the load vector which allows us to know the load of the
  ; new hour the appliance is used.
      let future_cons 25
      ifelse goal = 1 [ set future_cons matrix:plus load hour_of_concern ][
        set future_cons load ]
      let sorted_future_cons matrix:get-row future_cons 0
      ifelse n != 10 [
        set sorted_future_cons sort-by < matrix:get-row future_cons 0 ][
        set sorted_future_cons sort-by > matrix:get-row future_cons 0 ]
      ifelse goal = 1 [
        let min_future_cons first sorted_future_cons
        let position_new_freq position min_future_cons matrix:get-row future_cons 0
        matrix:set freqcons app_index_value position_max (matrix:get freqcons
        app_index_value position_max - freq_value)
        let old_freq matrix:get freqcons app_index_value (position_max - lower_limit
        + position_new_freq)
        matrix:set freqcons app_index_value (position_max - lower_limit
        + position_new_freq) (old_freq + freq_value)
        set econs_inter matrix:times (matrix:submatrix
        appliances 0 0 1 n_type_appliances) freqcons ][
        ifelse goal = 3 [
          if price_scenario != 2 [
            let min_future_cons first sorted_future_cons
            let position_new_freq position min_future_cons matrix:get-row future_cons 0
            matrix:set freqcons app_index_value position_max (matrix:get freqcons
            app_index_value position_max - freq_value)
            let old_freq matrix:get freqcons app_index_value (position_max - lower_limit
            + position_new_freq)
            matrix:set freqcons app_index_value (position_max - lower_limit
            + position_new_freq) (old_freq + freq_value)
            set econs_inter matrix:times (matrix:submatrix
            appliances 0 0 1 n_type_appliances) freqcons ] ] [
          if mix_scenario != 2 [
            let min_future_cons first sorted_future_cons
            let position_new_freq position min_future_cons matrix:get-row future_cons 0
            matrix:set freqcons app_index_value position_max (matrix:get freqcons
            app_index_value position_max - freq_value)
            let old_freq matrix:get freqcons app_index_value (position_max - lower_limit
            + position_new_freq)
            matrix:set freqcons app_index_value (position_max - lower_limit
            + position_new_freq) (old_freq + freq_value)
            set econs_inter matrix:times (matrix:submatrix
            appliances 0 0 1 n_type_appliances) freqcons ] ] ]
  ; Modify the appliance time-of-use matrix to account for the shift.
  ; Below global kW, $, CO2 etc. hourly profiles are updated (dynamic optimization)
      if goal = 1 [
        set e_peak (matrix:map - e_peak econs_inter_default)
        set e_peak (matrix:map + e_peak econs_inter) ] ]
    ifelse n < 6 [
      ifelse n < 4 [
        set n (n + 1) ][
        ifelse bhvr_set = 3 [
          ifelse not h&c_opti? [ set n (n + 6) ] [ set n (n + 3) ] ][
          set n (n + 1) ] ] ] [
        ifelse not h&c_opti? [
          set n (n + 4) ][
          set n (n + 1) ] ] ] ]
set e_peak matrix:make-constant 1 24 0
let comptK 0
while [ comptK < nt ][
  ask turtle comptK  [
    set e_peak (matrix:map + e_peak econs_inter) ]
  set comptK  (comptK  + 1) ]
if price_scenario = 3 [
  let real_cost_data (matrix:map * e_peak hourly_cost)
  let d_average_price (sum matrix:get-row real_cost_data 0
  / sum matrix:get-row e_peak 0)
  let d_median_cons median matrix:get-row e_peak 0
  let linear_param d_average_price / d_median_cons
  let diff_cons matrix:minus e_peak d_median_cons
  set hourly_cost2 matrix:plus (matrix:times linear_param diff_cons) d_average_price
  set c_peak (matrix:map * hourly_cost2 e_peak)
  let prop_factor (sum matrix:get-row c_peak 0 / sum matrix:get-row real_cost_data 0)
  set hourly_cost2 matrix:times hourly_cost2  (1 / prop_factor)
  set c_peak (matrix:map * hourly_cost2 e_peak) ]
end

to social_interactions
; The social psychological model of Byrka et al. (2016) is used to determine the
; the adoption of new behaviors. It combines a q-voter model and the
; Campbell's paradigm of Kaiser et al. (2010). See info tab for more details.
if nticks = 0 [
  ask turtles with [ color != grey AND n_bhvr > 0 ][
    set P_sbhvr (((P_sbhvr / (1 - P_sbhvr)) * exp (-1 * diff_discrepancy))
    / (((P_sbhvr / (1 - P_sbhvr)) * exp (-1 * diff_discrepancy)) + 1)) ] ]
; Update P_sbhvr of innovators
; To visualize adoption of new behaviors by the housholds
ask passives [ if (n_bhvr = 0) [ set color blue ] ]
ask frugals [ if (n_bhvr = 0) [ set color magenta ] ]
ask epicures [ if (n_bhvr = 0) [ set color (brown + 1) ] ]
ask stalwarts [ if (n_bhvr = 0) [ set color green ] ]
ask turtles with [color != grey ] [
  if n_bhvr = 1 [ set color yellow ]
  if n_bhvr = 2 [ set color orange ]
  if n_bhvr = 3 [ set color red ]
  if n_bhvr = 4 [ set color white ]
  if n_bhvr = 5 [ set color (lime + 3) ] ]
; Assumptions and explanations:
; Agents take actions with a certain probability. This probability is determined
; following Byrka's social psychological model. The main difference of present model
; compare to Byrka's is that agents can adopt several new behaviors instead of one.
; Another difference is that the probability that an agent take an action on its own
; (independantly of the states of its neighbors) is conditionned by its
; performance: agents compare their consumption to the minimum consumption
; in their "neighborhood", if they consume more than this value there
; is a chance that they take an action (on their own) (assumption that feedback
; provided in the smart building is comparative (often the case see
; e.g. Asensio et al. (2015) or Ehrhardt-Martinez et al. (2010))).
if (nticks / feedback_frequency_value) =
  int (nticks / feedback_frequency_value) AND (nticks != 0) [
  let cumul_cons_turtles sort-by < ([ cumul_cons ] of turtles with [ color != grey ])
  set min_cons item 0 cumul_cons_turtles
; Similarly min_cost or min_impact etc. could be defined and agents could
; compare themselves on this basis.
  ask turtles with [ color != grey ] [
; q-voter model
    let dice random-float 1
    ifelse dice < P_conform [
      let n_neighbors (count my-neighbor_links)
      ifelse (n_neighbors >= 4) AND (q-model = 4) [
        let neighbors_bhvr ([ n_bhvr ] of n-of 4 neighbor_link-neighbors)
        if (item 0 neighbors_bhvr > n_bhvr) AND (item 1 neighbors_bhvr > n_bhvr)
        AND (item 2 neighbors_bhvr > n_bhvr) AND (item 3 neighbors_bhvr > n_bhvr)
        AND (n_bhvr < n_bhvr_max) [
          set n_bhvr (n_bhvr + 1)
          set P_sbhvr (((P_sbhvr / (1 - P_sbhvr)) * exp (-1 * diff_discrepancy))
          / (((P_sbhvr / (1 - P_sbhvr)) * exp (-1 * diff_discrepancy)) + 1))
          set active? true ]
        if (item 0 neighbors_bhvr < n_bhvr) AND (item 1 neighbors_bhvr < n_bhvr)
        AND (item 2 neighbors_bhvr < n_bhvr) AND (item 3 neighbors_bhvr < n_bhvr)
        AND (n_bhvr > 0) [
          set n_bhvr (n_bhvr - 1)
          set P_sbhvr ((P_sbhvr / (1 - P_sbhvr))
          / (exp (-1 * diff_discrepancy) + (P_sbhvr / (1 - P_sbhvr)))) ; see info tab
          set give_up? true ] ][
        if (n_neighbors = 3) OR (q-model = 3) [
          let neighbors_bhvr ([ n_bhvr ] of n-of n_neighbors neighbor_link-neighbors)
          if (item 0 neighbors_bhvr > n_bhvr) AND (item 1 neighbors_bhvr > n_bhvr)
          AND (item 2 neighbors_bhvr > n_bhvr) AND (n_bhvr < n_bhvr_max) [
            set n_bhvr (n_bhvr + 1)
            set P_sbhvr (((P_sbhvr / (1 - P_sbhvr)) * exp (-1 * diff_discrepancy))
            / (((P_sbhvr / (1 - P_sbhvr)) * exp (-1 * diff_discrepancy)) + 1))
            set active? true ]
          if (item 0 neighbors_bhvr < n_bhvr) AND (item 1 neighbors_bhvr < n_bhvr)
          AND (item 2 neighbors_bhvr < n_bhvr) AND (n_bhvr > 0) [
            set n_bhvr (n_bhvr - 1)
            set P_sbhvr ((P_sbhvr / (1 - P_sbhvr))
            / (exp (-1 * diff_discrepancy) + (P_sbhvr / (1 - P_sbhvr))))
            set give_up? true ] ]
        if (n_neighbors = 2) OR (q-model = 2) [
          let neighbors_bhvr ([ n_bhvr ] of n-of n_neighbors neighbor_link-neighbors)
          if (item 0 neighbors_bhvr > n_bhvr) AND (item 1 neighbors_bhvr > n_bhvr)
          AND (n_bhvr < n_bhvr_max) [
            set n_bhvr (n_bhvr + 1)
            set P_sbhvr (((P_sbhvr / (1 - P_sbhvr)) * exp (-1 * diff_discrepancy))
            / (((P_sbhvr / (1 - P_sbhvr)) * exp (-1 * diff_discrepancy)) + 1))
            set active? true ]
          if (item 0 neighbors_bhvr < n_bhvr) AND (item 1 neighbors_bhvr < n_bhvr)
          AND (n_bhvr > 0) [
            set n_bhvr (n_bhvr - 1)
            set P_sbhvr ((P_sbhvr / (1 - P_sbhvr))
            / (exp (-1 * diff_discrepancy) + (P_sbhvr / (1 - P_sbhvr))))
            set give_up? true ] ]
        if (n_neighbors = 1) OR (q-model = 1) [
          let neighbors_bhvr ([ n_bhvr ] of n-of n_neighbors neighbor_link-neighbors)
          if (item 0 neighbors_bhvr > n_bhvr) AND (n_bhvr < n_bhvr_max) [
            set n_bhvr (n_bhvr + 1)
            set P_sbhvr (((P_sbhvr / (1 - P_sbhvr)) * exp (-1 * diff_discrepancy))
            / (((P_sbhvr / (1 - P_sbhvr)) * exp (-1 * diff_discrepancy)) + 1))
            set active? true ]
          if (item 0 neighbors_bhvr < n_bhvr) AND (n_bhvr > 0) [
            set n_bhvr (n_bhvr - 1)
            set P_sbhvr ((P_sbhvr / (1 - P_sbhvr))
            / (exp (-1 * diff_discrepancy) + (P_sbhvr / (1 - P_sbhvr))))
            set give_up? true ] ] ] ][
; Agent's own decision
; 0.01 to avoid rounding issues
; Similarly min_cost or min_impact etc. could be defined and agents could
; compare themselves on this basis.
      if (cumul_cons - min_cons) > 0.01 AND (n_bhvr < n_bhvr_max) [
        let dice2 random-float 1
        if dice2 < P_sbhvr [
          set n_bhvr (n_bhvr + 1)
          set P_sbhvr (((P_sbhvr / (1 - P_sbhvr)) * exp (-1 * diff_discrepancy))
          / (((P_sbhvr / (1 - P_sbhvr)) * exp (-1 * diff_discrepancy)) + 1))
          set active? true ] ] ]
 set cumul_cons 0 ] ] ; e.g we could use "set cumul_cost 0" and "set cumul_impact 0"
end

to electric_consumption_profile
; Compute all outputs from model' building blocks:
; i) freqcons - binary matrix summarizing information on when each appliances is used
; during a day; ii) appliances matrices - which contains amounts of energy consumed
; by each appliances when they are used; iii) hourly_cost vectors - contains
; electricity prices; iv) t_daily_lca - contain all impact factors.
ask turtles with [ color != grey AND n_bhvr = 0 ] [
  let j 0
  while [ j < 24 ][
    matrix:set-column econst j (map * matrix:get-row appliances 0
    matrix:get-column freqcons j)
    matrix:set-column default_econs j (map * matrix:get-row appliances_default2 0
    matrix:get-column default_freqcons j)
    set j (j + 1) ]
; Avoid that the electric consumption end up being negative due to the battery use:
; the battery is used untill being empty (for those time of use (the battery can
; be used twice during the day (4 kWh production each time)) the surplus is sold
; under microfit program.
  let jbis 0
  while [ jbis < 24 ][
    let column_w_issue 25
    let default_column_w_issue 25
    if (reduce + matrix:get-column econst jbis) < 0 [ set column_w_issue jbis ]
    if (reduce + matrix:get-column default_econs jbis) < 0 [
      if PV&Battery_scenario = 2 [ set default_column_w_issue jbis ] ]
    if column_w_issue != 25 [
      let counterPV 0
      while [ (reduce + matrix:get-column econst column_w_issue) < 0 AND
      counterPV < 10 ] [
        let local_surplus reduce + matrix:get-column econst column_w_issue
        matrix:set econst 11 column_w_issue (matrix:get econst 11
          column_w_issue - local_surplus)
        ifelse 23 - jbis < 11 [ matrix:set econst 11 (column_w_issue - 1)
          (matrix:get econst 11 (column_w_issue - 1) + local_surplus)
          set column_w_issue (column_w_issue - 1) ][
          matrix:set econst 11 (column_w_issue + 1)
          (matrix:get econst 11 (column_w_issue + 1) + local_surplus)
          set column_w_issue (column_w_issue + 1) ]
        set counterPV (counterPV + 1) ] ]
    if default_column_w_issue != 25 [
      let counterPV 0
      while [ (reduce + matrix:get-column default_econs default_column_w_issue)
        < 0 AND counterPV < 10 ] [
        let local_surplus reduce + matrix:get-column default_econs
        default_column_w_issue
        matrix:set default_econs 11 default_column_w_issue
        (matrix:get default_econs 11 default_column_w_issue - local_surplus)
        ifelse 23 - jbis < 11 [ matrix:set default_econs 11 (default_column_w_issue - 1)
          (matrix:get default_econs 11 (default_column_w_issue - 1)
            + local_surplus)
          set default_column_w_issue (default_column_w_issue - 1) ][
          matrix:set default_econs 11 (default_column_w_issue + 1)
          (matrix:get default_econs 11 (default_column_w_issue + 1)
            + local_surplus)
          set default_column_w_issue (default_column_w_issue + 1) ]
        set counterPV (counterPV + 1) ] ]
    set jbis (jbis + 1) ]
  set econst matrix:times (1 / 365) econst ; Transforms annual data to daily ones
  set default_econs matrix:times (1 / 365) default_econs
; To account for performance, first step : setup default emissions per tick
  let k 0
  while [ k < n_type_appliances ][
    matrix:set-row ht_h_impact k (map * matrix:get-row t_daily_lcia 0
    matrix:get-row econst k)
    matrix:set-row ht_default_impact k (map * matrix:get-row t_daily_lcia 0
    matrix:get-row default_econs k)  ; in kgC2H3Cleq
    matrix:set-row re_h_impact k (map * matrix:get-row t_daily_lcia 1
    matrix:get-row econst k)
    matrix:set-row re_default_impact k (map * matrix:get-row t_daily_lcia 1
    matrix:get-row default_econs k)  ; in kg PM2.5eq
    matrix:set-row ir_h_impact k (map * matrix:get-row t_daily_lcia 2
    matrix:get-row econst k)
    matrix:set-row ir_default_impact k (map * matrix:get-row t_daily_lcia 2
    matrix:get-row default_econs k)  ; in Bq C-14eq
    matrix:set-row old_h_impact k (map * matrix:get-row t_daily_lcia 3
    matrix:get-row econst k)
    matrix:set-row old_default_impact k (map * matrix:get-row t_daily_lcia 3
    matrix:get-row default_econs k)  ; in kgCFC11eq
    matrix:set-row po_h_impact k (map * matrix:get-row t_daily_lcia 4
    matrix:get-row econst k)
    matrix:set-row po_default_impact k (map * matrix:get-row t_daily_lcia 4
    matrix:get-row default_econs k)  ; in kgC2H4eq
    matrix:set-row ae_h_impact k (map * matrix:get-row t_daily_lcia 5
    matrix:get-row econst k)
    matrix:set-row ae_default_impact k (map * matrix:get-row t_daily_lcia 5
    matrix:get-row default_econs k)  ; in kg TEG water
    matrix:set-row te_h_impact k (map * matrix:get-row t_daily_lcia 6
    matrix:get-row econst k)
    matrix:set-row te_default_impact k (map * matrix:get-row t_daily_lcia 6
    matrix:get-row default_econs k)  ; in kg TEG soil
    matrix:set-row tan_h_impact k (map * matrix:get-row t_daily_lcia 7
    matrix:get-row econst k)
    matrix:set-row tan_default_impact k (map * matrix:get-row t_daily_lcia 7
    matrix:get-row default_econs k)  ; in kg So2eq
    matrix:set-row lo_h_impact k (map * matrix:get-row t_daily_lcia 8
    matrix:get-row econst k)
    matrix:set-row lo_default_impact k (map * matrix:get-row t_daily_lcia 8
    matrix:get-row default_econs k)  ; in m2org.arable
    matrix:set-row cc_h_impact k (map * matrix:get-row t_daily_lcia 9
    matrix:get-row econst k)
    matrix:set-row cc_default_impact k (map * matrix:get-row t_daily_lcia 9
    matrix:get-row default_econs k)  ; in kgCO2eq
    matrix:set-row nre_h_impact k (map * matrix:get-row t_daily_lcia 10
    matrix:get-row econst k)
    matrix:set-row nre_default_impact k (map * matrix:get-row t_daily_lcia 10
    matrix:get-row default_econs k)  ; in MJ primary
    matrix:set-row rme_h_impact k (map * matrix:get-row t_daily_lcia 11
    matrix:get-row econst k)
    matrix:set-row rme_default_impact k (map * matrix:get-row t_daily_lcia 11
    matrix:get-row default_econs k)  ; in MJ surplus
    ifelse price_scenario != 3 [
      matrix:set-row cost k (map * matrix:get-row hourly_cost 0
      matrix:get-row econst k)
      matrix:set-row default_cost k (map * matrix:get-row hourly_cost 0
      matrix:get-row default_econs k) ][ ; in $
      matrix:set-row cost k (map * matrix:get-row hourly_cost2 0
      matrix:get-row econst k)
      matrix:set-row default_cost k (map * matrix:get-row hourly_cost2 0
      matrix:get-row default_econs k) ]  ; in $
    set k (k + 1) ]
; The production sold under microfit allows to reduce agents' electricity bill
  matrix:set-row cost 11 (map + (matrix:get-row cost 11)
    matrix:get-row PV_surplus 0)
  matrix:set-row default_cost 11 (map + (matrix:get-row default_cost 11)
    matrix:get-row default_PV_surplus 0) ]
; To account for performance, second step: consequences of agents' behavior change
; Data are from multiple source : bhvrs concern each appliance and the probability
; that a bhvr concern an appliance is based on frequencies reported by ACEEE report
; (Ehrhardt-Martinez et al. (2010)) (for bhvr_set = 1), (see info tab).
ask turtles with [ color != grey AND n_bhvr != 0 ] [
  let HDD_value matrix:get HDD_% 0 nd
  let CDD_value matrix:get CDD_% 0 nd ; To be able to change bhvrs w/ weather
  if active? [
    let mat_inter_w matrix:copy p_bhvr_winter
    let mat_inter_s matrix:copy p_bhvr_summer
    let i 0
    while [ i < (n_type_appliances - 1) ] [
      let criteria reduce + matrix:get-column appliances i
      if criteria = 0 [ matrix:set mat_inter_w 0 i 0 matrix:set mat_inter_s 0 i 0 ]
      set i (i + 1) ]
    let nvx_tot reduce + matrix:get-row mat_inter_w 0
    let nvx_tot2 reduce + matrix:get-row mat_inter_s 0
    set mat_inter_w matrix:times (1 / nvx_tot) mat_inter_w
    set mat_inter_s matrix:times (1 / nvx_tot2) mat_inter_s
; Re-define probability matrices to adapt to each agent set of appliances
    let mat_rwheel_w matrix:make-constant 2 (n_type_appliances - 1) 0
    let mat_rwheel_s matrix:make-constant 2 (n_type_appliances - 1) 0
    let list_prob_w sort-by < matrix:get-row mat_inter_w 0
    matrix:set-row mat_rwheel_w 1 list_prob_w
    let list_prob_s sort-by < matrix:get-row mat_inter_s 0
    matrix:set-row mat_rwheel_s 1 list_prob_s
    let k 0
    while [ k < (n_type_appliances - 1) ][
      let prob_w item k list_prob_w
      let prob_s item k list_prob_s
      let position_prob_w position prob_w matrix:get-row mat_inter_w 0
      let position_prob_s position prob_s matrix:get-row mat_inter_s 0
      matrix:set mat_rwheel_w 0 k position_prob_w
      matrix:set mat_rwheel_s 0 k position_prob_s ; Order prob to match appliances
      set k (k + 1) ]             ; index order (i = 0 for s&o, i = 8 for sheat etc.
    let j 0
    while [ j < 10 ][
      matrix:set mat_rwheel_w 1 (j + 1)
      (matrix:get mat_rwheel_w 1 (j + 1) + matrix:get mat_rwheel_w 1 j)
      matrix:set mat_rwheel_s 1 (j + 1)
      (matrix:get mat_rwheel_s 1 (j + 1) + matrix:get mat_rwheel_s 1 j)
      set j (j + 1) ]
; Prepare the roulette wheel matrix used to select the value change consequent to
; the change in behavior ( i) energy conservation: consume x% less electricity (e.g.
; due to shorter cwash cycles etc.; ii) demand shifting: will activate a part of
; the shift_load procedure related to the concerned appliance.
    let current_mat_rwheel 15
    ifelse CDD_value < HDD_value [
      set current_mat_rwheel mat_rwheel_w ][ ; Set of bhvr related to "winter" or
      set current_mat_rwheel mat_rwheel_s ]  ; "summer" days
    while [ (bhvr_index = bhvr1) OR (bhvr_index = bhvr2) OR (bhvr_index = bhvr3)
    OR (bhvr_index = bhvr4) OR (bhvr_index = bhvr5) ][
      let dice random-float 1  ; Gives the line where to get the behavior
      ifelse dice < matrix:get current_mat_rwheel 1 0 [
        set bhvr_index matrix:get current_mat_rwheel 0 0 ][
        ifelse dice < matrix:get current_mat_rwheel 1 1 [
          set bhvr_index matrix:get current_mat_rwheel 0 1 ][
          ifelse dice < matrix:get current_mat_rwheel 1 2 [
            set bhvr_index matrix:get current_mat_rwheel 0 2 ][
            ifelse dice < matrix:get current_mat_rwheel 1 3 [
              set bhvr_index matrix:get current_mat_rwheel 0 3 ][
              ifelse dice < matrix:get current_mat_rwheel 1 4 [
                set bhvr_index matrix:get current_mat_rwheel 0 4 ][
                ifelse dice < matrix:get current_mat_rwheel 1 5 [
                  set bhvr_index matrix:get current_mat_rwheel 0 5 ][
                  ifelse dice < matrix:get current_mat_rwheel 1 6 [
                    set bhvr_index matrix:get current_mat_rwheel 0 6 ][
                    ifelse dice < matrix:get current_mat_rwheel 1 6 [
                      set bhvr_index matrix:get current_mat_rwheel 0 6 ][
                      ifelse dice < matrix:get current_mat_rwheel 1 7 [
                        set bhvr_index matrix:get current_mat_rwheel 0 7 ][
                        ifelse dice < matrix:get current_mat_rwheel 1 8 [
                          set bhvr_index matrix:get current_mat_rwheel 0 8 ][
                          ifelse dice < matrix:get current_mat_rwheel 1 9 [
                            set bhvr_index matrix:get current_mat_rwheel 0 9 ][
                            if dice < matrix:get current_mat_rwheel 1 10 [
                              set bhvr_index matrix:get current_mat_rwheel 0 10
                              ] ] ] ] ] ] ] ] ] ] ] ] ]
; Roulette wheel : get the corresponding line of bhvr matrix
; according to probability of bhvrs
      set bhvr_index2 bhvr_index
      let dice2 random 12  ; For each possible appliance concerned by a change of
                           ; behavior (consequence of agent decision)
                           ; rather one specific value we compiled several values
                           ; from various sources. Hence random 12 gives the column
                           ; which value is chosen amongst all possible ones.
      let bhvr matrix:get bhvr_matrix bhvr_index dice2
      ifelse bhvr = shift [
        matrix:set shift_bhvr 0 (n_bhvr - 1) bhvr_index ][
        let appliances_inter matrix:times bhvr
        matrix:submatrix appliances 0 bhvr_index 5 (bhvr_index + 1)
        matrix:set-column appliances bhvr_index matrix:get-column appliances_inter 0
        if bhvr_index = 8 [ set bhvr_sheat bhvr ]         ; Used to keep weather
        if (bhvr_index = 10) [ set bhvr_scool bhvr ] ]  ; effect on demand
    if n_bhvr = 1 [ set bhvr1 bhvr_index2 ]
    if n_bhvr = 2 [ set bhvr2 bhvr_index2 ]
    if n_bhvr = 3 [ set bhvr3 bhvr_index2 ]
    if n_bhvr = 4 [ set bhvr4 bhvr_index2 ]
    if n_bhvr = 5 [ set bhvr5 bhvr_index2 ]
    set active? false ] ]
ask turtles with [ color != grey ][
  if give_up? [      ; If agents go back to old behaviors,
    if n_bhvr = 0 [  ; we set bak old values.
      set appliances matrix:copy appliances_default2
      if bhvr1 = 8 [ set bhvr_sheat 1 ]
      if bhvr1 = 10 [ set bhvr_scool 1 ]
      set bhvr_index2 15
      set bhvr1 15
      matrix:set shift_bhvr 0 n_bhvr 15 ]
    if n_bhvr = 1 [
      matrix:set-column appliances bhvr_index2
      matrix:get-column appliances_default2 bhvr_index2
      if bhvr2 = 8 [ set bhvr_sheat 1 ]
      if bhvr2 = 10 [ set bhvr_scool 1 ]
      set bhvr_index2 bhvr1
      set bhvr2 15
      matrix:set shift_bhvr 0 n_bhvr 15 ]
    if n_bhvr = 2 [
      matrix:set-column appliances bhvr_index2
      matrix:get-column appliances_default2 bhvr_index2
      if bhvr3 = 8 [ set bhvr_sheat 1 ]
      if bhvr3 = 10 [ set bhvr_scool 1 ]
      set bhvr_index2 bhvr2
      set bhvr3 15
      matrix:set shift_bhvr 0 n_bhvr 15 ]
    if n_bhvr = 3 [
      matrix:set-column appliances bhvr_index2
      matrix:get-column appliances_default2 bhvr_index2
      if bhvr4 = 8 [ set bhvr_sheat 1 ]
      if bhvr4 = 10 [ set bhvr_scool 1 ]
      set bhvr_index2 bhvr3
      set bhvr4 15
      matrix:set shift_bhvr 0 n_bhvr 15 ]
    if n_bhvr = 4 [
      matrix:set-column appliances bhvr_index2
      matrix:get-column appliances_default2 bhvr_index2
      if (bhvr5 = 8) [ set bhvr_sheat 1 ]
      if (bhvr5 = 10) [ set bhvr_scool 1 ]
      set bhvr_index2 bhvr4
      set bhvr5 15
      matrix:set shift_bhvr 0 n_bhvr 15 ]
    set give_up? false ] ]
; To account for performance, third step: calculate current outputs.
; Once agents changed their bhvr we recalculate daily electric demand, impact
; and cost profiles (stored in vectors and matrices). Procedure similar to
; the first step.
ask turtles with [ color != grey AND n_bhvr != 0 ][
  let j 0
  while [ j < 24 ][
    matrix:set-column econst j (map * matrix:get-row appliances 0
    matrix:get-column freqcons j)
    matrix:set-column default_econs j (map * matrix:get-row appliances_default2 0
    matrix:get-column default_freqcons j)
    set j (j + 1) ]
  let jbis 0
  while [ jbis < 24 ][
    let column_w_issue 25
    let default_column_w_issue 25
    if (reduce + matrix:get-column econst jbis) < 0 [ set column_w_issue jbis ]
    if (reduce + matrix:get-column default_econs jbis) < 0 [
      if PV&Battery_scenario = 2 [ set default_column_w_issue jbis ] ]
    if column_w_issue != 25 [
      let counterPV 0
      while [ (reduce + matrix:get-column econst column_w_issue) < 0 AND
      counterPV < 10 ] [
        let local_surplus reduce + matrix:get-column econst column_w_issue
        matrix:set econst 11 column_w_issue (matrix:get econst 11
          column_w_issue - local_surplus)
        ifelse 23 - jbis < 11 [ matrix:set econst 11 (column_w_issue - 1)
          (matrix:get econst 11 (column_w_issue - 1) + local_surplus)
          set column_w_issue (column_w_issue - 1) ][
          matrix:set econst 11 (column_w_issue + 1)
          (matrix:get econst 11 (column_w_issue + 1) + local_surplus)
          set column_w_issue (column_w_issue + 1) ]
        set counterPV (counterPV + 1) ] ]
    if default_column_w_issue != 25 [
      let counterPV 0
      while [ (reduce + matrix:get-column default_econs default_column_w_issue)
        < 0 AND counterPV < 10 ] [
        let local_surplus reduce + matrix:get-column default_econs
        default_column_w_issue
        matrix:set default_econs 11 default_column_w_issue
        (matrix:get default_econs 11 default_column_w_issue - local_surplus)
        ifelse 23 - jbis < 11 [ matrix:set default_econs 11 (default_column_w_issue - 1)
          (matrix:get default_econs 11 (default_column_w_issue - 1)
            + local_surplus)
          set default_column_w_issue (default_column_w_issue - 1) ][
          matrix:set default_econs 11 (default_column_w_issue + 1)
          (matrix:get default_econs 11 (default_column_w_issue + 1)
            + local_surplus)
          set default_column_w_issue (default_column_w_issue + 1) ]
        set counterPV (counterPV + 1) ] ]
    set jbis (jbis + 1) ]
  set econst matrix:times (1 / 365) econst
  set default_econs matrix:times (1 / 365) default_econs
; (To account for performance, third step : setup emissions per tick)
  let k 0
  while [ k < n_type_appliances ][
    matrix:set-row ht_h_impact k (map * matrix:get-row t_daily_lcia 0
    matrix:get-row econst k)
    matrix:set-row ht_default_impact k (map * matrix:get-row t_daily_lcia 0
    matrix:get-row default_econs k)  ; in kgC2H3Cleq
    matrix:set-row re_h_impact k (map * matrix:get-row t_daily_lcia 1
    matrix:get-row econst k)
    matrix:set-row re_default_impact k (map * matrix:get-row t_daily_lcia 1
    matrix:get-row default_econs k)  ; in kg PM2.5eq
    matrix:set-row ir_h_impact k (map * matrix:get-row t_daily_lcia 2
    matrix:get-row econst k)
    matrix:set-row ir_default_impact k (map * matrix:get-row t_daily_lcia 2
    matrix:get-row default_econs k)  ; in Bq C-14eq
    matrix:set-row old_h_impact k (map * matrix:get-row t_daily_lcia 3
    matrix:get-row econst k)
    matrix:set-row old_default_impact k (map * matrix:get-row t_daily_lcia 3
    matrix:get-row default_econs k)  ; in kgCFC11eq
    matrix:set-row po_h_impact k (map * matrix:get-row t_daily_lcia 4
    matrix:get-row econst k)
    matrix:set-row po_default_impact k (map * matrix:get-row t_daily_lcia 4
    matrix:get-row default_econs k)  ; in kgC2H4eq
    matrix:set-row ae_h_impact k (map * matrix:get-row t_daily_lcia 5
    matrix:get-row econst k)
    matrix:set-row ae_default_impact k (map * matrix:get-row t_daily_lcia 5
    matrix:get-row default_econs k)  ; in kg TEG water
    matrix:set-row te_h_impact k (map * matrix:get-row t_daily_lcia 6
    matrix:get-row econst k)
    matrix:set-row te_default_impact k (map * matrix:get-row t_daily_lcia 6
    matrix:get-row default_econs k)  ; in kg TEG soil
    matrix:set-row tan_h_impact k (map * matrix:get-row t_daily_lcia 7
    matrix:get-row econst k)
    matrix:set-row tan_default_impact k (map * matrix:get-row t_daily_lcia 7
    matrix:get-row default_econs k)  ; in kg So2eq
    matrix:set-row lo_h_impact k (map * matrix:get-row t_daily_lcia 8
    matrix:get-row econst k)
    matrix:set-row lo_default_impact k (map * matrix:get-row t_daily_lcia 8
    matrix:get-row default_econs k)  ; in m2org.arable
    matrix:set-row cc_h_impact k (map * matrix:get-row t_daily_lcia 9
    matrix:get-row econst k)
    matrix:set-row cc_default_impact k (map * matrix:get-row t_daily_lcia 9
    matrix:get-row default_econs k)  ; in kgCO2eq
    matrix:set-row nre_h_impact k (map * matrix:get-row t_daily_lcia 10
    matrix:get-row econst k)
    matrix:set-row nre_default_impact k (map * matrix:get-row t_daily_lcia 10
    matrix:get-row default_econs k)  ; in MJ primary
    matrix:set-row rme_h_impact k (map * matrix:get-row t_daily_lcia 11
    matrix:get-row econst k)
    matrix:set-row rme_default_impact k (map * matrix:get-row t_daily_lcia 11
    matrix:get-row default_econs k)  ; in MJ surplus
    ifelse price_scenario != 3 [
      matrix:set-row cost k (map * matrix:get-row hourly_cost 0
      matrix:get-row econst k)
      matrix:set-row default_cost k (map * matrix:get-row hourly_cost 0
      matrix:get-row default_econs k) ][  ; in $
      matrix:set-row cost k (map * matrix:get-row hourly_cost2 0
      matrix:get-row econst k)
      matrix:set-row default_cost k (map * matrix:get-row hourly_cost2 0
      matrix:get-row default_econs k) ]  ; in $
    set k (k + 1) ]
  matrix:set-row cost 11 (map + (matrix:get-row cost 11)
    (matrix:get-row PV_surplus 0))
  matrix:set-row default_cost 11 (map + (matrix:get-row default_cost 11)
    (matrix:get-row default_PV_surplus 0)) ]
if (nticks / 24) = int (ticks / 24) [ ; Compute some outputs
  let m 0
  while [ m < nt ][
    ask turtle m [
    set global_econst (matrix:map + global_econst econst)
    set global_default_econst (matrix:map + global_default_econst default_econs) ]
    set m (m + 1) ]
  set passive0_econst [ econst ] of turtle 0
  set passive0_defaulteconst [ default_econs ] of turtle 0
  set yellowguy_econst [ econst ] of turtle yellowguy
  set yellowguy_defaulteconst [ default_econs ] of turtle yellowguy
  set passive0_freqcons [ freqcons ] of turtle 0
  set passive0_defaultfreqcons [ default_freqcons ] of turtle 0
  set yellowguy_freqcons [ freqcons ] of turtle yellowguy
  set yellowguy_defaultfreqcons [ default_freqcons ] of turtle yellowguy ]
end

to report_outputs ; Report variables of interest
ifelse hour_of_day != 24 [
  ask turtles with [ color != grey ] [
    set ht_live_impact (reduce + matrix:get-column ht_h_impact hour_of_day)
    set ht_live_default_impact
    (reduce + matrix:get-column ht_default_impact hour_of_day)
    set re_live_impact (reduce + matrix:get-column re_h_impact hour_of_day)
    set re_live_default_impact
    (reduce + matrix:get-column re_default_impact hour_of_day)
    set ir_live_impact (reduce + matrix:get-column ir_h_impact hour_of_day)
    set ir_live_default_impact
    (reduce + matrix:get-column ir_default_impact hour_of_day)
    set old_live_impact (reduce + matrix:get-column old_h_impact hour_of_day)
    set old_live_default_impact
    (reduce + matrix:get-column old_default_impact hour_of_day)
    set po_live_impact (reduce + matrix:get-column po_h_impact hour_of_day)
    set po_live_default_impact
    (reduce + matrix:get-column po_default_impact hour_of_day)
    set ae_live_impact (reduce + matrix:get-column ae_h_impact hour_of_day)
    set ae_live_default_impact
    (reduce + matrix:get-column ae_default_impact hour_of_day)
    set te_live_impact (reduce + matrix:get-column te_h_impact hour_of_day)
    set te_live_default_impact
    (reduce + matrix:get-column te_default_impact hour_of_day)
    set tan_live_impact (reduce + matrix:get-column tan_h_impact hour_of_day)
    set tan_live_default_impact
    (reduce + matrix:get-column tan_default_impact hour_of_day)
    set lo_live_impact (reduce + matrix:get-column lo_h_impact hour_of_day)
    set lo_live_default_impact
    (reduce + matrix:get-column lo_default_impact hour_of_day)
    set cc_live_impact (reduce + matrix:get-column cc_h_impact hour_of_day)
    set cc_live_default_impact
    (reduce + matrix:get-column cc_default_impact hour_of_day)
    set nre_live_impact (reduce + matrix:get-column nre_h_impact hour_of_day)
    set nre_live_default_impact
    (reduce + matrix:get-column nre_default_impact hour_of_day)
    set rme_live_impact (reduce + matrix:get-column rme_h_impact hour_of_day)
    set rme_live_default_impact
    (reduce + matrix:get-column rme_default_impact hour_of_day)
    set hh_live_impact ((ht_live_impact * matrix:get xf_endpoint_impact2002 0 0)
    + (re_live_impact * matrix:get xf_endpoint_impact2002 0 1)
     + (ir_live_impact * matrix:get xf_endpoint_impact2002 0 2)
     + (old_live_impact * matrix:get xf_endpoint_impact2002 0 3)
     + (po_live_impact * matrix:get xf_endpoint_impact2002 0 4))
    set hh_live_default_impact
    ((ht_live_default_impact * matrix:get xf_endpoint_impact2002 0 0)
    + (re_live_default_impact * matrix:get xf_endpoint_impact2002 0 1)
     + (ir_live_default_impact * matrix:get xf_endpoint_impact2002 0 2)
     + (old_live_default_impact * matrix:get xf_endpoint_impact2002 0 3)
     + (po_live_default_impact * matrix:get xf_endpoint_impact2002 0 4))
    set eq_live_impact ((ae_live_impact * matrix:get xf_endpoint_impact2002 0 5)
    + (te_live_impact * matrix:get xf_endpoint_impact2002 0 6)
     + (tan_live_impact * matrix:get xf_endpoint_impact2002 0 7)
     + (lo_live_impact * matrix:get xf_endpoint_impact2002 0 8))
    set eq_live_default_impact
    ((ae_live_default_impact * matrix:get xf_endpoint_impact2002 0 5)
    + (te_live_default_impact * matrix:get xf_endpoint_impact2002 0 6)
     + (tan_live_default_impact * matrix:get xf_endpoint_impact2002 0 7)
     + (lo_live_default_impact * matrix:get xf_endpoint_impact2002 0 8))
    set res_live_impact ((nre_live_impact * matrix:get xf_endpoint_impact2002 0 10)
    + (rme_live_impact * matrix:get xf_endpoint_impact2002 0 11))
    set res_live_default_impact
    ((nre_live_default_impact * matrix:get xf_endpoint_impact2002 0 10)
    + (rme_live_default_impact * matrix:get xf_endpoint_impact2002 0 11))
    set ss_live_impact ((hh_live_impact * matrix:get single_score_impact2002 0 0
    * matrix:get single_score_impact2002 vision 0) +
     (eq_live_impact * matrix:get single_score_impact2002 0 1
     * matrix:get single_score_impact2002 vision 1) +
     ((cc_live_impact * matrix:get single_score_impact2002 0 2 + res_live_impact
     * matrix:get single_score_impact2002 0 3)
     * matrix:get single_score_impact2002 vision 2))
    set ss_live_default_impact ((hh_live_default_impact
    * matrix:get single_score_impact2002 0 0
    * matrix:get single_score_impact2002 vision 0) +
     (eq_live_default_impact * matrix:get single_score_impact2002 0 1
     * matrix:get single_score_impact2002 vision 1) +
     ((cc_live_default_impact * matrix:get single_score_impact2002 0 2
     + res_live_default_impact * matrix:get single_score_impact2002 0 3)
       * matrix:get single_score_impact2002 vision 2))
    set live_econs (reduce + matrix:get-column econst hour_of_day)
    set live_default_econs (reduce + matrix:get-column default_econs hour_of_day)
    set live_cost (reduce + matrix:get-column cost hour_of_day)
    set live_default_cost (reduce + matrix:get-column default_cost hour_of_day)
; Below, simplistic optimization (O) model : the only constraints is that
; the sum be 1, hence the solution is simply the endpoint category that obtain
; the lowest score in points.
    if individual_vision = 1 [
      let max_point_category max (list (hh_live_impact
      * matrix:get single_score_impact2002 0 0)
      (eq_live_impact * matrix:get single_score_impact2002 0 1) (cc_live_impact
      * matrix:get single_score_impact2002 0 2)
      (res_live_impact * matrix:get single_score_impact2002 0 3))
      matrix:set turtle_vision max_point_category hour_of_day 1 ] ] ][
  set hour_of_day 0
  ask turtles with [ color != grey ] [
    set ht_live_impact (reduce + matrix:get-column ht_h_impact hour_of_day)
    set ht_live_default_impact
    (reduce + matrix:get-column ht_default_impact hour_of_day)
    set re_live_impact (reduce + matrix:get-column re_h_impact hour_of_day)
    set re_live_default_impact
    (reduce + matrix:get-column re_default_impact hour_of_day)
    set ir_live_impact (reduce + matrix:get-column ir_h_impact hour_of_day)
    set ir_live_default_impact
    (reduce + matrix:get-column ir_default_impact hour_of_day)
    set old_live_impact (reduce + matrix:get-column old_h_impact hour_of_day)
    set old_live_default_impact
    (reduce + matrix:get-column old_default_impact hour_of_day)
    set po_live_impact (reduce + matrix:get-column po_h_impact hour_of_day)
    set po_live_default_impact
    (reduce + matrix:get-column po_default_impact hour_of_day)
    set ae_live_impact (reduce + matrix:get-column ae_h_impact hour_of_day)
    set ae_live_default_impact
    (reduce + matrix:get-column ae_default_impact hour_of_day)
    set te_live_impact (reduce + matrix:get-column te_h_impact hour_of_day)
    set te_live_default_impact
    (reduce + matrix:get-column te_default_impact hour_of_day)
    set tan_live_impact (reduce + matrix:get-column tan_h_impact hour_of_day)
    set tan_live_default_impact
    (reduce + matrix:get-column tan_default_impact hour_of_day)
    set lo_live_impact (reduce + matrix:get-column lo_h_impact hour_of_day)
    set lo_live_default_impact
    (reduce + matrix:get-column lo_default_impact hour_of_day)
    set cc_live_impact (reduce + matrix:get-column cc_h_impact hour_of_day)
    set cc_live_default_impact
    (reduce + matrix:get-column cc_default_impact hour_of_day)
    set nre_live_impact (reduce + matrix:get-column nre_h_impact hour_of_day)
    set nre_live_default_impact
    (reduce + matrix:get-column nre_default_impact hour_of_day)
    set rme_live_impact (reduce + matrix:get-column rme_h_impact hour_of_day)
    set rme_live_default_impact
    (reduce + matrix:get-column rme_default_impact hour_of_day)
    set hh_live_impact ((ht_live_impact * matrix:get xf_endpoint_impact2002 0 0)
    + (re_live_impact * matrix:get xf_endpoint_impact2002 0 1)
     + (ir_live_impact * matrix:get xf_endpoint_impact2002 0 2)
     + (old_live_impact * matrix:get xf_endpoint_impact2002 0 3)
     + (po_live_impact * matrix:get xf_endpoint_impact2002 0 4))
    set hh_live_default_impact
    ((ht_live_default_impact * matrix:get xf_endpoint_impact2002 0 0)
    + (re_live_default_impact * matrix:get xf_endpoint_impact2002 0 1)
     + (ir_live_default_impact * matrix:get xf_endpoint_impact2002 0 2)
     + (old_live_default_impact * matrix:get xf_endpoint_impact2002 0 3)
     + (po_live_default_impact * matrix:get xf_endpoint_impact2002 0 4))
    set eq_live_impact ((ae_live_impact * matrix:get xf_endpoint_impact2002 0 5)
    + (te_live_impact * matrix:get xf_endpoint_impact2002 0 6)
     + (tan_live_impact * matrix:get xf_endpoint_impact2002 0 7)
     + (lo_live_impact * matrix:get xf_endpoint_impact2002 0 8))
    set eq_live_default_impact
    ((ae_live_default_impact * matrix:get xf_endpoint_impact2002 0 5)
    + (te_live_default_impact * matrix:get xf_endpoint_impact2002 0 6)
     + (tan_live_default_impact * matrix:get xf_endpoint_impact2002 0 7)
     + (lo_live_default_impact * matrix:get xf_endpoint_impact2002 0 8))
    set res_live_impact ((nre_live_impact * matrix:get xf_endpoint_impact2002 0 10)
    + (rme_live_impact * matrix:get xf_endpoint_impact2002 0 11))
    set res_live_default_impact ((nre_live_default_impact
    * matrix:get xf_endpoint_impact2002 0 10)
    + (rme_live_default_impact * matrix:get xf_endpoint_impact2002 0 11))
    set ss_live_impact ((hh_live_impact * matrix:get single_score_impact2002 0 0
    * matrix:get single_score_impact2002 vision 0) +
     (eq_live_impact * matrix:get single_score_impact2002 0 1
     * matrix:get single_score_impact2002 vision 1) +
     ((cc_live_impact * matrix:get single_score_impact2002 0 2 + res_live_impact
     * matrix:get single_score_impact2002 0 3)
     * matrix:get single_score_impact2002 vision 2))
    set ss_live_default_impact ((hh_live_default_impact
    * matrix:get single_score_impact2002 0 0
    * matrix:get single_score_impact2002 vision 0) +
     (eq_live_default_impact * matrix:get single_score_impact2002 0 1
     * matrix:get single_score_impact2002 vision 1) +
     ((cc_live_default_impact * matrix:get single_score_impact2002 0 2
     + res_live_default_impact * matrix:get single_score_impact2002 0 3)
       * matrix:get single_score_impact2002 vision 2))
    set live_econs (reduce + matrix:get-column econst hour_of_day)
    set live_default_econs (reduce + matrix:get-column default_econs hour_of_day)
    set live_cost (reduce + matrix:get-column cost hour_of_day)
    set live_default_cost (reduce + matrix:get-column default_cost hour_of_day)
    if individual_vision = 1 [
      let max_point_category max (list (hh_live_impact
      * matrix:get single_score_impact2002 0 0)
      (eq_live_impact * matrix:get single_score_impact2002 0 1) (cc_live_impact
      * matrix:get single_score_impact2002 0 2)
      (res_live_impact * matrix:get single_score_impact2002 0 3))
      matrix:set turtle_vision max_point_category hour_of_day 1 ] ] ]
set hour_of_day (hour_of_day + 1)
ask turtles with [ color != grey ][
; Savings computation in order to compute emissions due to indirect rebound effect
  set savings (live_default_cost - live_cost)
  set impact_reduction (cc_live_default_impact - cc_live_impact)
  let impact_rebound matrix:from-row-list csv:from-file "inputFiles/iv_rebound.csv"
  set impact_rebound matrix:times impact_consum matrix:transpose consum
  set ht_live_rebound_tot (savings * matrix:get impact_rebound 0 0)
  set re_live_rebound_tot (savings * matrix:get impact_rebound 1 0)
  set old_live_rebound_tot (savings * matrix:get impact_rebound 2 0)
  set po_live_rebound_tot (savings * matrix:get impact_rebound 3 0)
  set ae_live_rebound_tot (savings * matrix:get impact_rebound 4 0)
  set te_live_rebound_tot (savings * matrix:get impact_rebound 5 0)
  set tan_live_rebound_tot (savings * matrix:get impact_rebound 6 0)
  set cc_live_rebound_tot (savings * matrix:get impact_rebound 7 0)
  set ht_live_impact_tot (ht_live_impact + ht_live_rebound_tot)
  set re_live_impact_tot (re_live_impact + re_live_rebound_tot)
  set old_live_impact_tot (old_live_impact + old_live_rebound_tot)
  set po_live_impact_tot (po_live_impact + po_live_rebound_tot)
  set ae_live_impact_tot (ae_live_impact + ae_live_rebound_tot)
  set te_live_impact_tot (te_live_impact + te_live_rebound_tot)
  set tan_live_impact_tot (tan_live_impact + tan_live_rebound_tot)
  set cc_live_impact_tot (cc_live_impact + cc_live_rebound_tot)
  set hh_live_impact_tot (hh_live_impact + ((ht_live_rebound_tot
  * matrix:get xf_endpoint_impact2002 0 0) + (re_live_rebound_tot
  * matrix:get xf_endpoint_impact2002 0 1)
   + (old_live_rebound_tot * matrix:get xf_endpoint_impact2002 0 3)
   + (po_live_rebound_tot * matrix:get xf_endpoint_impact2002 0 4)))
  set eq_live_impact_tot (eq_live_impact + ((ae_live_rebound_tot
  * matrix:get xf_endpoint_impact2002 0 5) + (te_live_rebound_tot
  * matrix:get xf_endpoint_impact2002 0 6)
   + (tan_live_rebound_tot * matrix:get xf_endpoint_impact2002 0 7)))
  set ss_live_impact_tot ((hh_live_impact_tot
  * matrix:get single_score_impact2002 0 0
  * matrix:get single_score_impact2002 vision 0) +
   (eq_live_impact_tot * matrix:get single_score_impact2002 0 1
   * matrix:get single_score_impact2002 vision 1) +
   (cc_live_impact_tot * matrix:get single_score_impact2002 0 2
   * matrix:get single_score_impact2002 vision 2)) ]
; Set reporters of hourly variation of demand, impact, cost for two individual
; agents and for the entire building (or collection of "smart-homes".
set ht_passive0_impacttot [ ht_live_impact_tot ] of turtle 0
set re_passive0_impacttot [ re_live_impact_tot ] of turtle 0
set old_passive0_impacttot [ old_live_impact_tot ] of turtle 0
set po_passive0_impacttot [ po_live_impact_tot ] of turtle 0
set ae_passive0_impacttot [ ae_live_impact_tot ] of turtle 0
set te_passive0_impacttot [ te_live_impact_tot ] of turtle 0
set tan_passive0_impacttot [ tan_live_impact_tot ] of turtle 0
set cc_passive0_impacttot [ cc_live_impact_tot ] of turtle 0
set ht_passive0_impact [ ht_live_impact ] of turtle 0
set ht_passive0_defaultimpact [ ht_live_default_impact ] of turtle 0
set re_passive0_impact [ re_live_impact ] of turtle 0
set re_passive0_defaultimpact [ re_live_default_impact ] of turtle 0
set ir_passive0_impact [ ir_live_impact ] of turtle 0
set ir_passive0_defaultimpact [ ir_live_default_impact ] of turtle 0
set old_passive0_impact [ old_live_impact ] of turtle 0
set old_passive0_defaultimpact [ old_live_default_impact ] of turtle 0
set po_passive0_impact [ po_live_impact ] of turtle 0
set po_passive0_defaultimpact [ po_live_default_impact ] of turtle 0
set ae_passive0_impact [ ae_live_impact ] of turtle 0
set ae_passive0_defaultimpact [ ae_live_default_impact ] of turtle 0
set te_passive0_impact [ te_live_impact ] of turtle 0
set te_passive0_defaultimpact [ te_live_default_impact ] of turtle 0
set tan_passive0_impact [ tan_live_impact ] of turtle 0
set tan_passive0_defaultimpact [ tan_live_default_impact ] of turtle 0
set lo_passive0_impact [ lo_live_impact ] of turtle 0
set lo_passive0_defaultimpact [ lo_live_default_impact ] of turtle 0
set cc_passive0_impact [ cc_live_impact ] of turtle 0
set cc_passive0_defaultimpact [ cc_live_default_impact ] of turtle 0
set nre_passive0_impact [ nre_live_impact ] of turtle 0
set nre_passive0_defaultimpact [ nre_live_default_impact ] of turtle 0
set rme_passive0_impact [ rme_live_impact ] of turtle 0
set rme_passive0_defaultimpact [ rme_live_default_impact ] of turtle 0
set ht_yellowguy_impacttot [ ht_live_impact_tot ] of turtle yellowguy
set re_yellowguy_impacttot [ re_live_impact_tot ] of turtle yellowguy
set old_yellowguy_impacttot [ old_live_impact_tot ] of turtle yellowguy
set po_yellowguy_impacttot [ po_live_impact_tot ] of turtle yellowguy
set ae_yellowguy_impacttot [ ae_live_impact_tot ] of turtle yellowguy
set te_yellowguy_impacttot [ te_live_impact_tot ] of turtle yellowguy
set tan_yellowguy_impacttot [ tan_live_impact_tot ] of turtle yellowguy
set cc_yellowguy_impacttot [ cc_live_impact_tot ] of turtle yellowguy
set ht_yellowguy_impact [ ht_live_impact ] of turtle yellowguy
set ht_yellowguy_defaultimpact [ ht_live_default_impact ] of turtle yellowguy
set re_yellowguy_impact [ re_live_impact ] of turtle yellowguy
set re_yellowguy_defaultimpact [ re_live_default_impact ] of turtle yellowguy
set ir_yellowguy_impact [ ir_live_impact ] of turtle yellowguy
set ir_yellowguy_defaultimpact [ ir_live_default_impact ] of turtle yellowguy
set old_yellowguy_impact [ old_live_impact ] of turtle yellowguy
set old_yellowguy_defaultimpact [ old_live_default_impact ] of turtle yellowguy
set po_yellowguy_impact [ po_live_impact ] of turtle yellowguy
set po_yellowguy_defaultimpact [ po_live_default_impact ] of turtle yellowguy
set ae_yellowguy_impact [ ae_live_impact ] of turtle yellowguy
set ae_yellowguy_defaultimpact [ ae_live_default_impact ] of turtle yellowguy
set te_yellowguy_impact [ te_live_impact ] of turtle yellowguy
set te_yellowguy_defaultimpact [ te_live_default_impact ] of turtle yellowguy
set tan_yellowguy_impact [ tan_live_impact ] of turtle yellowguy
set tan_yellowguy_defaultimpact [ tan_live_default_impact ] of turtle yellowguy
set lo_yellowguy_impact [ lo_live_impact ] of turtle yellowguy
set lo_yellowguy_defaultimpact [ lo_live_default_impact ] of turtle yellowguy
set cc_yellowguy_impact [ cc_live_impact ] of turtle yellowguy
set cc_yellowguy_defaultimpact [ cc_live_default_impact ] of turtle yellowguy
set nre_yellowguy_impact [ nre_live_impact ] of turtle yellowguy
set nre_yellowguy_defaultimpact [ nre_live_default_impact ] of turtle yellowguy
set rme_yellowguy_impact [ rme_live_impact ] of turtle yellowguy
set rme_yellowguy_defaultimpact [ rme_live_default_impact ] of turtle yellowguy
set ht_default_impactbis (sum [ ht_live_impact ] of turtles with [ color != grey ])
set ht_impactbis (sum [ ht_live_impact ] of turtles with
[ color != grey AND n_bhvr != 0 ])
set ht_default_impact_building (sum [ ht_live_default_impact ] of turtles with
[ color != grey ])
set re_default_impactbis (sum [ re_live_impact ] of turtles with [ color != grey ])
set re_impactbis (sum [ re_live_impact ] of turtles with
[ color != grey AND n_bhvr != 0 ])
set re_default_impact_building (sum [ re_live_default_impact ] of turtles with
[ color != grey ])
set ir_default_impactbis (sum [ ir_live_impact ] of turtles with [ color != grey ])
set ir_impactbis (sum [ ir_live_impact ] of turtles with
[ color != grey AND n_bhvr != 0 ])
set ir_default_impact_building (sum [ ir_live_default_impact ] of turtles with
[ color != grey ])
set old_default_impactbis (sum [ old_live_impact ] of turtles with [ color != grey ])
set old_impactbis (sum [ old_live_impact ] of turtles with
[ color != grey AND n_bhvr != 0 ])
set old_default_impact_building (sum [ old_live_default_impact ] of turtles with
[ color != grey ])
set po_default_impactbis (sum [ po_live_impact ] of turtles with [ color != grey ])
set po_impactbis (sum [ po_live_impact ] of turtles with
[ color != grey AND n_bhvr != 0 ])
set po_default_impact_building (sum [ po_live_default_impact ] of turtles with
[ color != grey ])
set ae_default_impactbis (sum [ ae_live_impact ] of turtles with [ color != grey ])
set ae_impactbis (sum [ ae_live_impact ] of turtles with
[ color != grey AND n_bhvr != 0 ])
set ae_default_impact_building (sum [ ae_live_default_impact ] of turtles with
[ color != grey ])
set te_default_impactbis (sum [ te_live_impact ] of turtles with [ color != grey ])
set te_impactbis (sum [ te_live_impact ] of turtles with
[ color != grey AND n_bhvr != 0 ])
set te_default_impact_building (sum [ te_live_default_impact ] of turtles with
[ color != grey ])
set tan_default_impactbis (sum [ tan_live_impact ] of turtles with [ color != grey ])
set tan_impactbis (sum [ tan_live_impact ] of turtles with
[ color != grey AND n_bhvr != 0 ])
set tan_default_impact_building (sum [ tan_live_default_impact ] of turtles with
[ color != grey ])
set lo_default_impactbis (sum [ lo_live_impact ] of turtles with [ color != grey ])
set lo_impactbis (sum [ lo_live_impact ] of turtles with
[ color != grey AND n_bhvr != 0 ])
set lo_default_impact_building (sum [ lo_live_default_impact ] of turtles with
[ color != grey ])
set cc_default_impactbis (sum [ cc_live_impact ] of turtles with [ color != grey ])
set cc_impactbis (sum [ cc_live_impact ] of turtles with
[ color != grey AND n_bhvr != 0 ])
set cc_default_impact_building (sum [ cc_live_default_impact ] of turtles with
[ color != grey ])
set nre_default_impactbis (sum [ nre_live_impact ] of turtles with [ color != grey ])
set nre_impactbis (sum [ nre_live_impact ] of turtles with
[ color != grey AND n_bhvr != 0 ])
set nre_default_impact_building (sum [ nre_live_default_impact ] of turtles with
[ color != grey ])
set rme_default_impactbis (sum [ rme_live_impact ] of turtles with [ color != grey ])
set rme_impactbis (sum [ rme_live_impact ] of turtles with
[ color != grey AND n_bhvr != 0 ])
set rme_default_impact_building (sum [ rme_live_default_impact ] of turtles with
[ color != grey ])
set hh_default_impactbis (sum [ hh_live_impact ] of turtles with [ color != grey ])
set hh_impactbis (sum [ hh_live_impact ] of turtles with
[ color != grey AND n_bhvr != 0 ])
set hh_default_impact_building (sum [ hh_live_default_impact ] of turtles with
[ color != grey ])
set eq_default_impactbis (sum [ eq_live_impact ] of turtles with [ color != grey ])
set eq_impactbis (sum [ eq_live_impact ] of turtles with
[ color != grey AND n_bhvr != 0 ])
set eq_default_impact_building (sum [ eq_live_default_impact ] of turtles with
[ color != grey ])
set res_default_impactbis (sum [ res_live_impact ] of turtles with [ color != grey ])
set res_impactbis (sum [ res_live_impact ] of turtles with
[ color != grey AND n_bhvr != 0 ])
set res_default_impact_building (sum [ res_live_default_impact ] of turtles with
[ color != grey ])
set ss_default_impactbis (sum [ ss_live_impact ] of turtles with [ color != grey ])
set ss_impactbis (sum [ ss_live_impact ] of turtles with
[ color != grey AND n_bhvr != 0 ])
set ss_default_impact_building (sum [ ss_live_default_impact ] of turtles with
[ color != grey ])
set ht_impact_tot (sum [ ht_live_impact_tot ] of turtles with
[ color != grey AND n_bhvr != 0 ])
set re_impact_tot (sum [ re_live_impact_tot ] of turtles with
[ color != grey AND n_bhvr != 0 ])
set old_impact_tot (sum [ old_live_impact_tot ] of turtles with
[ color != grey AND n_bhvr != 0 ])
set po_impact_tot (sum [ po_live_impact_tot ] of turtles with
[ color != grey AND n_bhvr != 0 ])
set ae_impact_tot (sum [ ae_live_impact_tot ] of turtles with
[ color != grey AND n_bhvr != 0 ])
set te_impact_tot (sum [ te_live_impact_tot ] of turtles with
[ color != grey AND n_bhvr != 0 ])
set tan_impact_tot (sum [ tan_live_impact_tot ] of turtles with
[ color != grey AND n_bhvr != 0 ])
set cc_impact_tot (sum [ cc_live_impact_tot ] of turtles with
[ color != grey AND n_bhvr != 0 ])
set hh_impact_tot (sum [ hh_live_impact_tot ] of turtles with
[ color != grey AND n_bhvr != 0 ])
set eq_impact_tot (sum [ eq_live_impact_tot ] of turtles with
[ color != grey AND n_bhvr != 0 ])
set ss_impact_tot (sum [ ss_live_impact_tot ] of turtles with
[ color != grey AND n_bhvr != 0 ])
set ht_impact_tot2 ((sum [ ht_live_impact_tot ] of turtles with
[ color != grey AND n_bhvr != 0 ]) + (sum [ ht_live_impact ] of turtles with
[ color != grey AND n_bhvr = 0 ]))
set re_impact_tot2 ((sum [ re_live_impact_tot ] of turtles with
[ color != grey AND n_bhvr != 0 ]) + (sum [ re_live_impact ] of turtles with
[ color != grey AND n_bhvr = 0 ]))
set old_impact_tot2 ((sum [ old_live_impact_tot ] of turtles with
[ color != grey AND n_bhvr != 0 ]) + (sum [ old_live_impact ] of turtles with
[ color != grey AND n_bhvr = 0 ]))
set po_impact_tot2 ((sum [ po_live_impact_tot ] of turtles with
[ color != grey AND n_bhvr != 0 ]) + (sum [ po_live_impact ] of turtles with
[ color != grey AND n_bhvr = 0 ]))
set ae_impact_tot2 ((sum [ ae_live_impact_tot ] of turtles with
[ color != grey AND n_bhvr != 0 ]) + (sum [ ae_live_impact ] of turtles with
[ color != grey AND n_bhvr = 0 ]))
set te_impact_tot2 ((sum [ te_live_impact_tot ] of turtles with
[ color != grey AND n_bhvr != 0 ]) + (sum [ te_live_impact ] of turtles with
[ color != grey AND n_bhvr = 0 ]))
set tan_impact_tot2 ((sum [ tan_live_impact_tot ] of turtles with
[ color != grey AND n_bhvr != 0 ]) + (sum [ tan_live_impact ] of turtles with
[ color != grey AND n_bhvr = 0 ]))
set cc_impact_tot2 ((sum [ cc_live_impact_tot ] of turtles with
[ color != grey AND n_bhvr != 0 ]) + (sum [ cc_live_impact ] of turtles with
[ color != grey AND n_bhvr = 0 ]))
set hh_impact_tot2 ((sum [ hh_live_impact_tot ] of turtles with
[ color != grey AND n_bhvr != 0 ]) + (sum [ hh_live_impact ] of turtles with
[ color != grey AND n_bhvr = 0 ]))
set eq_impact_tot2 ((sum [ eq_live_impact_tot ] of turtles with
[ color != grey AND n_bhvr != 0 ]) + (sum [ eq_live_impact ] of turtles with
[ color != grey AND n_bhvr = 0 ]))
set ss_impact_tot2 ((sum [ ss_live_impact_tot ] of turtles with
[ color != grey AND n_bhvr != 0 ]) + (sum [ ss_live_impact ] of turtles with
[ color != grey AND n_bhvr = 0 ]))
set costbis (sum [ live_cost ] of turtles with [ color != grey ])
set costbis2 (sum [ live_default_cost ] of turtles with [ color != grey ])
set electric_consumption (sum [ live_econs ] of turtles with [ color != grey ])
set electric_consumption2 (sum [ live_default_econs ] of turtles with
[ color != grey ])
set SB_default_cons (SB_default_cons + electric_consumption2)
set SB_default_cost (SB_default_cost + costbis2)
set SB_default_impact_ht (SB_default_impact_ht + ht_default_impact_building)
set SB_default_impact_re (SB_default_impact_re + re_default_impact_building)
set SB_default_impact_ir (SB_default_impact_ir + ir_default_impact_building)
set SB_default_impact_old (SB_default_impact_old + old_default_impact_building)
set SB_default_impact_po (SB_default_impact_po + po_default_impact_building)
set SB_default_impact_ae (SB_default_impact_ae + ae_default_impact_building)
set SB_default_impact_te (SB_default_impact_te + te_default_impact_building)
set SB_default_impact_tan (SB_default_impact_tan + tan_default_impact_building)
set SB_default_impact_lo (SB_default_impact_lo + lo_default_impact_building)
set SB_default_impact_cc (SB_default_impact_cc + cc_default_impact_building)
set SB_default_impact_nre (SB_default_impact_nre + nre_default_impact_building)
set SB_default_impact_rme (SB_default_impact_rme + rme_default_impact_building)
set SB_default_impact_hh (SB_default_impact_hh + hh_default_impact_building)
set SB_default_impact_eq (SB_default_impact_eq + eq_default_impact_building)
set SB_default_impact_res (SB_default_impact_res + res_default_impact_building)
set SB_default_impact_ss (SB_default_impact_ss + ss_default_impact_building)
set SB_cons (SB_cons + electric_consumption)
set SB_cost (SB_cost + costbis)
set SB_impact_ht (SB_impact_ht + ht_default_impactbis)
set SB_impact_re (SB_impact_re + re_default_impactbis)
set SB_impact_ir (SB_impact_ir + ir_default_impactbis)
set SB_impact_old (SB_impact_old + old_default_impactbis)
set SB_impact_po (SB_impact_po + po_default_impactbis)
set SB_impact_ae (SB_impact_ae + ae_default_impactbis)
set SB_impact_te (SB_impact_te + te_default_impactbis)
set SB_impact_tan (SB_impact_tan + tan_default_impactbis)
set SB_impact_lo (SB_impact_lo + lo_default_impactbis)
set SB_impact_cc (SB_impact_cc + cc_default_impactbis)
set SB_impact_nre (SB_impact_nre + nre_default_impactbis)
set SB_impact_rme (SB_impact_rme + rme_default_impactbis)
set SB_impact_hh (SB_impact_hh + hh_default_impactbis)
set SB_impact_eq (SB_impact_eq + eq_default_impactbis)
set SB_impact_res (SB_impact_res + res_default_impactbis)
set SB_impact_ss (SB_impact_ss + ss_default_impactbis)
set SB_RE_impact_ht (SB_RE_impact_ht + ht_impact_tot2)
set SB_RE_impact_re (SB_RE_impact_re + re_impact_tot2)
set SB_RE_impact_old (SB_RE_impact_old + old_impact_tot2)
set SB_RE_impact_po (SB_RE_impact_po + po_impact_tot2)
set SB_RE_impact_ae (SB_RE_impact_ae + ae_impact_tot2)
set SB_RE_impact_te (SB_RE_impact_te + te_impact_tot2)
set SB_RE_impact_tan (SB_RE_impact_tan + tan_impact_tot2)
set SB_RE_impact_cc (SB_RE_impact_cc + cc_impact_tot2)
set SB_RE_impact_hh (SB_RE_impact_hh + hh_impact_tot2)
set SB_RE_impact_eq (SB_RE_impact_eq + eq_impact_tot2)
set SB_RE_impact_ss (SB_RE_impact_ss + ss_impact_tot2)
; Computation of turtles cumulative performances
ask turtles with [ color != grey ][
  set cumul_cons (cumul_cons + live_econs)
  set cumul_cost (cumul_cost + live_cost)
  set cumul_impact (cumul_impact + cc_live_impact)
  set h_smarthome_cons replace-item (hour_of_day - 1) h_smarthome_cons live_econs
  if (nticks / 24) = int (nticks / 24) [
    set cumul_h_smarthome_cons (map + cumul_h_smarthome_cons h_smarthome_cons) ] ]
set default_cc_art3 (default_cc_art3 + electric_consumption2 * matrix:get xf_avrg_art3 0 nticks)
end

to week_end
; Change daily frequencies of appliances use, "appliance-dice"
; (hourly probabilities to set an appliance on) and prices.
if (nw != 0) [
  if (nticks / (120 + (nw * 168))) = int (ticks / (120 + (nw * 168))) [
    set hourly_proba matrix:from-row-list csv:from-file "inputFiles/we_data_prob.csv"
    set daily_wd_freq (matrix:map * daily_wd_freq daily_we_freq)
    let price_inter matrix:from-row-list csv:from-file "inputFiles/price_inter.csv"
    let price_factor matrix:get hourly_cost 0 0 ; First value is always equal to the
    matrix:set-row hourly_cost 0                ; weekend price for our dataset
    matrix:get-row matrix:times price_factor price_inter 0 ] ]
if (nw != 0) [
  if (nticks / ((120 + 48) + (nw * 168))) = int (ticks / ((120 + 48) + (nw * 168)))[
    set hourly_proba matrix:from-row-list csv:from-file "inputFiles/Data_prob.csv"
    set daily_wd_freq (matrix:map / daily_wd_freq daily_we_freq)
    if price_scenario != 2 [
      if nd < 30 [ ; Ontario energy board: TOU prices changed april 30 2013
        set hourly_cost
        matrix:from-row-list csv:from-file "inputFiles/electricity_prices_winter0.csv" ]
      if nd > 29 AND nd < 214 [
        set hourly_cost ; Ontario energy board: TOU prices changed october 31 2013
        matrix:from-row-list csv:from-file "inputFiles/electricity_prices_summer.csv" ]
      if nd > 213  [
        set hourly_cost
        matrix:from-row-list csv:from-file "inputFiles/electricity_prices_winter.csv" ] ] ] ]
end

; PROCEDURE CALLED BY OTHER BUTTONS THAN GO AND SETUP
to show_link
ifelse show_link? [
  ask links [
    hide-link ]
  set show_link? false ] [
  ask links [
    show-link ]
  set show_link? true ]
end

to test
; Here some procedure can be tested
output-print "No procedure added to test button"
end

; END, see info tab for more details.
@#$#@#$#@
GRAPHICS-WINDOW
174
10
664
596
-1
-1
15.0
1
10
1
1
1
0
0
0
1
0
31
-36
0
1
1
1
ticks
30.0

BUTTON
88
10
172
56
setup
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
1
10
86
55
go
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
252
764
318
797
test
test
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
173
764
250
797
Show Links
show_link
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
1
58
173
91
n_bhvr_epicures
n_bhvr_epicures
0
3
3
1
1
NIL
HORIZONTAL

SLIDER
1
93
173
126
P_conform
P_conform
0
1
0.275
0.1
1
NIL
HORIZONTAL

SLIDER
0
127
173
160
P_sbhvr_stalwarts
P_sbhvr_stalwarts
0
1
0.2
0.01
1
NIL
HORIZONTAL

SWITCH
0
163
172
196
SmallWorldNetwork?
SmallWorldNetwork?
1
1
-1000

SLIDER
0
198
172
231
K_rgraph
K_rgraph
0
999
99
1
1
NIL
HORIZONTAL

SLIDER
3
512
170
545
turtle_multiplier
turtle_multiplier
1
10
1
1
1
NIL
HORIZONTAL

SLIDER
3
302
171
335
proportion_agent_type
proportion_agent_type
1
3
1
1
1
NIL
HORIZONTAL

SWITCH
3
547
171
580
all_electric?
all_electric?
0
1
-1000

SLIDER
3
337
170
370
goal
goal
1
4
2
1
1
NIL
HORIZONTAL

SWITCH
2
441
170
474
up_north?
up_north?
1
1
-1000

SLIDER
4
406
170
439
rebound_scenario
rebound_scenario
1
4
1
1
1
NIL
HORIZONTAL

SWITCH
3
477
171
510
h&c_opti?
h&c_opti?
1
1
-1000

SLIDER
4
582
172
615
bhvr_set
bhvr_set
1
4
1
1
1
NIL
HORIZONTAL

SWITCH
2
233
172
266
random_seed?
random_seed?
1
1
-1000

SLIDER
1
268
172
301
impact_goal
impact_goal
0
15
9
1
1
NIL
HORIZONTAL

SLIDER
1
617
173
650
re_impact_goal
re_impact_goal
0
10
7
1
1
NIL
HORIZONTAL

SLIDER
1
653
173
686
vision
vision
1
3
1
1
1
NIL
HORIZONTAL

SLIDER
0
688
172
721
individual_vision
individual_vision
0
1
0
1
1
NIL
HORIZONTAL

SLIDER
0
724
172
757
rewiring_prob
rewiring_prob
0
1
0
0.01
1
NIL
HORIZONTAL

SWITCH
1
758
169
791
gamma_distrib?
gamma_distrib?
1
1
-1000

SWITCH
1
792
169
825
RE2ndAssumption?
RE2ndAssumption?
1
1
-1000

SWITCH
-1
826
169
859
agent_decision_process?
agent_decision_process?
0
1
-1000

SLIDER
1
861
170
894
feedback_frequency
feedback_frequency
1
4
3
1
1
NIL
HORIZONTAL

SLIDER
172
832
316
865
gamma_alpha
gamma_alpha
0.2
1
1
0.1
1
NIL
HORIZONTAL

SLIDER
172
799
316
832
gamma_lambda
gamma_lambda
0.05
0.07
0.07
0.005
1
NIL
HORIZONTAL

SLIDER
3
372
170
405
price_scenario
price_scenario
1
3
1
1
1
NIL
HORIZONTAL

SLIDER
1
896
171
929
mix_scenario
mix_scenario
1
3
3
1
1
NIL
HORIZONTAL

SWITCH
172
866
316
899
constant_weather?
constant_weather?
1
1
-1000

PLOT
175
598
416
763
Hourly electric consumption
time (hours)
kW
0.0
10.0
0.0
10.0
true
true
"" ""
PENS
"SB kWh" 1.0 0 -16777216 true "" "plot electric_consumption"
"BaU kWh" 1.0 0 -7500403 true "" "plot electric_consumption2"

PLOT
419
596
664
763
Hourly cost
time (hours)
$
0.0
10.0
0.0
10.0
true
true
"" ""
PENS
"SB cost" 1.0 0 -16777216 true "" "plot costbis"
"BaU cost" 1.0 0 -7500403 true "" "plot costbis2"

OUTPUT
319
764
664
900
11

PLOT
665
10
1069
181
Climate change impact:  hourly variation
time (hours)
kgCO2eq
0.0
0.0
0.0
0.0
true
true
"" ""
PENS
"SB Emissions all tenants" 1.0 0 -16777216 true "" "plot cc_default_impactbis"
"SB Emissions IT users  " 1.0 0 -10899396 true "" "plot cc_impactbis"
"SB Emission IT users w RE" 1.0 0 -2674135 true "" "plot cc_impact_tot"
"BaU Emissions" 1.0 0 -13345367 true "" "plot cc_default_impact_building"

PLOT
666
183
1069
353
Human health impact:  hourly variation
time (hours)
DALY
0.0
0.0
0.0
0.0
true
true
"" ""
PENS
"SB Emissions all tenants" 1.0 0 -16777216 true "" "plot hh_default_impactbis"
"SB Emissions IT users  " 1.0 0 -10899396 true "" "plot hh_impactbis"
"SB Emissions IT users w RE" 1.0 0 -2674135 true "" "plot hh_impact_tot"
"BaU Emissions" 1.0 0 -13345367 true "" "plot hh_default_impact_building"

PLOT
666
356
1070
527
Ecosystem quality impact: hourly variation
time (hours)
PDF*m2*yr
0.0
0.0
0.0
0.0
true
true
"" ""
PENS
"SB Emissions all tenants" 1.0 0 -16777216 true "" "plot eq_default_impactbis"
"SB Emissions IT users" 1.0 0 -10899396 true "" "plot eq_impactbis"
"SB Emissions IT users w RE" 1.0 0 -2674135 true "" "plot eq_impact_tot"
"BaU Emissions" 1.0 0 -13345367 true "" "plot eq_default_impact_building"

PLOT
666
529
1071
698
Ressource impact: hourly variation
time (hours)
MJ primary
0.0
0.0
0.0
0.0
true
true
"" ""
PENS
"SB Emissions all tenants" 1.0 0 -16777216 true "" "plot res_default_impactbis"
"SB Emissions IT users" 1.0 0 -10899396 true "" "plot res_impactbis"
"BaU Emissions" 1.0 0 -13345367 true "" "plot res_default_impact_building"

PLOT
666
699
1070
869
"Ecological footprint": hourly variation
time (hours)
Point
0.0
0.0
0.0
0.0
true
true
"" ""
PENS
"SB Emissions all tenants" 1.0 0 -16777216 true "" "plot ss_default_impactbis"
"SB Emissions IT users" 1.0 0 -10899396 true "" "plot ss_impactbis"
"SB Emissions IT users w RE" 1.0 0 -2674135 true "" "plot ss_impact_tot"
"BaU Emissions" 1.0 0 -13345367 true "" "plot ss_default_impact_building"

PLOT
1073
11
1273
182
Passive0 (cc_impact)
NIL
NIL
0.0
0.0
0.0
0.0
true
false
"" ""
PENS
"impact_tot" 1.0 0 -2674135 true "" "ifelse (ticks = 0) [ ][plot [cc_live_impact_tot] of turtle 0]"
"impact" 1.0 0 -10899396 true "" "ifelse (ticks = 0) [ ][plot [cc_live_impact] of turtle 0]"
"default_impact" 1.0 0 -13345367 true "" "ifelse (ticks = 0) [ ][plot [cc_live_default_impact] of turtle 0]"

PLOT
1073
183
1273
353
YellowTurtle (cc_impact)
NIL
NIL
0.0
0.0
0.0
0.0
true
false
"" ""
PENS
"impact_tot" 1.0 0 -2674135 true "" "ifelse (ticks = 0) [ ][plot [cc_live_impact_tot] of turtle yellowguy]"
"impact" 1.0 0 -10899396 true "" "ifelse (ticks = 0) [ ][plot [cc_live_impact] of turtle yellowguy]"
"default_impact" 1.0 0 -13345367 true "" "ifelse (ticks = 0) [ ][plot [cc_live_default_impact] of turtle yellowguy]"

PLOT
1074
356
1346
528
Behavior adoption
time (hours)
Number of households
0.0
10.0
0.0
100.0
true
true
"" ""
PENS
"Turtle w/ 1 bhvr" 1.0 0 -1184463 true "" "ifelse (count turtles with [n_bhvr = 1 and color != grey] = 0)[plot 0][plot round ((count turtles with [n_bhvr = 1 and color != grey] / count turtles with [color != grey]) * 100)]"
"Turtle w/ 2 bhvr" 1.0 0 -955883 true "" "ifelse (count turtles with [n_bhvr = 2 and color != grey] = 0)[plot 0][plot round ((count turtles with [n_bhvr = 2 and color != grey] / count turtles with [color != grey]) * 100)]"
"Turtle w/ 3 bhvr" 1.0 0 -2674135 true "" "ifelse (count turtles with [n_bhvr = 3 and color != grey] = 0)[plot 0][plot round ((count turtles with [n_bhvr = 3 and color != grey] / count turtles with [color != grey]) * 100)]"
"Turtle w/ 4 bhvr" 1.0 0 -534828 true "" "ifelse (count turtles with [n_bhvr = 4 and color != grey] = 0)[plot 0][plot round ((count turtles with [n_bhvr = 4 and color != grey] / count turtles with [color != grey]) * 100)]"
"Turtle w/ 5 bhvr" 1.0 0 -5509967 true "" "ifelse (count turtles with [n_bhvr = 5 and color != grey] = 0)[plot 0][plot round ((count turtles with [n_bhvr = 5 and color != grey] / count turtles with [color != grey]) * 100)]"

SLIDER
172
900
344
933
PV&Battery_scenario
PV&Battery_scenario
1
3
2
1
1
NIL
HORIZONTAL

SWITCH
346
901
497
934
inventory_detail?
inventory_detail?
1
1
-1000

SLIDER
499
901
650
934
q-model
q-model
1
4
4
1
1
NIL
HORIZONTAL

@#$#@#$#@
## WHAT IS IT?

This agent-based model models the use of electricity by a set of standard homes and a set of smart homes. It aims at studying the environmental impacts of both sets. Smart homes follow a simple set of rules: its households can adopt or give up pro-environmental behaviors dependent on other agents situation and difficulty of changing habits.

The model shows the importance of peer pressure when changing one's habits. No "centralized cause" explains each agent behavior as it depends partly on other agents' states. Moreover dynamic changes in the system and its context are included in the ABM.

## HOW TO USE IT

The ABM can be controled from the interface tab. The SETUP and the GO buttons are the main controls. The model also uses various inputs. The NetLogo CSV-Extention allows to import those inputs in the model while the NetLogo Matrix Extension convert those data into matrices.

### Interface tab

Various parameters can be setup to vary the conditions of the simulation (see below for a detailed description). The SETUP button build household agents' social network, the system process as a weighted Network and initial values of global and agent-owned variables.

Set the desired number of agents, values for peer pressure strenght (P_conform) and difficulty of changing behaviors (P_sbhvr_stalwarts). Agent subtypes distribution, system location, and price scenario can also be changed with appropriate sliders.

The GO button wil start the simulation: agents will consume electricity and change related behaviors. As a consequences their electricity consumption profiles will change in time.

### Model inputs

In order to change model inputs create csv files (e.g. from a spreadsheet) containing your data. The inputs are then converted into matrices. Be aware that for a given matrix it may be possible that part of the code uses constants rather than a variable containing matrix dimensions to acess data: hence adapt the code or your data to avoid errors when running the model. The csv files provided with the model and containing our own data are detailed in the credits and references subsection.

## THINGS TO NOTICE

Adoption of behavior changes through time and depending on model's parameters different curves can be obtained. For instance, from an initial "seed" (early adopters) adoption within the population can be:

i) fast before reaching a plateau (the maximum possibly adopted);

ii) slow, without reaching the plateau (but with more simulation time would probably reach one);

iii) be in a state of "dynamic equilibrium" where agents continually adopt and give up pro-environmental behaviors;

iv) decrease quickly.

Following the adoption curve, electricity consumption, cost and impact curves change. Moreover, not accounting for context through temperature (up_north? and constant_weather?) or mix variability (mix_scenario) can change desaggregated and aggregated results.

## THINGS TO TRY

Change the values of P_conform and P_sbhvr_stalwarts allows to explore different aggregated results on adoption and other metrics. Also changing metrics for load scheduling and price scenarios are interesting configurations to explore. Different experimental design with the behavior space module of NetLogo can be designed.

Explore different social network and its impact on the result or change the value of q in the q voter model are also things that can be interesting to try.

## EXTENDING THE MODEL

Incorporate different social psychological models such as the theory of planned behavior can be a relevant model extention. Improving the solar PV electricity production model would be of great value to explore different smart homes scenarios. Full buildings, solar PV and IT infrastructure LCA could be added when calculating the impact. The model could run on longer than 1-year dataset. Finally, a heat transfer model could be added to link more preciselly external temperature and energy need.

## NETLOGO FEATURES

CSV and matrices extention are widely use in this ABM. Moreover the behavior space module is relevant in this ABM as there are a great number of parameters that can be changed: it allows to explore different situation for smart homes development.

## HOW IT WORKS, CREDITS, AND REFERENCES

### Abbreviations

-Electricity = elec
-Behavior(s) = bhvr
-Rebound effect = RE
-Subject to = s.t.
-Stove and oven = s&o, fridge = ref, freezer = fre, dishwasher = dish, clothe washer = cwah, dryer = dry, other load = otha, lightnings = light, surface heating = sheat, water heating = wheat, surface cooling = scool.
-ht = human toxicity (line 0), re = respiratory effect (line 1), ir = ionizing radiation (line 2), old = ozone layer depletion (line 3), po = photochemical oxidation (line 4), ae = aquatic ecotoxicity (line 5), te = terrestrial ecotoxicity (line 6), tan = terrestrial acidification, nutrification (line 7), lo = land occupation (line 8), cc = climate change (line 9), nre = non-renewable energy (line 10), rme = raw material extraction (line 11), hh = human health (line 12), eq = ecosystem quality (line 13), res = ressource (line 14), ss = single score (line 15)
-O = optimization
-EEIO = Environmentally Extended Input Ouput Tables
-HDD / CDD = heating / cooling degree days
-TOU = Time Of Use

### Data and assumptions:

#### Energy data

Energy data (distribution of appliances in Ontario's population and 2013-2014 energy used in Ontario per appliances) are taken from:
 http://oee.nrcan.gc.ca/corporate/statistics/neud/dpa/showTable.cfm?type=CP&sector=res&juris=on&rn=31&page=4

Studying the data conducted to assume that housholds owning a clothe washer also own a dryer.

#### Energy consumption profiles:

The method from Paatero & Lund (2006) was modified to draw agents' energy consumption profiles. The probability for each household (agent) i, of using an appliance ai on day d at a hour h is:

Pusage(ai, d, h) = Pday(ai, d) * Phour(ai, d, h)

With Pday the probability that the appliance a is used on day d and Phour the probability that the appliance a is used during hour h. Once an appliance is used at a certain hour a load lasting the whole hour is generated. Although this method is not as accurate than Paatero's it is simpler to implement in the ABM. Values are taken from Paatero & Lund (2006) and related input files are: hh_n&E_app, hh_n&E_app_all_elec, Data_prob, we_data_prob, daily_wd_freq, daily_we_freq (see below for more details on input files).

We assume that Ontarian statistical energy data represents well Toronto's situation.

#### Weather data

The correlation between space heating and cooling demands and the external temperature is accounted for by means of the definition of heating and cooling degree days (respectively HDD and CDD). The base temperature Tb is set at 18˚C. Each day, yearly space heating and cooling demands are multiplied by the ratio of the corresponding HDD (or CDD) and its yearly sum. Temperatures used to compute HDDs and CDDs are taken from:

Government of Canada (2016). Historical Climate Data.   Retrieved from http://climate.weather.gc.ca/

considering the April 2013 – March 2014 period and the Toronto weather station.

#### Photovoltaic (PV) system

Hourly production are taken from :

National Renewable Energy Laboratory (NREL). (2018). PVWatts Calculator.   Retrieved from http://pvwatts.nrel.gov/pvwatts.php

Considering Toronto and Thunder Bay as geographic locations. We also assumed rooftop solar panel oriented south. All default parameter for this stype of solar panel have been kept from the PVWatts Calculator except that the power value have been set to 3 kW, value taken from:

Adepetu, A., & Keshav, S. (2016). Understanding solar PV and battery adoption in Ontario: an agent-based approach. Paper presented at the e-Energy.

The battery is considered to be 8 kWh, value taken from the reference above (Adepetu et al. (2016). The PVWatts Calculator consider a "typical" year at the location chosen (average of time series (20 year average)). Finally from hourly data we obtain daily ones.

PV&Battery_scenario: 1 --> no PV system installed on either home (smart & standard), 2 --> PV system installed on both the smart and the standard home, 3 --> PV system installed on the smart home only

Assumptions:

i) When PV&Battery_scenario = 2 is selected: the PV system LCA is limited to the use phase (as for the rest of the study, two similar homes are compared and hence all life cycle phase except the use phase are similar and can be excluded from the analysis). Under PV&Battery_scenario = 3 the similarity assumption does not hold anymore but we did not consider it, hence care must be taken when analysing results under that configuration. As with the LCA of the IT infrastructure an LCA of the PV system would greatly enhanced results' value.

ii) We considered that the battery store the PV electricity production of the prior day when it is used. (Hence while the model starting day is 01/04/2013, and the end day is 31/03/2014 (included) or 01/04/2014 (not included) the data PV_output_Toronto & PV_output_ThunderBay have for starting day 31/03/---- (fictionnal date = 20-years average) and ending day 30/03/---- (fictionnal date = 20-years average)). If the PV electricity production is superior to the battery capacity, the excess is sold according to the microfit program, data from :

Independent Electricity System Operator (IESO). (2018). microFIT Program Documents.   Retrieved from http://www.ieso.ca/get-involved/microfit/program-documents

01/04/2013 - 26/08/2013: microfit_price = 0.0549
26/08/2013 - 31/03/2014: microfit_price = 0.0396

Finally, if the electricity consumption at any hour h0 is inferior to the battery capacity (8 kWh) the excess is "spillovered" h0 - 1 or h0 + 1 depending if h0 is superior to midday or not. The process repeat untill the battery is emptied or hour 1 or 24 is reached (in that case a negative value of electricity consumption could theoretically appear but never do in practice).

iii) In the scenario 2 we evaluate the advantage of smart homes' load shifting capabilities: both standard and smart homes have a PV&Battery system. However in the standard home the battery can be used any time during the day (chosen in a randomly fashion) while in the smart home the model optimization heuristic applies and hence the battery is used in order to decrease peaks. It is a departure from reality but nonetheless permit to get an idea on the potential of load shifting when PV systems are involved.

Assumptions consequences:

Assumption i) diminish the LCA study value, especially if the scenario 3 is chosen. Assumption ii) makes the model less realist. Indeed in the real world we could expect the household to use all of the output of the solar panel (using the battery only if necessary) to avoid paying for electicity as its TOU prices are all higher than the microFIT prices for the 04/13-04/14 period. Nonetheless, incorporating PV system in the model gives an idea of the benefits we could expect when comnining this technology to smart home technology.

#### Electry prices:

Electricity prices are taken from the Ontario Energy Board for the April 2013 - April 2014 period (Time Of Use (TOU) pricing):

(Ontario Energy Board. (2013). Regulated Price Plan Price Report May 1, 2013 to April 30, 2014 Retrieved from
 http://www.ontarioenergyboard.ca/oeb/_Documents/EB-2004-0205/RPP_Price_Report_May2013_20130405.pdf

and Ontario Energy Board. (2013). Regulated Price Plan Price Report November 1, 2013 to October 31, 2014. Retrieved from

http://www.ontarioenergyboard.ca/oeb/_Documents/EB-2004-0205/RPP_Price_Report_Nov2013_20131017.pdf )

Other pricing scenarios can be tested in the model:

-The constant price scenario use a value computed from 30 simulations using the TOU pricing in order to allow comparability between scenario (the adopted perspective is that the utility would set up a TOU or a constant price that provide the same revenue): the simulations averaged amount paid by all households for electricity is divided by the simulations averaged electricity consumed by all households. The mean obtained is 0.08475226 (+/- 9.35E-10). Standard deviation is low which indicates that results obtained under TOU or constant price can be reasonably compared.

-An Algorithm for a real-time pricing scenario was implemented: again to allow a meaningful comparison between scenario real-time pricing and TOU scenarios, the algorithm was built in order to ensure an equivalent revenue for the utility.

First the average daily cost of electricity for the population under the TOU scenario is calculated (average_price). Then the population daily median load is retrieved (median_consumption). A linear parameter is then calculated as: average_price  / median_consumption (linear_parameter).

For each hour h of the day the difference between the electricity consumption for h and median_consumption is computed and the vector obtained is then multiplied by linear_parameter. The vector is further muliplied by the ratio between the total daily cost obtained with the TOU scenario and the previous steps in order to ensure comparability.

Verification of the algorithm showed that total yearly cost (or utility revenue) obtained with the TOU scenario and the real-time pricing scenario were the same.

#### Agents' behavioural differences

Distribution of the population is coming from: Valocchi, M., Schurr, A., Juliano, J., & Nelson, E. (2007). Plugging in the consumer. IBM Institute for Business Value.

Assumptions:
i) We know the percentages of passives and stalwarts from the reference but we assume that the rest is equally distributed between frugals and epicures;
ii) We have for the USA: passives = 45, frugals = 14, epicures = 14, stalwarts = 27 (if pop = 100) (we assume situation is the same in Canada (both Western countries)). All other values in the model come from the same reference and use assumption i).

Agent types differs in two ways: the maximum number of pro-environmental behaviours they can adopt throughout the simulation and their attitude (see below for more details).

### Kaiser and Byrka social psychological model:

To account for households’ change of behavior due to energy feedback we adopt a similar approach to Birka et al. (2016): a q-voter model (with q = 4) (Castellano et al. (2009)) is combined with Kaiser’s concept of difficulty (Kaiser et al. (2010)).

After a certain number of ticks (determined by the slider "feedback_frequency": 1 = 1 tick, a hour; 2 = 24 ticks, a day; 3 = 168 ticks, a week; 4 = 720, a month), agents decide to change or not their electricity consumption behaviors (by adopting pro-environmental ones (e.g. turn off the lights when living a room etc.)):

-First, agents are subjected to the influence of their peers (social norm). Indeed this factor was proven to be influential when households make energy related decisions (Ayres et al. (2013)).

Agents roll a dice and are subjected to social norm if the result is lower than the probability of conforming to peers (set by the modeller). If so they select four agents with which they are connected to (through a Network which type is left to the modeller) and compare their own number of adopted pro-environmental behaviors with those of the selected agents. If selected agents unanimously have more (or less) adopted pro-environmental behaviors than the agent, then the later adopt one more (or give up one) pro-environmental behaviors (if its maximum number of pro-environmnetal behavior that can be adopted has not been reached yet (or if it superior to zero in the case where a pro-environmental behavior is given up)). If selected agents are not unanimous, nothing happens.

-Second, agents are subjected to the difficulty of changing their everyday life habits into pro-environmental ones. As a broad concept difficulty allows to represent a number of factors hingering change in behaviour (i.e. effort (which regroups different psychological factors) and personal ressource (such as time and money)) Kaiser et al. (2010)).

Agents compare their own consumption to the population lowest and if superior, roll a dice. If the result is lower than the probability of adopting a new pro-environmental behaviour (P_sbhvr), they do so (if their maximum number of pro-environmnetal behavior that can be adopted have not been reached yet).

The figure below summarize agents' behaviour rule:

![Agents' decision rule](file:Images\AgentDecisionRule.png)

Pc probability to conform, Pk probability to adopt a new pro-environmental behaviour, ntj number of pro-environmental behaviors adopted at time-step t by agent j, Cj electric consumption of agent j, Cmin is the minimum electric consumption in the population, U[0;1] the result of a single draw of a continuous uniform distribution and the reversed U is the intersection operator.

P_sbhvr decreases when the adopted number of behavior increases (n_bhvr) (in acccordance with Kaiser theory) increases: it is more and more difficult to adopt new pro-environmental behaviors (P_sbhvr decreases by 0.57 (diff_discrepancy) when n_bhvr increases by 1). The value 0.57 has been taken from Kaiser's study (computation of an "average" difficulty based on empirical scores for 2 home behaviors: "in winter I lower space heating when I leave home", "I wash my clothes without pre-washing" (Kaiser et al. (2010)).

P_sbhvr also depends on agent's attitude. Similarly to above, Kaiser's study is exploited to compute attitude level difference between agent types from empirical data. Attitude difference is calculated for 20 behaviours for two groups of individuals. Kaiser's prediction is verified as the differences are approximately the same independently of behaviors. We averaged the attitude differences obtained which gives a value of 0.71. To adjust P_sbhvr according to agents attitude and behaviour difficulty we have in the model (Rasch model taken from Kaiser et al. (2010)):

P_sbhvr_avg = (((P_sbhvr_stalwarts / (1 - P_sbhvr_stalwarts)) * exp (-1 * att_discrepancy)) / (((P_sbhvr_stalwarts / (1 - P_sbhvr_stalwarts)) * exp (-1 * att_discrepancy)) + 1))

(P_sbhvr_avg and P_sbhvr_stalwarts being the probability of adopting a new behavior depending on the subtype of agents (P_sbhvr_stalwarts for stalwarts and frugals agents, P_sbhvr_avg for epicures and passives agents)).

P_sbhvr(t+1) = (((P_sbhvr(t) / (1 - P_sbhvr(t))) * exp (-1 * diff_discrepancy)) / (((P_sbhvr(t) / (1 - P_sbhvr(t))) * exp (-1 * diff_discrepancy)) + 1))

(The above function is used when a new pro-environmental behavior is adopted by an agent, its inverse is used when an agent give up a pro-environmental behavior).

The maximum number of pro-environmental behavior adopted by epicures agents is set by the modeler (up to 3) (use n_bhvr_epicures slider). Frugals and passives can adopt a maximum of n_bhvr_epicures + 1 pro-environmental behaviors while stalwarts can adopt n_bhvr_epicures + 2. This was determined from Valocchi et al. (2007) definitions of energy consumer types.

### Consequences of pro-environmental behavior adoption:

We aim to model what happens when households subjected to real-time electricity consumption feedback provided by a smart home adopt pro-environmental behaviors. However the nature of the behaviors that are adopted by a given household  at the end of the decision process and their consequences on electricity consumption is not exactly known. In fact that behavior will depend on the given household, its socio-economical characteristics, its values etc. Hence, each households might adopt different behaviors and thus a deterministic consequence following the agent decision process might not be the most realistic modelling way.

Hence, the algorithm in charge of translating adoption of new behaviours into change in electricity consumption profiles accounts for the heterogeneity that can exist in households population, and each agents adopt their own pro-environmental behaviors whose consequences on electricity consumption profiles can be different. For instance of two agents that adopted one pro-environmental behaviour, one might have a new electricity consumption profile where peak hour load is shifted to an off-peak period and the other might have a new electricity consumption profile where space heating daily load is reduced by 2.6% (reduction independant to the hour of the day).

A roulette wheel process is exploited to select one consequence amongs all possibilities following an agent's decision. Weight on roulette wheel are determinded from:

Ehrhardt-Martinez, K., Donnelly, K. A., & Laitner, S. (2010). Advanced metering initiatives and residential feedback programs: a meta-review for household electricity-saving opportunities. p73

Each frequency reported in the ACEEE document is given a value (low = 1 up to very high = 6). Then, the weight of a behavior in the roulette wheel process is equal to:

sum(score of each bhvr in the category of appliance) / sum(score of each bhvr in each category of appliance)

The figure below shows appliances' probabilities to be subjected to a change of behaviour used in the roulette wheel process:

![roulette wheel probabilities](file:Images\RouletteWheel.png)

Behaviors related to surface heating (or cooling) appears only if HDD >= CDD (resp CDD > HDD). Then, once the appliance which is subjected to a consequence is selected actual values are taken from empirical data listing percentage reduction or shift of electricity load. Each consequence is given the same weight in this process. Empirical data are taken from Paetz et al. (2012), Paatero et al. (2006), Ehrhardt-Martinez et al. (2010), Asensio et al. (2015), Ueno et al. (2006), Wood et al. (2003), Hydro One Networks (2008), Ceniceros et al. (2009), Aguilar et al. (2005), Abrahamse et al. (2005), Darby et al. (2006), Benders et al. (2006), McCalley (2006), Whithanage (2016).

Hence the process summarized in agents choosing an appliance where to change its habits according to empirical data (and so according to Kaiser's model because easiest pro-environmental behaviors are also the ones that are the most common in a given population (comes from Kaiser's definition (Kaiser et al. (2010))). Moreover once an appliance has been selected the consequence on electricity consumption profile also come from empirical data which participate to validate the model (Sopha et al. (2017)).

What we described above accounts for the baseline case of the ABM (bhvr_set slider = 1 --> set up bhvr_matrix1 constructed as mentionned above) as it is primarily built on empirical data. However other situations can be simulated:

i) bhvr_set slider = 1 --> bhvr_matrix1 = empirical data, assumption and optimization model (to represent automatic scheduling feature of the smart home) --> automatic peak shaving, occupant peak shaving and energy conservation;
ii) bhvr_set slider = 2 --> bhvr_matrix2 = modeler choice of behaviour and optimization model --> automatic peak shaving (only fridge, freezer and heating / cooling loads) occupant peak shaving and energy conservation as decided by modeler;
iii) bhvr_set slider = 3 --> bhvr_matrix3 = empirical data and assumption --> no peak shaving but occupant energy conservation;
iv) bhvr_set slider = 4 --> bhvr_matrix4 = optimization model only --> automatic peak shaving but no occupant energy conservation (but still dependant on agent's decision; to get a 100% independant optimization model: set agent_decision_process? to false).

### Network construction and characteristics measurements:

#### Network construction:

Although it is possible to try different Network type, baseline case is a fully connected Network (set slider K_rgraph to 99, rewiring_prob to 0 and gamma_distrib? to false). Other type of Network that can be constructed are:

i) Built with the random-gamma primitive: shape parameter alpha = 1 and reciprocal of scale parameter lamda = 0.07 gives about the same average number of neighboor per agents than the small-world option with K_rgraph = 10, rewiring_prob = 0.1 (20) but the shape of the distribution is different (and standard deviation). Alpha and lambda can also be set by the modeler with sliders gamma_alpha and gamma_lambda.

This simple method is based upon reading Querini et al. (2014) (including supplementary information), Albert et al. (2002) and Eppstein et al. (2011), the underlying assumption of drawing agents number of neighbors with a gamma distribution is that a deviation of Poisson distribution showing a power-law tail (which can be drawn with the random gamma primitive) is often seen in real-world social Network.

Note: Because agents can create links to neighbors who already created their links the real distribution does not fit the distribution given by the random-gamma primitive. For instance for 100 000 draws of the random gamma primitive (alpha = 1, lambda = 0.05) we obtain an average number of neighbors of around 25 with a standard deviation of 14 and the shape of the distribution curve is close to a gamma distribution with alpha = 2.9 and lambda = 0.1 (with a mean of around 25 and standard deviation of around 14).

ii) Small-world network: Most of the algorithm is adapted directly from

Wilensky, U. (2005). NetLogo Small Worlds model.

Modeler can choose the rewiring probability used for the small world with the help of the slider "rewiring_prob". The number of edges per vertex in the equivalent regular lattice can also be chosen by the modeller with the help of the slider "K_rgraph". K_rgraph = 10 and rewiring_prob = 0.1 gives an average number of neighboor per agents of 20, which is arbitrarly choosen for gamma distribution and the small-world. Querini et al. (2014) and others above mentionned have similar values in their studies.

Ensuring Small-world:
Individual agents number of neighboors with K_rgraph = 10 (and rewiring_prob = 0.1): 20 Network with k edges per nodes and n nodes. Hence in the model with parameters fixed at values above: number of edges per node = 20 (for 100 nodes (agents) # edges = 2000). According to: Watts, D. J., & Strogatz, S. H. (1998). Collective dynamics of ‘small-world’networks. Nature, 393(6684), 440-442. We need for a small-world: n >> k >> ln(n) >> 1 Considering all values above: 100 = 5*20 (# of nodes is five times bigger than the number of edges per vertex) and we obtain 20 = about 4*ln(100)  ( ln(100) = about 4.6 ) and 4.6 = about 5*1. So we have about half an order of magnitude for each conditions. By comparison Strogatz et al. have: n = 1000, k = 10, hence 1000 >> 10 and ln(1000) = 6.9 which is about 7 times greater than 1 and 10 is about 1.5 times greater than ln(1000). Finally, except for the value 1000 >> 10 which compare poorly to 100 >> 20 we have similar conditions than Strogatz et al. This difference diminishes when number of agents increases.

iii) Random network: is similar to the small-world except rewiring_prob is set to 1.

#### Network characteristics:

The model provides a way to characterize the Network (those answering Small-World requirements, i.e. n >> k >> ln(n) >> 1) by calculating both the clustering coefficients and the average path-lenghts of the modeled network and its regular lattice or random equivalents. Algorithms are adapted from:

Wilensky, U. (2005). NetLogo Small Worlds model.

Two coefficients are then computed for the modeled network:

-s small world coeff: Watts, D. J., & Strogatz, S. H. (1998). Collective dynamics of ‘small-world’networks. Nature, 393(6684), 440-442.

-w small world coeff: Humphries, M. D., Gurney, K., & Prescott, T. J. (2006). The brainstem reticular formation is a small-world, not scale-free, network.

The latter being more accurate in characterizing a Small-World network. In the output part of the GUI the modeler is able to see if the modeled network can be considered a small-world network as well as if it is closer from a regular lattice or a random network.

### Hybrid life cycle assessment:

#### LCA

In general the LCA follows Davis et al. (2009): the technology matrix is represented by a weighted network of agents (technology agents). Each technology agent owns elementary flows and its system contribution.

#### Goal and scope:

The ABM aims to assess difference in environmental impact between a set of smart homes and a set of standard homes ("ceteri paribus" i.e. standard and smart homes are considered equivalent in all aspect except that smart homes provide real-time electricity consumption feedback and load scheduling services to households; smart homes IT infrastructures impacts have been assessed in another study and are not accounted for here). Electricity processes are all "system", meaning they contains all their life cycle elementary flows. Electricity production includes plant construction and other related activities. However as standard and smart homnes are considered equivalent no others building processes such as materials end of life or production are included, in other words, only the homes' use phase of is included in the assessment. The LCA can be qualified as consequential.

The functionnal unit is:

"Use of home electrical appliances by residents of a home in Toronto from April 2013 to April 2014"

When appliances are used, the functionnal unit that is used is "# kWh". Water use is excluded.

System boudaries are mentionned above: only the home use phase is included and impact of ICT hardware is neglected (but was studied in a different project) (see the figure below).

![system's life cycle](file:Images\SystemLifeCycle.png)

In the figure we see the system (the Ontario electricity mix) at two different time steps (from top to bottom: April 1st 2013 00:00 & April 1st 2013 01:00).

Assumptions and limitations are mentionned all through the info tab.

No allocation occurs and included impact categories are impact2002+ ones: human toxicity, respiratory effect, ionizing radiation, ozone layer depletion, photochemical oxidation, aquatic ecotoxicity, terrestrial ecotoxicity, terrestrial acidification, nutrification, land occupation, climate change, non-renewable energy, raw material extraction. Impact 2002+ Endpoint categories (human health, ecosystem quality, climate change, resource) and single score are also included.

#### Life cycle inventory:

##### Data

The ecoinvent system processes used in the model are taken from "market for electricity, high voltage, alloc. default, U":

-electricity production, hard coal, alloc. default, S (CA-ON)

-electricity production, lignite, alloc. default, S (CA-ON)

-heat and power co-generation, wood chips, 6667 kW, state-of-the-art 2014, alloc.   default, S (CA-ON)

-heat and power co-generation, biogas, gas engine, alloc. default, S (CA-ON)

-treatment of blast furnace gas, in power plant, alloc. default, S (CA-ON)

-electricity production, natural gas, combined cycle power plant, alloc. default, S (CA-ON)

-treatment of coal gas, in power plant, alloc. default, S (CA-ON)

-heat and power co-generation, natural gas, conventional power plant, 100MW electrical, alloc. default, S (CA-ON)

-electricity production, natural gas, at conventional power plant, alloc. default, S (CA-ON)

-electricity production, hydro, pumped storage, alloc. default, S (CA-ON)

-electricity production, hydro, run-of-river, alloc. default, S (CA-ON)

-electricity production, hydro, reservoir, non-alpine region, alloc. default, S (CA-ON)

-electricity, high voltage, import from CA-MB, alloc. default, S (CA-ON)

-electricity, high voltage, import from NPCC, US only, alloc. default, S (CA-ON)

-electricity, high voltage, import from Quebec, alloc. default, S (CA-ON)

-electricity production, nuclear, pressure water reactor, heavy water moderated, alloc. default, S (CA-ON)

-electricity production, oil, alloc. default, S (CA-ON)

-market for transmission network, long-distance, alloc. default, S (GLO)

-market for transmission network, electricity, high voltage, alloc. default, S (GLO)

-electricity production, wind, 1-3MW turbine, onshore, alloc. default, S (CA-ON)

-electricity production, wind, <1MW turbine, onshore, alloc. default, S (CA-ON)

-electricity production, wind, >3MW turbine, onshore, alloc. default, S (CA-ON)

However with the exception of: market for transmission network, long-distance, alloc. default, S (GLO) and market for transmission network, electricity, high voltage, alloc. default, S (GLO) (units: km), values (unit: kWh) are taken from historical hourly data of Ontario's electricity production rather than the ecoinvent database (to build a hourly-desaggregated mix (see below)).

##### Construction of hourly desaggregated electrical mix:

To build the Ontarian hourly defined mix, data from the Independent Electricity System Operator (IESO) has been collected in a previous work ( Maurice, E., Dandres, T., Farrahi Moghaddam, R., Nguyen, K., Lemieux, Y., Cherriet, M., & Samson, R. (2014). Modelling of Electricity Mix in Temporal Differentiated Life-Cycle-Assessment to Minimize Carbon Footprint of a Cloud Computing Service. Paper presented at the ICT for Sustainability 2014 (ICT4S-14). ), IESO website: http://www.ieso.ca

Those data can be used to define the state of the mix (i.e. the contributions of each technologies to produce Ontario's electricity) at each hour of the period April 2013 - April 2014. The figure below show the state of the mix for one day and for two hours at a 6 month interval.

![ontario mix](file:Images\OntarioMix.png)

The mix is based on the model M3 of Itten et al. (2012), hence we have:

Domestic production - exports + imports = supply mix

In summary, each technology agent owns a vector containing its hourly contributon to the system (the mix) for the period April 13 - April 14.

The household agents use the supply mix to get their appliances working.

#### Life cycle impact assessment:

Similarly than system's contribution, each technology agent owns a vector containing its elementary flows per functionnal unit. The LCA is made each day for 24 hours for 1kWh of consumed electricity. Then households agents' electricity consumption is translated into impact simply by multipliying the elctricity consumption with the impact of electricity for 1 kWh. In summary we have:

TAU(t,m) = SUM(k(m))(c o BAt-1f)    (1)
GAMMAj = I(Fj LAMBDAj)    (2)

In eq.1: TAU(t,m) the impact factor (or amount of impact per functional unit) at hour t for the impact category m; k the number of characterization factor of impact category m; c the vector of all characterization factors (for all midpoint impact categories) of Impact2002+ (size 2755); B the 2755x16 elementary flows matrix; At the technology matrix at hour t and f the functional unit (here 1 kilowatt-hour).

In eq.2: GAMMAj the impact vector containing impact scores for all 12 midpoint categories of impact2002+; Fj the 24x11 binary matrix for appliances uses of agent j during the day; LAMBDAj the vector of hourly demands of appliances (being 0 when agent j does not own an appliance). In all equations o is the Hadamard product.

Notably, at the end of the process we get a 12x24 matrix I which contains impact factors for each hour of the day and midpoint impact categories of Impact2002+. The figure below illustrate the state of the system at two different time step (conceptual view):

![HourlyMixConcept](file:Images\HourlyMixConcept.png)

Legend: Technologies agents (T1, T2, T3) at two different time-steps (conceptual view); fu = functional units, At = system’s technology matrix at t, EF = elementary flows, SC = agent’s system contribution.

#### Impact assessment from consumption displacement (rebound effect):

Environmental impact from RE is estimated in two steps:

i) Savings allowed by smart homes are computed for each household agents at each time step using the hourly electricity price and the default or smart electricity consumption of the households. From Canadian Input-Ouput tables the final demand vector is then exploited to reallocate households’ savings (Lesage 2012 (Open IO-Canada online tool)) (expressed in % of total budget spent in any commodity). From Girod et al. (2011) framework, we use both consumption as usual (constant consumption budget and unchanged consumption preferences) as well as more of others (i.e. savings are reallocated to all consumption commodities following household preferences except electricity) concepts to estimate RE.

ii) expenditures in each commodity are converted in environmental impact using open IO-Canada (Lesage 2012 (Open IO-Canada online tool)). As opposed to the LCA impat factors are calculated prior to the simulation (stored in a csv file), hence commodities impact factors are not recalculated and the model does not contain the IO table per se (but the later was used to determine impact factors, as well as the final demand vector). For any day of a simulation:

r = ((Fj LAMBDAj)d - (Fj LAMBDAj)s) o p     (3)
Rtm = SUM(k(m))(rt CHItranspose E o c)    (4)

In eq.3: r the resource vector (in our case monetary savings); Fj and LAMBDAj defined above, (d meaning default consumption in equivalent standard home and s meaning
smart home); p is the hourly vector price (size 24). In eq.4: Rtm the environmental load imputable to RE at hour t in midpoint impact category m; k defined above; rt monetary savings at hour t; CHI Ontarian’s consumption mix (vector of size 151); E elementary flows matrix (size 151x321); c defined above. Finally, H a 8x24 matrix is obtained from Rt,m containing hourly emissions potentially due to RE for eight of the twelves midpoint categories of impact 2002+ (no elementary flows are reported for ionizing radiation, land use, energy, and mineral resource impact categories in open IO-Canada)

Different scenarios for the use of savings by the households can be explored with the button "rebound_scenario" of the interface :

rebound_scenario = 1: Consumption as usual (savings spent in all commodities except electricity with % according to statistic)
rebound_scenario = 2: Air Travel only
rebound_scenario = 3: financial commodities only

### Model's inputs for "An Agent-Based Model To Evaluate Smart Homes Sustainability Potential"

-appliances_elec_consump: a 5x12 null matrix which is filled in the model with appliances' energy consumption. Rows are type of energy (in order: electric, gaz, oil, wood, coal/propane) and columns are type of appliances (in order: stove&oven, fridge, freezer, dishwasher, clothe washer, dryer, other load (TC, computer etc.), light, space heating, water heating, space cooling).

-bhvr_matrix (and bhvr_matrix2, bhvr_matrix3, bhvr_matrix4) :  11x12 matrices containing possible consequences for each appliances (1 line = possible consequences for one appliance) of households change in behaviors. Consequence is expressed either as a percentage of standard load or a code indicating that the appliance is subject to a heuristic optimization algorithm that shift its load in time. bhvr_matrix = possible consequences taken from empirical data: K. Ehrhardt-Martinez et al. (2010), O. I. Asensio et al. (2015), T. Ueno (2006), G. Wood et al. (2003), http://www.ontarioenergyboard.ca/documents/cases/EB-2004-0205/smartpricepilot/TOU_Pilot_Report_HydroOne_20080513.pdf (accessed 15/07/2016), B. Ceniceros et al. (2009), C. Aguilar et al. (2005), W. Abrahamse et al. (2005) (they implies energy conservation and activation of automatic load sheduling by household agents. bhvr_matrix2 = user can set up its own consequences, currently set-up to a matrix of ones (no consequences), it can be used in experimental design (facilitated in NetLogo with behavior space module). bhvr_matrix3 = same as bhvr_matrix without the consequence of load scheduling activation, it can be used to strictly study energy conservation as a consequence of energy feedback. bhvr_matrix4 : same as bhvr_matrix without the consequence of load energy conservation, it can be used to strictly study load scheduling activation as a consequence of energy feedback.

-CDD_2014: a vector of size 365 containing cooling degree days during the period april 2013 - march 2014 for the Toronto weather station.

-CDD_2014_TB: a vector of size 365 containing cooling degree days during the period april 2013 - march 2014 for the Thunder Bay weather station.

-CDD_blank: a vector of size 365 containing ones used in calculation.

-Consumption_airtravel: a vector of size 151 containing contribution (in percent (ratio of dollars spent in a commodity to total income of an average Canadian household)) of Canadian input-output commodities in Canadian consumption under the assumption that only the commodity "Air transportation services" is bought with the savings allowed by the utilization of smart home features.

-Consumption_investement: a vector of size 151 containing contribution (in percent (ratio of dollars spent in a commodity to total income of an average Canadian household)) of Canadian input-output commodities in Canadian consumption under the assumption that only the commodity "Investment banking, security brokerage and securities dealing services" is bought with the savings allowed by the utilization of smart home features.

-Consumption_mix: a vector of size 151 containing contribution (in percent (ratio of dollars spent in a commodity to total income of an average Canadian household)) of Canadian input-output commodities in Canadian consumption under the assumption that only the commodity "Electricity" is not bought with the savings allowed by the utilization of smart home features.

-daily_wd_freq: a vector of size 12 which contains daily usage of appliances for week days. For instance in average the stove and oven is used 1.46 times. Values are taken from Paatero et al. (2006).

-daily_we_freq: a vector of size 12 which contains daily usage of appliances for weekend days expressed as a ratio compared to week days. For instance in average the stove and oven is used 1.46 x 1.082 times on a weekend day (aturday or sunday). Values are taken from Paatero et al. (2006).

-Data_freq: 11x24 zero matrix used to create the binary matrix holding information on hourly usage of appliances.

-Data_prob: a 12x1000 matrix containing information on usage hours of each appliance. Each line hold several times each hour of the day. The ratio of the number of time an hour is in the line over a 1000 is the probability of using the appliance at that hour (values from Paatero et al. (2006)). Hence in the code each line of the matrix act as a 1000 faces loaded dice which give usage hours of each appliance according to Paatero et al. (2006) frequencies.

-ef_mix: a 2755x16 matrix containing values for impact2002+ elementary flows for each of the 16 processes used to model the Ontarian mix. The elementary flows are ordered according to impact2002+ midpoint categories, lines: 0 to 174 = Climate change; 175 to 867 = Aquatic ecotoxicity; 868 to 897 = Land occupation; 898 to 916 = Terrestrial acidification and nutrification; 917 to 1595 = Terrestrial ecotoxicity; 1596 to 2232 = Human toxicity; 2233 to 2356 = Ionizing radiation; 2357 to 2450 = Ozone layer depletion; 2451 to 2686 = Photochemical oxidation; 2687 to 2714 = Respiratory effects; 2715 to 2747 = Ressources mineral extraction; 2748 to 2754 = Non-renewable energy. The processes are ordered as, column : 1 = electricity consumed in Ontario; 2 = electricity exported from Ontario to neighbooring region; 3 = electricity produced in Ontario; 4 = Nuclear (ecoinvent3.1); 5 = Coal (ecoinvent3.1); 6 = Gas (ecoinvent3.1); 7 = Hydro (ecoinvent3.1); 8 = Wind (ecoinvent3.1); 9 = Oil (ecoinvent3.1); 10 =Import manitoba (ecoinvent3.1); 11 = Import Minesota (ecoinvent3.1); 12 = Import Michigan (ecoinvent3.1); 13 = Import NewYork (ecoinvent3.1); 14 = Import Québec (ecoinvent3.1); 15 = Transmission1 (ecoinvent3.1); 16 = Transmission2 (ecoinvent3.1).

-electricity_prices_constant: a vector of size 24 which contains an electricity price constant for each hour of the day. The value is calculated by averaging 30 simulation results of the division of the average cost paid by all agents with the Time Of Use tarrif (TOU) as reported by the Ontario Energy Board over the april 2013 april 2014 period and the average default consumption of those agents. It ensures that the total cost paid by all agents are the same wether the TOU tarrif is used or the constant price.

-electricity_prices_summer (and electricity_prices_winter, electricity_prices_winter0): vectors of size 24 which contain electricity prices for each hour of the day (TOU tarrifs) according to the Ontario Energy Board over the april 2013 april 2014 period.

-HDD_2014, (and HDD_2014_TB, HDD_blank): same as CDDs but for heating degree days instead of cooling degree days.

-hh_n&E_app: a 8x23 matrix. Columns : different type of appliances runned with different type of fuel. Lines:

   i) First line = 1st fuel type: 0 = electricity, 1 = gaz, 2 = Oil, 3 = wood, 4 = other (coal and propane).

   ii) Second line = 2nd fuel type: values mean the same as for the first line and 15 = no second fuel type.

   iii) Third line = type of appliances: 0 = stove&oven, 1 = fridge, 3 = freezer, 4 = dishwasher, 5 = clothe washer, 6 = dryer, 7 = other load, 8 = light, 9 = space heating, 10 = water heating, 11 = space cooling.

   iv) Fourth line = Ontarian's households ownership distribution of appliances (e.g.0.5 for an appliance means that half of Ontarian households own that appliance).

   v) Fifth line = loads generated by appliances demands.

   vi) Sixth line = appliances independence: 4th means the appliance has a dependant, 29th means the appliance is dependent to another one (e.g. households do not own a dryer if they don't own a clothe washer and for any given day they do not use the dryer if they did not used the clothe washer earlier that day).

   vii) Seventh line = boolean value that indicate if an appliance has a uniform probability distribution for its daily load (e.g. fridge as an approximation), while data_prob also contain a uniform distribution for the concerned appliance, this boolean indicate to the load frequency determination algorithm that, if possible, the appliance is not used twice the same hour (reinforce the uniformity of load distribution).

   viii) Eighth line = appliances load scheduling constraints, 1 = appliance cannot start earlier than 1 hour before the initial (or default) time it was suppose to start, and no more than 2 hours later; 2 = appliance cannot start earlier than 0 hour before the initial (or default) time it was suppose to start, and no more than 2 hours later; 3 = appliance cannot start earlier than 0 hour before the initial (or default) time it was suppose to start, and anytime later than that (but not the next day). Those constraints are rather strong whcich can explain weak reduction obtained with the load scheduling strategy in the model, there are however coming from various empirical or theoretical studies (Paatero and Lund (2006), Paetz et al. (2012), Ehrhardt-Martinez et al. (2010), Darby (2006)). For lines 6, 7, 8 after the 11th columns values are just 25, equivalent to "NaN".

-hh_n&E_app_all_elec: a 8x23 matrix. Same as hh_n&E_app but the fourth line does not contain any values for appliances using other "fuel" than electricity.

-impact_consum_all: a 8x151 matrix containing impact factors (impact_metric / $) for 151 commodities of the Canadian input output table (the ones included in the final demand vector). Impact factors are calculated from the 321 elementary flows included in the environmental satellite of Open IO-Canada for 8 Impact2002+ midpoint categories (there are no elementary flows reported for 4 midpoint categories).

-initial_values: 11x24 matrix containing all zeros to set up a matrix.

-iv_rebound: vector of size 151 containing zeros to set up a vector.

-mix_ontario: 8760x16 matrix containing contributions of the 16 processes composing the Ontarian mix for each hour of the april 2013 - april 2014 period. The processes are ordered as, column : 1 = electricity consumed in Ontario; 2 = electricity exported from Ontario to neighbooring region; 3 = electricity produced in Ontario; 4 = Nuclear (ecoinvent3.1); 5 = Coal (ecoinvent3.1); 6 = Gas (ecoinvent3.1); 7 = Hydro (ecoinvent3.1); 8 = Wind (ecoinvent3.1); 9 = Oil (ecoinvent3.1); 10 =Import manitoba (ecoinvent3.1); 11 = Import Minesota (ecoinvent3.1); 12 = Import Michigan (ecoinvent3.1); 13 = Import NewYork (ecoinvent3.1); 14 = Import Québec (ecoinvent3.1); 15 = Transmission1 (ecoinvent3.1); 16 = Transmission2 (ecoinvent3.1). Constant and marginal mixes are also defined in "mix_ontario_constant" and "mix_ontario_marginal"

-P_bhvr_summer: a vector of size 12 containing probabilities that an appliance is concerned with a change in behavior (based on Ehrhardt-Martinez et al. (2010)) in summer (i.e. space heating probability is 0). It is used in the roulette wheel process.

-P_bhvr_winter: same as above but for the winter period (i.e. space cooling probability is zero).

-price_inter: a vector of size 24 containing all ones used to compute a constant electricity price for weekends.

-PV_output_Toronto: a vector of size 365 containing daily production of the PV system described above.

-PV_output_ThunderBay: a vector of size 365 containing daily production of the PV system described above.

-single_score_impact2002: a 4x4 matrix containing in the first line impact2002+ normalization factors (1st column to fourth units are: point/DALY, point/pdf*m2*yr, point/kgCO2eq and point/MJ) and the three other lines: Recipe single score ponderation perspectives, (https://www.pre-sustainability.com/recipe) (left to right: human health, ecosystem quality, climate change + ressource and NaN (a zero) (convention is to regroup climate change and resource categories of impact2002+ to use Recipe's perspectives).

-tech_matrix: a 16x16 matrix used to build the processes' agents network used in the LCA. Values come from the first line of the mix_ontario file. The purpose of this matrix is simply to build the Network (or process tree), the values are then taken from mix_ontario for each time step (hour) of a given simulation.

-tech_matrix_constant_mix: same as above but contain values for a yearly averaged electricity mix  (instead of an hourly defined one). Under the assumption of a yearly rather than hourly defined mix one must change the values in the mix_ontario file.

-we_data_prob: Same as Data_prob above but for weekend days, values are taken from Paatero et al. (2006).

-xf_endpoint_impact2002: a vector of size 12 containing characterization factors to convert midpoint results to endpoint ones. Unit of factors from column 1 to column 12: DALY / kg C2H3Cleq, DALY / kg PM2.5eq, DALY / BqC-14eq, DALY / kgCFC-11eq, DALY / kgC2H4eq, PDF*m2*yr / kg TEGwater, PDF*m2*yr / kg TEGsoil, PDF*m2*yr / kg SO2eq, PDF*m2*yr / m2org.arable, kgCO2eq / kgCO2eq, MJprimary / MJprimary, MJsurplus / MJprimary

-xf_impact2002: a vector of size 2755 containing midpoint characterization factors for the 2755 impact 2002+ elementary flows lined up in the same order as for the ef_mix file. (Lines: 0 to 174 = Climate change; 175 to 867 = Aquatic ecotoxicity; 868 to 897 = Land occupation; 898 to 916 = Terrestrial acidification and nutrification; 917 to 1595 = Terrestrial ecotoxicity; 1596 to 2232 = Human toxicity; 2233 to 2356 = Ionizing radiation; 2357 to 2450 = Ozone layer depletion; 2451 to 2686 = Photochemical oxidation; 2687 to 2714 = Respiratory effects; 2715 to 2747 = Ressources mineral extraction; 2748 to 2754 = Non-renewable energy).

-xf_impact2002_RE2ndAssumption: Same as above but containing only elementary flows listed in the open IO-Canada EEIO tables.

### Interface explanation

This part details the interfacce tab of the NetLogo model:

#### Buttons

-go: launch the go procedure forever (untill go is pressed again). The go procedure stops after 8760 ticks (i.e. one year of simulation from April 2013 to April 2014).

-setup: launch the setup procedure, which build the network and prepare the model for the simulation.

-n_bhvr_epicures: set up the maximum number of pro-environmental behaviors agents "epicures" can adopt. Agents "passives" and "frugals" can adopt one more pro-environmental behavior than epicures, agents "stalwarts" can adopt two more. The slider goes from 0 to 3.

-P_conform: set up the probability that an agent conform to its q (q=4) neighbors in the q-voters model used in the ABM.

-P_sbhvr_stalwarts: set up the probability that an agent adopt a pro-environmental behaviors according to the difficulty model used in the ABM.

-SmallWorldNetwork?: boolean used in the model to ensure that the built network has small-world characteristics.

-K_rgraph: slider which controls the number of edges per node * 2 in the networks.

-random_seed?: boolean which allows modeler to plant a random seed in order to ensure repeatability (Warning: it would ensure that all random numbers are the same between two simulations, however because in some configurations the number of random numbers generated are different, it might not gives results that are comparable).

-impact_goal: setup the category for which load scheduling procedure is done (i.e. peak shaving is not done on the electricity consumption curve but rather on the impact curve). Values on the slider indicate which impact category is chosen for the load scheduling:

0 --> ht --> human toxicity
1 --> re --> respiratory effect
2 --> ir --> ionizing radiation
3 --> old --> ozone layer depletion
4 --> po --> photochemical oxidation
5 --> ae --> aquatic ecotoxicity
6 --> te --> terrestrial ecotoxicity
7 --> tan --> terrestrial acidification, nutrification
8 --> lo --> land occupation
9 --> cc --> climate change
10 --> nre --> non-renewable energy
11 --> rme --> raw material extraction
12 --> hh --> human health
13 --> eq --> ecosystem quality
14 --> res --> ressource
15 --> ss --> single score

-proportion_agent_type: allows to change the distribution of subtypes of agents in the population according to Valocchi et al. (2007):

proportion_agent_type = 1: corresponds to Nort American situation, 45 passive agents, 27 stalwart agents, 14 epicure agents and 14 frugal agents.
proportion_agent_type = 2: corresponds to Japanese situation, 40 passive agents, 54 stalwart agents, 3 epicure agents and 3 frugal agents.
proportion_agent_type = 1: corresponds to United Kingdom situation, 56 passive agents, 22 stalwart agents, 11 epicure agents and 11 frugal agents.

-goal: setup the metric that is used for load scheduling: 1 = kWh, 2 = impact (which impact is setup with impact_goal), 3 = $, 4 = impact but accounting for RE (which impact is setup with re_impact_goal). For goal 1 & 4 it is the temporal variability that is minimized while for goal 1 and 3 it is the amount of metric itself.

-price_scenario: allows to setup three different electricity price schemes: 1 = historical TOU prices as set up by the Ontario Energy board (Ontario Energy Board. (2013). Regulated Price Plan Price Report May 1, 2013 to April 30, 2014 Retrieved from http://www.ontarioenergyboard.ca/oeb/_Documents/EB-2004-0205/RPP_Price_Report_May2013_20130405.pdf and Ontario Energy Board. (2013). Regulated Price Plan Price Report November 1, 2013 to October 31, 2014. Retrieved from http://www.ontarioenergyboard.ca/oeb/_Documents/EB-2004-0205/RPP_Price_Report_Nov2013_20131017.pdf), 2 = constant price as for a total cost similar to TOU prices, 3 = variable prices following electricity demand as for a total cost similar to TOU prices.

-Rebound scenario: 1 = Consumption as usual (savings spent in all commodities except electricity with % according to statistic), 2 = Air Travel only, 3 = financial commodities only.

-up_north?: boolean which setup the HDDs and CDDs corresponding to: off = Toronto, on = Thunder Bay.

-h&c_opti?: boolean which setup the load scheduling procedure of water heating and space heating appliances: off = no sheduling of space and water heating loads, on = scheduling of space and water heating loads.

-turtle_multiplier: setup the number of agents. 1 = 100, 2 = 200, ..., 10 = 1000.

-all_electric?: boolean which setup the type of energy used in the homes: off = gaz, coal, electricity etc. can be used by the different appliances according to statistical data on energy use (Natural Resources Canada. (2016). Comprehensive Energy Use Database.   Retrieved from http://oee.nrcan.gc.ca/corporate/statistics/neud/dpa/showTable.cfm?type=CP&sector=res&juris=on&rn=31&page=4
), on = only electricity is used by the homes.

-bhvr_set: setup different scenarios of adopted behaviors. 1 = load scheduling and energy conservation behaviors as retrieved from empirical studies, 2 = choice left to the modeller and it is 1 (energy conservation) by default i.e. no changes in the smart home as to the equivalent standard home, 3 = no load scheduling but load conservation as to empirical studies, 4 = load scheduling but no load conservation as to empirical studies.

-re_impact_goal: setup the category including RE for which load scheduling procedure is done (i.e. peak shaving is not done on the electricity consumption curve but rather on the impact including RE curve). Values on the slider indicate which impact category is chosen for the load scheduling:

0 --> ht --> human toxicity
1 --> re --> respiratory effect
2 --> old --> ozone layer depletion
3 --> po --> photochemical oxidation
4 --> ae --> aquatic ecotoxicity
5 --> te --> terrestrial ecotoxicity
6 --> tan --> terrestrial acidification, nutrification
7 --> cc --> climate change
8 --> hh --> human health
9 --> eq --> ecosystem quality
10 --> ss --> single score

-vision: setup the perspective adopted to convert impact 2002+ endpoint scores into a single score.

1 = Egalitarian :  HH=0.3 EQ=0.5 R+CC=0.2
2 = Hierarchist : HH=0.4 EQ=0.4  R+CC=0.2
3 = Individualist: HH=0.55 EQ=0.25 R+CC=0.2

-individual_vision: boolean (with values 0 and 1 instead of false and true) which setup if agents' vision is the same for each agent or decided by the agent according to the highest "impacting" category for the agent once normalized in points.

-rewiring_prob: slider which controls the rewiring probability in the Watts-Strogatz algorithm when building the network.

-gamma_distrib?: boolean used in the model to ensure that the built network has gamma distribution characteristics.

-RE2ndAssumption?: boolean which allows to limit the 2755 ecoinvent elementary flows to the 321 that are in open IO-Canada, (on = 321 elementary flows for both direct and indirect impact measurement, off= 2755 elementary flows to measure direct impact but only 321 for indirect impact measurement).

-agent_decision_process?: boolean that activate or deactivate the agents' decision rule (q-voter model and Kaiser difficulty theory). It allows to better explore the ABM's behavior space: in combinaison with bhr_set the modeler can study the load scheduling or the energy conservation parts of the model alone and without assuming any social-psychological influence on smart homes energy efficiency (in other words it allows to study "best case" scenarios).

-feedback_frequency: allows to modulate energy feedback recurrence assumption: feedback_frequency = 1 --> hourly feedback (1 tick), feedback_frequency = 2 --> daily feedback (24 ticks), feedback_frequency = 3 --> weekly feedback (168 ticks), feedback_frequency = 4 --> monthly feedback (720 ticks).

-mix_scenario: slider with three values (1, 2 or 3) for different mix_scenarios: 1 = hourly dessagreggated mix, 2 = yearly averaged mix, 3 = marginal hourly dessagreggated mix (based on Dandres et al. 2017). Other scenarios could be added (e.g. monthly or 5-min desaggregated mixes).

-Show Links: button that allows to see link agents connecting households agents.

-test: button that allows to try a procedure in the model (written witin the test procedure in the code tab).

-gamma_lambda: setup the lambda parameter of the algorith using a gamma distribution to build households agents network.

-gamma_alpha: setup the alpha parameter of the algorith using a gamma distribution to build households agents network.

-constant_weather?: boolean that allows to take into account weather influence on electricity consumption profiles. on = HDD and CDD are used to weight space heating and space cooling load in the model, off = space heating and cooling loads are always constant no matter what day of the year it is (e.g. not matter if it is a winter or a summer day).

-PV&Battery_scenario: 1 --> no PV system installed on either home (smart & standard), 2 --> PV system installed on both the smart and the standard home, 3 --> PV system installed on the smart home only

#### Output windows

-World: show households, technology agents, and their links. Agents change color with the number of adopted pro-environmental behaviors. No agents are moving, the space dimension of the world is not exploited.

-Impacts' windows: show climate change, human health, ecosystem quality and resource impacts in real time (x axis is time steps (tiks), y axis is the amount of impacts).

-Electricity consumption and cost windows: show electricity consumption and cost in real time (x axis is time steps (tiks), y axis is the amount of electricity or money).

-Ouput window: allows to communicate with modeller. It is notably used to notify the modeller wether the built households agents network has small-world characteristics or not.

-Behavior adoption window: shows in real time the number of agents owning 1, 2, 3, 4 or 5 pro-environmental behaviors.

-Passive and yellow turtles windows: show the climate change impact profiles of one innovator and one passive agent.

### References

Y. Matsuno, K. I. Takahashi, and M. Tsuda, "Eco-Efficiency for Information and Communications Technology (ICT): The State of Knowledge in Japan," in Electronics & the Environment, Proceedings of the 2007 IEEE International Symposium on, 2007, pp. 1-5.

E. Kitou and A. Horvath, "Transportation Choices and Air Pollution Effects of Telework," Journal of Infrastructure Systems, vol. 12, pp. 121-134, 2006.

I. Reichart, "The Environmental Impact of Getting the News," Journal of Industrial Ecology, vol. 6, pp. 185-200, 2002.

Global e-Sustainability Initiative, "GeSI SMARTer 2020: the role of ICT in driving a sustainable future," Global e-Sustainability Initiative, Brussels, Belgium, 2012.

IPCC, "Climate Change 2014: Synthesis Report. Contribution of Working Groups I, II and III to the Fifth Assessment Report of the Intergovernmental Panel on Climate Change [Core Writing Team, R.K. Pachauri and L.A. Meyer (eds.)]. IPCC, Geneva, Switzerland, ," 2014.

A.-G. Paetz, E. Dütschke, and W. Fichtner, "Smart Homes as a Means to Sustainable Energy Consumption: A Study of Consumer Perceptions," Journal of Consumer Policy, vol. 35, pp. 23-41, 2012.

A. Saad al-sumaiti, M. H. Ahmed, and M. M. A. Salama, "Smart Home Activities: A Literature Review," Electric Power Components and Systems, vol. 42, pp. 294-305, 2014/03/12 2014.

I. Røpke and T. H. Christensen, "Energy impacts of ICT – Insights from an everyday life perspective," Telematics and Informatics, vol. 29, pp. 348-361, 11// 2012.

Y. Arushanyan, E. Ekener-Petersen, and G. Finnveden, "Lessons learned – Review of LCAs for ICT products and services," Computers in Industry, vol. 65, pp. 211-234, 2// 2014.

S. Hellweg and L. M. i Canals, "Emerging approaches, challenges and opportunities in life cycle assessment," Science, vol. 344, pp. 1109-1113, 2014.

ISO, "ISO 14040:2006(F)," ed: ISO, 2006.

ISO, "ISO 14044:2006(F)," ed: ISO, 2006.

B. Girod, P. de Haan, and R. Scholz, "Consumption-as-usual instead of ceteris paribus assumption for demand," The International Journal of Life Cycle Assessment, vol. 16, pp. 3-11, 2011/01/01 2011.

D. F. Vivanco and E. van der Voet, "The rebound effect through industrial ecology's eyes: a review of LCA-based studies," International Journal of Life Cycle Assessment, vol. 19, pp. 1933-1947, Dec 2014.

M. Spielmann, P. de Haan, and R. W. Scholz, "Environmental rebound effects of high-speed transport technologies: a case study of climate change rebound effects of a future underground maglev train system," Journal of Cleaner Production, vol. 16, pp. 1388-1398, 9// 2008.

E. P. di Sorrentino, E. Woelbert, and S. Sala, "Consumers and their behavior: state of the art in behavioral science supporting use phase modeling in LCA and ecodesign," The International Journal of Life Cycle Assessment, vol. 21, pp. 237-251, 2016.

W. Zhang, S. Tan, Y. Lei, and S. Wang, "Life cycle assessment of a single-family residential building in Canada: A case study," Building Simulation, vol. 7, pp. 429-438, August 01 2014.

A. Sharma, A. Saxena, M. Sethi, V. Shree, and Varun, "Life cycle assessment of buildings: A review," Renewable and Sustainable Energy Reviews, vol. 15, pp. 871-875, 2011/01/01/ 2011.

O. Ortiz, C. Bonnet, J. C. Bruno, and F. Castells, "Sustainability based on LCM of residential dwellings: A case study in Catalonia, Spain," Building and Environment, vol. 44, pp. 584-594, 2009/03/01/ 2009.

J. G. Bull and R. A. Kozak, "Comparative life cycle assessments: The case of paper and digital media," Environmental Impact Assessment Review, vol. 45, pp. 10-18, 2// 2014.

M. Ahmadi Achachlouei, Å. Moberg, and E. Hochschorner, "Life Cycle Assessment of a Magazine, Part I: Tablet Edition in Emerging and Mature States," Journal of Industrial Ecology, vol. 19, pp. 575-589, 2015.

P. Baustert and E. Benetto, "Uncertainty analysis in agent-based modelling and consequential life cycle assessment coupled models: A critical review," Journal of Cleaner Production, vol. 156, pp. 378-394, 2017/07/10/ 2017.

A. Hicks and T. Theis, "An agent based approach to the potential for rebound resulting from evolution of residential lighting technologies," The International Journal of Life Cycle Assessment, vol. 19, pp. 370-376, 2014/02/01 2014.

U. Wilensky and W. Rand, An introduction to agent-based modeling: modeling natural, social, and engineered complex systems with NetLogo: MIT Press, 2015.

M. Mitchell, Complexity: A guided tour: Oxford University Press, 2009.

F. A. Qayyum, M. Naeem, A. S. Khwaja, A. Anpalagan, L. Guan, and B. Venkatesh, "Appliance Scheduling Optimization in Smart Home Networks," IEEE Access, vol. 3, pp. 2176-2190, 2015.

R. H. Thaler and C. R. Sunstein, Nudge : improving decisions about health, wealth, and happiness: Yale University Press, 2008.

M. A. Delmas, M. Fischlein, and O. I. Asensio, "Information strategies and energy conservation behavior: A meta-analysis of experimental studies from 1975 to 2012," Energy Policy, vol. 61, pp. 729-739, 10// 2013.

I. Ayres, S. Raseman, and A. Shih, "Evidence from two large field experiments that peer comparison feedback can reduce residential energy usage," The Journal of Law, Economics, and Organization, vol. 29, pp. 992-1022, 2013.

H. Allcott, "Social norms and energy conservation," Journal of Public Economics, vol. 95, pp. 1082-1095, 2011/10/01/ 2011.

C. Davis, I. Nikolić, and G. P. J. Dijkema, "Integration of Life Cycle Assessment Into Agent-Based Modeling," Journal of Industrial Ecology, vol. 13, pp. 306-325, 2009.

S. A. Miller, S. Moysey, B. Sharp, and J. Alfaro, "A Stochastic Approach to Model Dynamic Systems in Life Cycle Assessment," Journal of Industrial Ecology, vol. 17, pp. 352-362, 2013.

T. Dandres, R. Farrahi Moghaddam, K. K. Nguyen, Y. Lemieux, R. Samson, and M. Cheriet, "Consideration of marginal electricity in real-time minimization of distributed data centre emissions," Journal of Cleaner Production, vol. 143, pp. 116-124, 2017/02/01/ 2017.

E. Maurice, T. Dandres, R. Farrahi Moghaddam, K. Nguyen, Y. Lemieux, M. Cherriet, et al., "Modelling of Electricity Mix in Temporal Differentiated Life-Cycle-Assessment to Minimize Carbon Footprint of a Cloud Computing Service," in ICT for Sustainability 2014 (ICT4S-14), 2014.

S. Hargreaves-Heap and Y. Varoufakis, Game theory: a critical introduction: Routledge, 1995.

F. Querini and E. Benetto, "Agent-based modelling for assessing hybrid and electric cars deployment policies in Luxembourg and Lorraine," Transportation Research Part A: Policy and Practice, vol. 70, pp. 149-161, 12// 2014.

N. Bichraoui-Draper, M. Xu, S. A. Miller, and B. Guillaume, "Agent-based life cycle assessment for switchgrass-based bioenergy systems," Resources, Conservation and Recycling, vol. 103, pp. 171-178, 10// 2015.

Q. Florent and B. Enrico, "Combining Agent-Based Modeling and Life Cycle Assessment for the Evaluation of Mobility Policies," Environmental Science & Technology, vol. 49, pp. 1744-1751, 2015/02/03 2015.

J. R. Snape, "Incorporating human behaviour in an agent based model of technology adoption in the transition to a smart grid," PhD, Institute of Energy and Sustainable Development, De Montfort University, 2015.

J. V. Paatero and P. D. Lund, "A model for generating household electricity load profiles," International Journal of Energy Research, vol. 30, pp. 273-290, 2006.

Natural Resources Canada. (2016, 08/12/2016). Comprehensive Energy Use Database. Available: http://oee.nrcan.gc.ca/corporate/statistics/neud/dpa/showTable.cfm?type=CP&sector=res&juris=on&rn=31&page=4

Government of Canada. (2016, 05/08/2016). Historical Data. Available: http://climate.weather.gc.ca/historical_data/search_historic_data_e.html

F. G. Kaiser, K. Byrka, and T. Hartig, "Reviving Campbell’s Paradigm for Attitude Research," Personality and Social Psychology Review, vol. 14, pp. 351-367, November 1, 2010 2010.

K. Byrka, A. Jȩdrzejewski, K. Sznajd-Weron, and R. Weron, "Difficulty is critical: The importance of social factors in modeling diffusion of green products and practices," Renewable and Sustainable Energy Reviews, vol. 62, pp. 723-735, 9// 2016.

M. Valocchi, A. Schurr, J. Juliano, and E. Nelson, "Plugging in the consumer," IBM Institute for Business Value, 2007.

D. J. Watts and S. H. Strogatz, "Collective dynamics of ‘small-world’networks," nature, vol. 393, pp. 440-442, 1998.

K. Ehrhardt-Martinez, K. A. Donnelly, and S. Laitner, "Advanced metering initiatives and residential feedback programs: a meta-review for household electricity-saving opportunities," 2010.

O. I. Asensio and M. A. Delmas, "Nonprice incentives and energy conservation," Proceedings of the National Academy of Sciences, vol. 112, pp. E510-E515, February 10, 2015 2015.

T. Ueno, R. Inada, O. Saeki, and K. Tsuji, "Effectiveness of an energy-consumption information system for residential buildings," Applied Energy, vol. 83, pp. 868-883, 8// 2006.

G. Wood and M. Newborough, "Dynamic energy-consumption indicators for domestic appliances: environment, behaviour and design," Energy and Buildings, vol. 35, pp. 821-841, 9// 2003.

Hydro One Networks. (15/07/2016). Hydro One Networks Inc. Time-of-use pricing pilot project results. Toronto, Ontario, May, 2008. Available: http://www.ontarioenergyboard.ca/documents/cases/EB-2004-0205/smartpricepilot/TOU_Pilot_Report_HydroOne_20080513.pdf

B. Ceniceros and W. Bos, "Insights and Questions: Customer Response to the Home Electricity Reports, Results from SMUD's," 2009.

C. Aguilar, D. White, and D. L. Ryan, "Domestic water heating and water heater energy consumption in Canada," Canadian Building Energy End-Use Data and Analysis Centre, 2005.

W. Abrahamse, L. Steg, C. Vlek, and T. Rothengatter, "A review of intervention studies aimed at household energy conservation," Journal of Environmental Psychology, vol. 25, pp. 273-291, 9// 2005.

S. Darby, "The effectiveness of feedback on energy consumption," A Review for DEFRA of the Literature on Metering, Billing and direct Displays, vol. 486, p. 2006, 2006.

Independant Electricity System operator. (2015, 05/10/2015). Ontario Demand. Available: http://www.ieso.ca/Pages/Power-Data/default.aspx#

P. Lesage, G. Kijko, C. Vallée-Schmitter, D. Maxime, and (CIRAIG). (2012, 10/02/2016). Open IO-Canada online tool. Available: http://www.ciraig.org/en/open_io_canada/index.html

B. M. Sopha, C. A. Klӧckner, and D. Febrianti, "Using agent-based modeling to explore policy options supporting adoption of natural gas vehicles in Indonesia," Journal of Environmental Psychology, vol. 52, pp. 149-165, 2017/10/01/ 2017.

IESO. (2017, 23/07/2017). Hourly Ontario and Market Demands, 2002-2016. Available: http://www.ieso.ca/en/power-data/data-directory

Dandres T, Farrahi Moghaddam R, Nguyen KK, Lemieux Y, Samson R, Cheriet M. 2017. "Consideration of marginal electricity in real-time minimization of distributed data centre emissions." Journal of Cleaner Production. 143:116-124.

### Further considerations

With no doubt the code could be lightened and improved (e.g. replace the four household breeds by one and differentiate household type with a built-in variable). It could be made more efficient and sophisticated. Nethertheless we hope readers will find interesting scenarios to explore and certain developed procedures usefull. Finally, feel free to write us with any questions or comments to:

julien.walzberg@polymtl.ca

## COPYRIGHT AND LICENSE

![Creative Commons Attribution – Share Alike (CC BY-SA) license](file:Images\CreativeCommonLicence.png)

The Smart Homes Agent Based Model was built to allow the use of the model under a Creative Commons Attribution – Share Alike (CC BY-SA) license. You are therefore free to share (copy and redistribute the material in any medium or format) or adapt (remix, transform, and build upon the material) for any purpose, even commercially. However, you must give appropriate credit (Julien Walzberg, Thomas Dandres, Nicolas Merveille, Mohamed Cheriet, Réjean Samson, "An Agent-Based Model To Evaluate Smart Homes Sustainability Potential" IEEE pimrc'17 SP-02 Software-defined edge computing in smart cities, 08-13 October 2017 - Montreal, QC, Canada), provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use. Also, if you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270

@#$#@#$#@
NetLogo 5.2.1
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="TO_USE_from_nowOn" repetitions="1" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>electric_consumption</metric>
    <metric>electric_consumption2</metric>
    <metric>costbis</metric>
    <metric>costbis2</metric>
    <metric>ht_default_impactbis</metric>
    <metric>ht_impactbis</metric>
    <metric>ht_default_impact_building</metric>
    <metric>re_default_impactbis</metric>
    <metric>re_impactbis</metric>
    <metric>re_default_impact_building</metric>
    <metric>ir_default_impactbis</metric>
    <metric>ir_impactbis</metric>
    <metric>ir_default_impact_building</metric>
    <metric>old_default_impactbis</metric>
    <metric>old_impactbis</metric>
    <metric>old_default_impact_building</metric>
    <metric>po_default_impactbis</metric>
    <metric>po_impactbis</metric>
    <metric>po_default_impact_building</metric>
    <metric>ae_default_impactbis</metric>
    <metric>ae_impactbis</metric>
    <metric>ae_default_impact_building</metric>
    <metric>te_default_impactbis</metric>
    <metric>te_impactbis</metric>
    <metric>te_default_impact_building</metric>
    <metric>tan_default_impactbis</metric>
    <metric>tan_impactbis</metric>
    <metric>tan_default_impact_building</metric>
    <metric>lo_default_impactbis</metric>
    <metric>lo_impactbis</metric>
    <metric>lo_default_impact_building</metric>
    <metric>cc_default_impactbis</metric>
    <metric>cc_impactbis</metric>
    <metric>cc_default_impact_building</metric>
    <metric>nre_default_impactbis</metric>
    <metric>nre_impactbis</metric>
    <metric>nre_default_impact_building</metric>
    <metric>rme_default_impactbis</metric>
    <metric>rme_impactbis</metric>
    <metric>rme_default_impact_building</metric>
    <metric>hh_default_impactbis</metric>
    <metric>hh_impactbis</metric>
    <metric>hh_default_impact_building</metric>
    <metric>eq_default_impactbis</metric>
    <metric>eq_impactbis</metric>
    <metric>eq_default_impact_building</metric>
    <metric>res_default_impactbis</metric>
    <metric>res_impactbis</metric>
    <metric>res_default_impact_building</metric>
    <metric>ss_default_impactbis</metric>
    <metric>ss_impactbis</metric>
    <metric>ss_default_impact_building</metric>
    <metric>ht_impact_tot</metric>
    <metric>re_impact_tot</metric>
    <metric>old_impact_tot</metric>
    <metric>po_impact_tot</metric>
    <metric>ae_impact_tot</metric>
    <metric>te_impact_tot</metric>
    <metric>tan_impact_tot</metric>
    <metric>cc_impact_tot</metric>
    <metric>hh_impact_tot</metric>
    <metric>eq_impact_tot</metric>
    <metric>ss_impact_tot</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>matrix:get-row global_econst 0</metric>
    <metric>matrix:get-row global_econst 1</metric>
    <metric>matrix:get-row global_econst 2</metric>
    <metric>matrix:get-row global_econst 3</metric>
    <metric>matrix:get-row global_econst 4</metric>
    <metric>matrix:get-row global_econst 5</metric>
    <metric>matrix:get-row global_econst 6</metric>
    <metric>matrix:get-row global_econst 7</metric>
    <metric>matrix:get-row global_econst 8</metric>
    <metric>matrix:get-row global_econst 9</metric>
    <metric>matrix:get-row global_econst 10</metric>
    <metric>matrix:get-row global_default_econst 0</metric>
    <metric>matrix:get-row global_default_econst 1</metric>
    <metric>matrix:get-row global_default_econst 2</metric>
    <metric>matrix:get-row global_default_econst 3</metric>
    <metric>matrix:get-row global_default_econst 4</metric>
    <metric>matrix:get-row global_default_econst 5</metric>
    <metric>matrix:get-row global_default_econst 6</metric>
    <metric>matrix:get-row global_default_econst 7</metric>
    <metric>matrix:get-row global_default_econst 8</metric>
    <metric>matrix:get-row global_default_econst 9</metric>
    <metric>matrix:get-row global_default_econst 10</metric>
    <metric>matrix:get-row passive0_econst 0</metric>
    <metric>matrix:get-row passive0_econst 1</metric>
    <metric>matrix:get-row passive0_econst 2</metric>
    <metric>matrix:get-row passive0_econst 3</metric>
    <metric>matrix:get-row passive0_econst 4</metric>
    <metric>matrix:get-row passive0_econst 5</metric>
    <metric>matrix:get-row passive0_econst 6</metric>
    <metric>matrix:get-row passive0_econst 7</metric>
    <metric>matrix:get-row passive0_econst 8</metric>
    <metric>matrix:get-row passive0_econst 9</metric>
    <metric>matrix:get-row passive0_econst 10</metric>
    <metric>matrix:get-row passive0_defaulteconst 0</metric>
    <metric>matrix:get-row passive0_defaulteconst 1</metric>
    <metric>matrix:get-row passive0_defaulteconst 2</metric>
    <metric>matrix:get-row passive0_defaulteconst 3</metric>
    <metric>matrix:get-row passive0_defaulteconst 4</metric>
    <metric>matrix:get-row passive0_defaulteconst 5</metric>
    <metric>matrix:get-row passive0_defaulteconst 6</metric>
    <metric>matrix:get-row passive0_defaulteconst 7</metric>
    <metric>matrix:get-row passive0_defaulteconst 8</metric>
    <metric>matrix:get-row passive0_defaulteconst 9</metric>
    <metric>matrix:get-row passive0_defaulteconst 10</metric>
    <metric>matrix:get-row yellowguy_econst 0</metric>
    <metric>matrix:get-row yellowguy_econst 1</metric>
    <metric>matrix:get-row yellowguy_econst 2</metric>
    <metric>matrix:get-row yellowguy_econst 3</metric>
    <metric>matrix:get-row yellowguy_econst 4</metric>
    <metric>matrix:get-row yellowguy_econst 5</metric>
    <metric>matrix:get-row yellowguy_econst 6</metric>
    <metric>matrix:get-row yellowguy_econst 7</metric>
    <metric>matrix:get-row yellowguy_econst 8</metric>
    <metric>matrix:get-row yellowguy_econst 9</metric>
    <metric>matrix:get-row yellowguy_econst 10</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 0</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 1</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 2</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 3</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 4</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 5</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 6</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 7</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 8</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 9</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 10</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact</metric>
    <metric>SB_impact_RE</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="TO_USE_measure_at_every_step" repetitions="1" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>electric_consumption</metric>
    <metric>electric_consumption2</metric>
    <metric>costbis</metric>
    <metric>costbis2</metric>
    <metric>ht_default_impactbis</metric>
    <metric>ht_impactbis</metric>
    <metric>ht_default_impact_building</metric>
    <metric>re_default_impactbis</metric>
    <metric>re_impactbis</metric>
    <metric>re_default_impact_building</metric>
    <metric>ir_default_impactbis</metric>
    <metric>ir_impactbis</metric>
    <metric>ir_default_impact_building</metric>
    <metric>old_default_impactbis</metric>
    <metric>old_impactbis</metric>
    <metric>old_default_impact_building</metric>
    <metric>po_default_impactbis</metric>
    <metric>po_impactbis</metric>
    <metric>po_default_impact_building</metric>
    <metric>ae_default_impactbis</metric>
    <metric>ae_impactbis</metric>
    <metric>ae_default_impact_building</metric>
    <metric>te_default_impactbis</metric>
    <metric>te_impactbis</metric>
    <metric>te_default_impact_building</metric>
    <metric>tan_default_impactbis</metric>
    <metric>tan_impactbis</metric>
    <metric>tan_default_impact_building</metric>
    <metric>lo_default_impactbis</metric>
    <metric>lo_impactbis</metric>
    <metric>lo_default_impact_building</metric>
    <metric>cc_default_impactbis</metric>
    <metric>cc_impactbis</metric>
    <metric>cc_default_impact_building</metric>
    <metric>nre_default_impactbis</metric>
    <metric>nre_impactbis</metric>
    <metric>nre_default_impact_building</metric>
    <metric>rme_default_impactbis</metric>
    <metric>rme_impactbis</metric>
    <metric>rme_default_impact_building</metric>
    <metric>hh_default_impactbis</metric>
    <metric>hh_impactbis</metric>
    <metric>hh_default_impact_building</metric>
    <metric>eq_default_impactbis</metric>
    <metric>eq_impactbis</metric>
    <metric>eq_default_impact_building</metric>
    <metric>res_default_impactbis</metric>
    <metric>res_impactbis</metric>
    <metric>res_default_impact_building</metric>
    <metric>ss_default_impactbis</metric>
    <metric>ss_impactbis</metric>
    <metric>ss_default_impact_building</metric>
    <metric>ht_impact_tot</metric>
    <metric>re_impact_tot</metric>
    <metric>old_impact_tot</metric>
    <metric>po_impact_tot</metric>
    <metric>ae_impact_tot</metric>
    <metric>te_impact_tot</metric>
    <metric>tan_impact_tot</metric>
    <metric>cc_impact_tot</metric>
    <metric>hh_impact_tot</metric>
    <metric>eq_impact_tot</metric>
    <metric>ss_impact_tot</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>matrix:get-row global_econst 0</metric>
    <metric>matrix:get-row global_econst 1</metric>
    <metric>matrix:get-row global_econst 2</metric>
    <metric>matrix:get-row global_econst 3</metric>
    <metric>matrix:get-row global_econst 4</metric>
    <metric>matrix:get-row global_econst 5</metric>
    <metric>matrix:get-row global_econst 6</metric>
    <metric>matrix:get-row global_econst 7</metric>
    <metric>matrix:get-row global_econst 8</metric>
    <metric>matrix:get-row global_econst 9</metric>
    <metric>matrix:get-row global_econst 10</metric>
    <metric>matrix:get-row global_default_econst 0</metric>
    <metric>matrix:get-row global_default_econst 1</metric>
    <metric>matrix:get-row global_default_econst 2</metric>
    <metric>matrix:get-row global_default_econst 3</metric>
    <metric>matrix:get-row global_default_econst 4</metric>
    <metric>matrix:get-row global_default_econst 5</metric>
    <metric>matrix:get-row global_default_econst 6</metric>
    <metric>matrix:get-row global_default_econst 7</metric>
    <metric>matrix:get-row global_default_econst 8</metric>
    <metric>matrix:get-row global_default_econst 9</metric>
    <metric>matrix:get-row global_default_econst 10</metric>
    <metric>matrix:get-row passive0_econst 0</metric>
    <metric>matrix:get-row passive0_econst 1</metric>
    <metric>matrix:get-row passive0_econst 2</metric>
    <metric>matrix:get-row passive0_econst 3</metric>
    <metric>matrix:get-row passive0_econst 4</metric>
    <metric>matrix:get-row passive0_econst 5</metric>
    <metric>matrix:get-row passive0_econst 6</metric>
    <metric>matrix:get-row passive0_econst 7</metric>
    <metric>matrix:get-row passive0_econst 8</metric>
    <metric>matrix:get-row passive0_econst 9</metric>
    <metric>matrix:get-row passive0_econst 10</metric>
    <metric>matrix:get-row passive0_defaulteconst 0</metric>
    <metric>matrix:get-row passive0_defaulteconst 1</metric>
    <metric>matrix:get-row passive0_defaulteconst 2</metric>
    <metric>matrix:get-row passive0_defaulteconst 3</metric>
    <metric>matrix:get-row passive0_defaulteconst 4</metric>
    <metric>matrix:get-row passive0_defaulteconst 5</metric>
    <metric>matrix:get-row passive0_defaulteconst 6</metric>
    <metric>matrix:get-row passive0_defaulteconst 7</metric>
    <metric>matrix:get-row passive0_defaulteconst 8</metric>
    <metric>matrix:get-row passive0_defaulteconst 9</metric>
    <metric>matrix:get-row passive0_defaulteconst 10</metric>
    <metric>matrix:get-row yellowguy_econst 0</metric>
    <metric>matrix:get-row yellowguy_econst 1</metric>
    <metric>matrix:get-row yellowguy_econst 2</metric>
    <metric>matrix:get-row yellowguy_econst 3</metric>
    <metric>matrix:get-row yellowguy_econst 4</metric>
    <metric>matrix:get-row yellowguy_econst 5</metric>
    <metric>matrix:get-row yellowguy_econst 6</metric>
    <metric>matrix:get-row yellowguy_econst 7</metric>
    <metric>matrix:get-row yellowguy_econst 8</metric>
    <metric>matrix:get-row yellowguy_econst 9</metric>
    <metric>matrix:get-row yellowguy_econst 10</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 0</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 1</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 2</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 3</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 4</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 5</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 6</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 7</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 8</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 9</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 10</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="feedback_validation_finalstep" repetitions="5" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.5"/>
      <value value="0.6"/>
      <value value="0.7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <steppedValueSet variable="P_conform" first="0" step="0.1" last="1"/>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="feedback_validation_everystep" repetitions="1" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>electric_consumption</metric>
    <metric>electric_consumption2</metric>
    <metric>costbis</metric>
    <metric>costbis2</metric>
    <metric>ht_default_impactbis</metric>
    <metric>ht_impactbis</metric>
    <metric>ht_default_impact_building</metric>
    <metric>re_default_impactbis</metric>
    <metric>re_impactbis</metric>
    <metric>re_default_impact_building</metric>
    <metric>ir_default_impactbis</metric>
    <metric>ir_impactbis</metric>
    <metric>ir_default_impact_building</metric>
    <metric>old_default_impactbis</metric>
    <metric>old_impactbis</metric>
    <metric>old_default_impact_building</metric>
    <metric>po_default_impactbis</metric>
    <metric>po_impactbis</metric>
    <metric>po_default_impact_building</metric>
    <metric>ae_default_impactbis</metric>
    <metric>ae_impactbis</metric>
    <metric>ae_default_impact_building</metric>
    <metric>te_default_impactbis</metric>
    <metric>te_impactbis</metric>
    <metric>te_default_impact_building</metric>
    <metric>tan_default_impactbis</metric>
    <metric>tan_impactbis</metric>
    <metric>tan_default_impact_building</metric>
    <metric>lo_default_impactbis</metric>
    <metric>lo_impactbis</metric>
    <metric>lo_default_impact_building</metric>
    <metric>cc_default_impactbis</metric>
    <metric>cc_impactbis</metric>
    <metric>cc_default_impact_building</metric>
    <metric>nre_default_impactbis</metric>
    <metric>nre_impactbis</metric>
    <metric>nre_default_impact_building</metric>
    <metric>rme_default_impactbis</metric>
    <metric>rme_impactbis</metric>
    <metric>rme_default_impact_building</metric>
    <metric>hh_default_impactbis</metric>
    <metric>hh_impactbis</metric>
    <metric>hh_default_impact_building</metric>
    <metric>eq_default_impactbis</metric>
    <metric>eq_impactbis</metric>
    <metric>eq_default_impact_building</metric>
    <metric>res_default_impactbis</metric>
    <metric>res_impactbis</metric>
    <metric>res_default_impact_building</metric>
    <metric>ss_default_impactbis</metric>
    <metric>ss_impactbis</metric>
    <metric>ss_default_impact_building</metric>
    <metric>ht_impact_tot</metric>
    <metric>re_impact_tot</metric>
    <metric>old_impact_tot</metric>
    <metric>po_impact_tot</metric>
    <metric>ae_impact_tot</metric>
    <metric>te_impact_tot</metric>
    <metric>tan_impact_tot</metric>
    <metric>cc_impact_tot</metric>
    <metric>hh_impact_tot</metric>
    <metric>eq_impact_tot</metric>
    <metric>ss_impact_tot</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>matrix:get-row global_econst 0</metric>
    <metric>matrix:get-row global_econst 1</metric>
    <metric>matrix:get-row global_econst 2</metric>
    <metric>matrix:get-row global_econst 3</metric>
    <metric>matrix:get-row global_econst 4</metric>
    <metric>matrix:get-row global_econst 5</metric>
    <metric>matrix:get-row global_econst 6</metric>
    <metric>matrix:get-row global_econst 7</metric>
    <metric>matrix:get-row global_econst 8</metric>
    <metric>matrix:get-row global_econst 9</metric>
    <metric>matrix:get-row global_econst 10</metric>
    <metric>matrix:get-row global_default_econst 0</metric>
    <metric>matrix:get-row global_default_econst 1</metric>
    <metric>matrix:get-row global_default_econst 2</metric>
    <metric>matrix:get-row global_default_econst 3</metric>
    <metric>matrix:get-row global_default_econst 4</metric>
    <metric>matrix:get-row global_default_econst 5</metric>
    <metric>matrix:get-row global_default_econst 6</metric>
    <metric>matrix:get-row global_default_econst 7</metric>
    <metric>matrix:get-row global_default_econst 8</metric>
    <metric>matrix:get-row global_default_econst 9</metric>
    <metric>matrix:get-row global_default_econst 10</metric>
    <metric>matrix:get-row passive0_econst 0</metric>
    <metric>matrix:get-row passive0_econst 1</metric>
    <metric>matrix:get-row passive0_econst 2</metric>
    <metric>matrix:get-row passive0_econst 3</metric>
    <metric>matrix:get-row passive0_econst 4</metric>
    <metric>matrix:get-row passive0_econst 5</metric>
    <metric>matrix:get-row passive0_econst 6</metric>
    <metric>matrix:get-row passive0_econst 7</metric>
    <metric>matrix:get-row passive0_econst 8</metric>
    <metric>matrix:get-row passive0_econst 9</metric>
    <metric>matrix:get-row passive0_econst 10</metric>
    <metric>matrix:get-row passive0_defaulteconst 0</metric>
    <metric>matrix:get-row passive0_defaulteconst 1</metric>
    <metric>matrix:get-row passive0_defaulteconst 2</metric>
    <metric>matrix:get-row passive0_defaulteconst 3</metric>
    <metric>matrix:get-row passive0_defaulteconst 4</metric>
    <metric>matrix:get-row passive0_defaulteconst 5</metric>
    <metric>matrix:get-row passive0_defaulteconst 6</metric>
    <metric>matrix:get-row passive0_defaulteconst 7</metric>
    <metric>matrix:get-row passive0_defaulteconst 8</metric>
    <metric>matrix:get-row passive0_defaulteconst 9</metric>
    <metric>matrix:get-row passive0_defaulteconst 10</metric>
    <metric>matrix:get-row yellowguy_econst 0</metric>
    <metric>matrix:get-row yellowguy_econst 1</metric>
    <metric>matrix:get-row yellowguy_econst 2</metric>
    <metric>matrix:get-row yellowguy_econst 3</metric>
    <metric>matrix:get-row yellowguy_econst 4</metric>
    <metric>matrix:get-row yellowguy_econst 5</metric>
    <metric>matrix:get-row yellowguy_econst 6</metric>
    <metric>matrix:get-row yellowguy_econst 7</metric>
    <metric>matrix:get-row yellowguy_econst 8</metric>
    <metric>matrix:get-row yellowguy_econst 9</metric>
    <metric>matrix:get-row yellowguy_econst 10</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 0</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 1</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 2</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 3</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 4</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 5</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 6</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 7</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 8</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 9</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 10</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>time_simul</metric>
    <steppedValueSet variable="P_sbhvr_stalwarts" first="0" step="0.1" last="1"/>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.3"/>
      <value value="0.4"/>
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="feedback_validation_finalstep5-7" repetitions="10" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <steppedValueSet variable="P_conform" first="0" step="0.1" last="1"/>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="9-1feedback_validation_redo" repetitions="10" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.9"/>
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <steppedValueSet variable="P_conform" first="0" step="0.1" last="1"/>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="TO_USE_measure_at_the_end" repetitions="1" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="2-3feedback_validation_redo" repetitions="30" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <steppedValueSet variable="P_conform" first="0.2" step="0.025" last="0.3"/>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="NetworkType" repetitions="30" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0.1"/>
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="NetworkType2" repetitions="30" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="ArticleAllSteps" repetitions="1" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>electric_consumption</metric>
    <metric>electric_consumption2</metric>
    <metric>costbis</metric>
    <metric>costbis2</metric>
    <metric>ht_default_impactbis</metric>
    <metric>ht_impactbis</metric>
    <metric>ht_default_impact_building</metric>
    <metric>re_default_impactbis</metric>
    <metric>re_impactbis</metric>
    <metric>re_default_impact_building</metric>
    <metric>ir_default_impactbis</metric>
    <metric>ir_impactbis</metric>
    <metric>ir_default_impact_building</metric>
    <metric>old_default_impactbis</metric>
    <metric>old_impactbis</metric>
    <metric>old_default_impact_building</metric>
    <metric>po_default_impactbis</metric>
    <metric>po_impactbis</metric>
    <metric>po_default_impact_building</metric>
    <metric>ae_default_impactbis</metric>
    <metric>ae_impactbis</metric>
    <metric>ae_default_impact_building</metric>
    <metric>te_default_impactbis</metric>
    <metric>te_impactbis</metric>
    <metric>te_default_impact_building</metric>
    <metric>tan_default_impactbis</metric>
    <metric>tan_impactbis</metric>
    <metric>tan_default_impact_building</metric>
    <metric>lo_default_impactbis</metric>
    <metric>lo_impactbis</metric>
    <metric>lo_default_impact_building</metric>
    <metric>cc_default_impactbis</metric>
    <metric>cc_impactbis</metric>
    <metric>cc_default_impact_building</metric>
    <metric>nre_default_impactbis</metric>
    <metric>nre_impactbis</metric>
    <metric>nre_default_impact_building</metric>
    <metric>rme_default_impactbis</metric>
    <metric>rme_impactbis</metric>
    <metric>rme_default_impact_building</metric>
    <metric>hh_default_impactbis</metric>
    <metric>hh_impactbis</metric>
    <metric>hh_default_impact_building</metric>
    <metric>eq_default_impactbis</metric>
    <metric>eq_impactbis</metric>
    <metric>eq_default_impact_building</metric>
    <metric>res_default_impactbis</metric>
    <metric>res_impactbis</metric>
    <metric>res_default_impact_building</metric>
    <metric>ss_default_impactbis</metric>
    <metric>ss_impactbis</metric>
    <metric>ss_default_impact_building</metric>
    <metric>ht_impact_tot</metric>
    <metric>re_impact_tot</metric>
    <metric>old_impact_tot</metric>
    <metric>po_impact_tot</metric>
    <metric>ae_impact_tot</metric>
    <metric>te_impact_tot</metric>
    <metric>tan_impact_tot</metric>
    <metric>cc_impact_tot</metric>
    <metric>hh_impact_tot</metric>
    <metric>eq_impact_tot</metric>
    <metric>ss_impact_tot</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>matrix:get-row global_econst 0</metric>
    <metric>matrix:get-row global_econst 1</metric>
    <metric>matrix:get-row global_econst 2</metric>
    <metric>matrix:get-row global_econst 3</metric>
    <metric>matrix:get-row global_econst 4</metric>
    <metric>matrix:get-row global_econst 5</metric>
    <metric>matrix:get-row global_econst 6</metric>
    <metric>matrix:get-row global_econst 7</metric>
    <metric>matrix:get-row global_econst 8</metric>
    <metric>matrix:get-row global_econst 9</metric>
    <metric>matrix:get-row global_econst 10</metric>
    <metric>matrix:get-row global_default_econst 0</metric>
    <metric>matrix:get-row global_default_econst 1</metric>
    <metric>matrix:get-row global_default_econst 2</metric>
    <metric>matrix:get-row global_default_econst 3</metric>
    <metric>matrix:get-row global_default_econst 4</metric>
    <metric>matrix:get-row global_default_econst 5</metric>
    <metric>matrix:get-row global_default_econst 6</metric>
    <metric>matrix:get-row global_default_econst 7</metric>
    <metric>matrix:get-row global_default_econst 8</metric>
    <metric>matrix:get-row global_default_econst 9</metric>
    <metric>matrix:get-row global_default_econst 10</metric>
    <metric>matrix:get-row passive0_econst 0</metric>
    <metric>matrix:get-row passive0_econst 1</metric>
    <metric>matrix:get-row passive0_econst 2</metric>
    <metric>matrix:get-row passive0_econst 3</metric>
    <metric>matrix:get-row passive0_econst 4</metric>
    <metric>matrix:get-row passive0_econst 5</metric>
    <metric>matrix:get-row passive0_econst 6</metric>
    <metric>matrix:get-row passive0_econst 7</metric>
    <metric>matrix:get-row passive0_econst 8</metric>
    <metric>matrix:get-row passive0_econst 9</metric>
    <metric>matrix:get-row passive0_econst 10</metric>
    <metric>matrix:get-row passive0_defaulteconst 0</metric>
    <metric>matrix:get-row passive0_defaulteconst 1</metric>
    <metric>matrix:get-row passive0_defaulteconst 2</metric>
    <metric>matrix:get-row passive0_defaulteconst 3</metric>
    <metric>matrix:get-row passive0_defaulteconst 4</metric>
    <metric>matrix:get-row passive0_defaulteconst 5</metric>
    <metric>matrix:get-row passive0_defaulteconst 6</metric>
    <metric>matrix:get-row passive0_defaulteconst 7</metric>
    <metric>matrix:get-row passive0_defaulteconst 8</metric>
    <metric>matrix:get-row passive0_defaulteconst 9</metric>
    <metric>matrix:get-row passive0_defaulteconst 10</metric>
    <metric>matrix:get-row yellowguy_econst 0</metric>
    <metric>matrix:get-row yellowguy_econst 1</metric>
    <metric>matrix:get-row yellowguy_econst 2</metric>
    <metric>matrix:get-row yellowguy_econst 3</metric>
    <metric>matrix:get-row yellowguy_econst 4</metric>
    <metric>matrix:get-row yellowguy_econst 5</metric>
    <metric>matrix:get-row yellowguy_econst 6</metric>
    <metric>matrix:get-row yellowguy_econst 7</metric>
    <metric>matrix:get-row yellowguy_econst 8</metric>
    <metric>matrix:get-row yellowguy_econst 9</metric>
    <metric>matrix:get-row yellowguy_econst 10</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 0</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 1</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 2</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 3</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 4</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 5</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 6</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 7</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 8</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 9</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 10</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="NonLinearity" repetitions="30" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
      <value value="2"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="ConstantWeather" repetitions="30" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Constantmix" repetitions="30" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Validation&amp;SensitivityAnalysis_DO" repetitions="30" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
      <value value="2"/>
      <value value="3"/>
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Validation&amp;SensitivityAnalysis_DO_moreImpact" repetitions="30" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="12"/>
      <value value="13"/>
      <value value="14"/>
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Validation&amp;SensitivityAnalysis_DO_hcTRUE_Goal&amp;Price" repetitions="10" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
      <value value="2"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
      <value value="2"/>
      <value value="3"/>
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Validation&amp;SensitivityAnalysis_DO_hcTRUE_moreImpact" repetitions="30" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="12"/>
      <value value="13"/>
      <value value="14"/>
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="PcPbh" repetitions="4" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <steppedValueSet variable="P_sbhvr_stalwarts" first="0" step="0.1" last="1"/>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <steppedValueSet variable="P_conform" first="0" step="0.1" last="1"/>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Test_PVsystem" repetitions="30" runMetricsEveryStep="false">
    <setup>if behaviorspace-run-number &gt; 10 [ set PV&amp;Battery_scenario 2 ]
if behaviorspace-run-number &gt; 20 [ set PV&amp;Battery_scenario 3 ]
setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Test_PVsystem2" repetitions="30" runMetricsEveryStep="false">
    <setup>if behaviorspace-run-number &gt; 10 [ set PV&amp;Battery_scenario 2 ]
if behaviorspace-run-number &gt; 20 [ set PV&amp;Battery_scenario 3 ]
setup</setup>
    <go>go</go>
    <metric>PV&amp;Battery_scenario</metric>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Test_Plackett-Bruman" repetitions="120" runMetricsEveryStep="false">
    <setup>if behaviorspace-run-number &gt; 0 [ set price_scenario 1
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 3 set impact_goal 9 set vision 1
  set h&amp;c_opti? true set rebound_scenario 3
  set K_rgraph 99 set rewiring_prob 0 set turtle_multiplier 1
  set PV&amp;Battery_scenario 2 ]
if behaviorspace-run-number &gt; 10 [ set price_scenario 1
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.625 set P_conform 0.275
  set goal 3 set impact_goal 9 set vision 1
  set h&amp;c_opti? false set rebound_scenario 3
  set K_rgraph 99 set rewiring_prob 0 set turtle_multiplier 3
  set PV&amp;Battery_scenario 1 ]
if behaviorspace-run-number &gt; 20 [ set price_scenario 2
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.7
  set goal 3 set impact_goal 9 set vision 1
  set h&amp;c_opti? false set rebound_scenario 1
  set K_rgraph 99 set rewiring_prob 0 set turtle_multiplier 3
  set PV&amp;Battery_scenario 2 ]
if behaviorspace-run-number &gt; 30 [ set price_scenario 1
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.625 set P_conform 0.275
  set goal 2 set impact_goal 15 set vision 1
  set h&amp;c_opti? false set rebound_scenario 1
  set K_rgraph 10 set rewiring_prob 0.1 set turtle_multiplier 3
  set PV&amp;Battery_scenario 2 ]
if behaviorspace-run-number &gt; 40 [ set price_scenario 1
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.625 set P_conform 0.7
  set goal 3 set impact_goal 9 set vision 1
  set h&amp;c_opti? true set rebound_scenario 1
  set K_rgraph 10 set rewiring_prob 0.1 set turtle_multiplier 1
  set PV&amp;Battery_scenario 2 ]
if behaviorspace-run-number &gt; 50 [ set price_scenario 1
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.7
  set goal 2 set impact_goal 15 set vision 1
  set h&amp;c_opti? false set rebound_scenario 3
  set K_rgraph 10 set rewiring_prob 0.1 set turtle_multiplier 1
  set PV&amp;Battery_scenario 1 ]
if behaviorspace-run-number &gt; 60 [ set price_scenario 2
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.625 set P_conform 0.275
  set goal 2 set impact_goal 15 set vision 1
  set h&amp;c_opti? true set rebound_scenario 1
  set K_rgraph 99 set rewiring_prob 0 set turtle_multiplier 1
  set PV&amp;Battery_scenario 1  ]
if behaviorspace-run-number &gt; 70 [ set price_scenario 2
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.625 set P_conform 0.7
  set goal 3 set impact_goal 9 set vision 1
  set h&amp;c_opti? true set rebound_scenario 3
  set K_rgraph 10 set rewiring_prob 0.1 set turtle_multiplier 3
  set PV&amp;Battery_scenario 1  ]
if behaviorspace-run-number &gt; 80 [ set price_scenario 2
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.625 set P_conform 0.7
  set goal 2 set impact_goal 15 set vision 1
  set h&amp;c_opti? false set rebound_scenario 3
  set K_rgraph 99 set rewiring_prob 0 set turtle_multiplier 1
  set PV&amp;Battery_scenario 2 ]
if behaviorspace-run-number &gt; 90 [ set price_scenario 1
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.7
  set goal 2 set impact_goal 15 set vision 1
  set h&amp;c_opti? true set rebound_scenario 1
  set K_rgraph 299 set rewiring_prob 0 set turtle_multiplier 3
  set PV&amp;Battery_scenario 1 ]
if behaviorspace-run-number &gt; 100 [ set price_scenario 2
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 2 set impact_goal 15 set vision 1
  set h&amp;c_opti? true set rebound_scenario 3
  set K_rgraph 10 set rewiring_prob 0.1 set turtle_multiplier 3
  set PV&amp;Battery_scenario 2  ]
if behaviorspace-run-number &gt; 110 [ set price_scenario 2
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 3 set impact_goal 9 set vision 1
  set h&amp;c_opti? false set rebound_scenario 1
  set K_rgraph 10 set rewiring_prob 0.1 set turtle_multiplier 1
  set PV&amp;Battery_scenario 1 ]
setup</setup>
    <go>go</go>
    <metric>price_scenario</metric>
    <metric>up_north?</metric>
    <metric>proportion_agent_type</metric>
    <metric>P_sbhvr_stalwarts</metric>
    <metric>P_conform</metric>
    <metric>goal</metric>
    <metric>impact_goal</metric>
    <metric>vision</metric>
    <metric>h&amp;c_opti?</metric>
    <metric>rebound_scenario</metric>
    <metric>K_rgraph</metric>
    <metric>rewiring_prob</metric>
    <metric>turtle_multiplier</metric>
    <metric>PV&amp;Battery_scenario</metric>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="A) Plackett-Bruman" repetitions="120" runMetricsEveryStep="false">
    <setup>if behaviorspace-run-number &gt; 0 [ set price_scenario 1
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 3 set impact_goal 9 set vision 1
  set h&amp;c_opti? true set rebound_scenario 3
  set K_rgraph 99 set rewiring_prob 0 set turtle_multiplier 1
  set PV&amp;Battery_scenario 2 ]
if behaviorspace-run-number &gt; 10 [ set price_scenario 1
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.625 set P_conform 0.275
  set goal 3 set impact_goal 9 set vision 1
  set h&amp;c_opti? false set rebound_scenario 3
  set K_rgraph 99 set rewiring_prob 0 set turtle_multiplier 3
  set PV&amp;Battery_scenario 1 ]
if behaviorspace-run-number &gt; 20 [ set price_scenario 2
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.7
  set goal 3 set impact_goal 9 set vision 1
  set h&amp;c_opti? false set rebound_scenario 1
  set K_rgraph 99 set rewiring_prob 0 set turtle_multiplier 3
  set PV&amp;Battery_scenario 2 ]
if behaviorspace-run-number &gt; 30 [ set price_scenario 1
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.625 set P_conform 0.275
  set goal 2 set impact_goal 15 set vision 1
  set h&amp;c_opti? false set rebound_scenario 1
  set K_rgraph 10 set rewiring_prob 0.1 set turtle_multiplier 3
  set PV&amp;Battery_scenario 2 ]
if behaviorspace-run-number &gt; 40 [ set price_scenario 1
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.625 set P_conform 0.7
  set goal 3 set impact_goal 9 set vision 1
  set h&amp;c_opti? true set rebound_scenario 1
  set K_rgraph 10 set rewiring_prob 0.1 set turtle_multiplier 1
  set PV&amp;Battery_scenario 2 ]
if behaviorspace-run-number &gt; 50 [ set price_scenario 1
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.7
  set goal 2 set impact_goal 15 set vision 1
  set h&amp;c_opti? false set rebound_scenario 3
  set K_rgraph 10 set rewiring_prob 0.1 set turtle_multiplier 1
  set PV&amp;Battery_scenario 1 ]
if behaviorspace-run-number &gt; 60 [ set price_scenario 2
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.625 set P_conform 0.275
  set goal 2 set impact_goal 15 set vision 1
  set h&amp;c_opti? true set rebound_scenario 1
  set K_rgraph 99 set rewiring_prob 0 set turtle_multiplier 1
  set PV&amp;Battery_scenario 1  ]
if behaviorspace-run-number &gt; 70 [ set price_scenario 2
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.625 set P_conform 0.7
  set goal 3 set impact_goal 9 set vision 1
  set h&amp;c_opti? true set rebound_scenario 3
  set K_rgraph 10 set rewiring_prob 0.1 set turtle_multiplier 3
  set PV&amp;Battery_scenario 1  ]
if behaviorspace-run-number &gt; 80 [ set price_scenario 2
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.625 set P_conform 0.7
  set goal 2 set impact_goal 15 set vision 1
  set h&amp;c_opti? false set rebound_scenario 3
  set K_rgraph 99 set rewiring_prob 0 set turtle_multiplier 1
  set PV&amp;Battery_scenario 2 ]
if behaviorspace-run-number &gt; 90 [ set price_scenario 1
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.7
  set goal 2 set impact_goal 15 set vision 1
  set h&amp;c_opti? true set rebound_scenario 1
  set K_rgraph 299 set rewiring_prob 0 set turtle_multiplier 3
  set PV&amp;Battery_scenario 1 ]
if behaviorspace-run-number &gt; 100 [ set price_scenario 2
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 2 set impact_goal 15 set vision 1
  set h&amp;c_opti? true set rebound_scenario 3
  set K_rgraph 10 set rewiring_prob 0.1 set turtle_multiplier 3
  set PV&amp;Battery_scenario 2  ]
if behaviorspace-run-number &gt; 110 [ set price_scenario 2
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 3 set impact_goal 9 set vision 1
  set h&amp;c_opti? false set rebound_scenario 1
  set K_rgraph 10 set rewiring_prob 0.1 set turtle_multiplier 1
  set PV&amp;Battery_scenario 1 ]
setup</setup>
    <go>go</go>
    <metric>price_scenario</metric>
    <metric>up_north?</metric>
    <metric>proportion_agent_type</metric>
    <metric>P_sbhvr_stalwarts</metric>
    <metric>P_conform</metric>
    <metric>goal</metric>
    <metric>impact_goal</metric>
    <metric>vision</metric>
    <metric>h&amp;c_opti?</metric>
    <metric>rebound_scenario</metric>
    <metric>K_rgraph</metric>
    <metric>rewiring_prob</metric>
    <metric>turtle_multiplier</metric>
    <metric>PV&amp;Battery_scenario</metric>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="B) FeedbackVsPeakShaving" repetitions="10" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="3"/>
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="true"/>
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="2"/>
      <value value="1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="C) Stern Experiment" repetitions="320" runMetricsEveryStep="false">
    <setup>if behaviorspace-run-number &gt; 0 [ set price_scenario 2
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 3 set impact_goal 9 set vision 1
  set h&amp;c_opti? false set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 10 [ set price_scenario 1
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 2 set impact_goal 15 set vision 1
  set h&amp;c_opti? true set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 20 [ set price_scenario 2
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 2 set impact_goal 15 set vision 1
  set h&amp;c_opti? true set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 30 [ set price_scenario 1
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 3 set impact_goal 9 set vision 1
  set h&amp;c_opti? false set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 40 [ set price_scenario 2
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 2 set impact_goal 15 set vision 1
  set h&amp;c_opti? false set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 50 [ set price_scenario 1
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 3 set impact_goal 9 set vision 1
  set h&amp;c_opti? true set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 60 [ set price_scenario 2
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 3 set impact_goal 9 set vision 1
  set h&amp;c_opti? true set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 70 [ set price_scenario 1
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 2 set impact_goal 15 set vision 1
  set h&amp;c_opti? false set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 80 [ set price_scenario 2
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.625 set P_conform 0.275
  set goal 3 set impact_goal 9 set vision 1
  set h&amp;c_opti? true set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 90 [ set price_scenario 1
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.625 set P_conform 0.275
  set goal 2 set impact_goal 15 set vision 1
  set h&amp;c_opti? false set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 100 [ set price_scenario 2
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.625 set P_conform 0.275
  set goal 2 set impact_goal 15 set vision 1
  set h&amp;c_opti? false set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 110 [ set price_scenario 1
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.625 set P_conform 0.275
  set goal 3 set impact_goal 9 set vision 1
  set h&amp;c_opti? true set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 120 [ set price_scenario 2
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.625 set P_conform 0.275
  set goal 2 set impact_goal 15 set vision 1
  set h&amp;c_opti? true set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 130 [ set price_scenario 1
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.625 set P_conform 0.275
  set goal 3 set impact_goal 9 set vision 1
  set h&amp;c_opti? false set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 140 [ set price_scenario 2
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.625 set P_conform 0.275
  set goal 3 set impact_goal 9 set vision 1
  set h&amp;c_opti? false set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 150 [ set price_scenario 1
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.625 set P_conform 0.275
  set goal 2 set impact_goal 15 set vision 1
  set h&amp;c_opti? true set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 160 [ set price_scenario 2
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.7
  set goal 3 set impact_goal 9 set vision 1
  set h&amp;c_opti? false set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 170 [ set price_scenario 1
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.7
  set goal 2 set impact_goal 15 set vision 1
  set h&amp;c_opti? true set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 180 [ set price_scenario 2
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.7
  set goal 2 set impact_goal 15 set vision 1
  set h&amp;c_opti? true set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 190 [ set price_scenario 1
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.7
  set goal 3 set impact_goal 9 set vision 1
  set h&amp;c_opti? false set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 200 [ set price_scenario 2
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.7
  set goal 2 set impact_goal 15 set vision 1
  set h&amp;c_opti? false set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 210 [ set price_scenario 1
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.7
  set goal 3 set impact_goal 9 set vision 1
  set h&amp;c_opti? true set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 220 [ set price_scenario 2
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.7
  set goal 3 set impact_goal 9 set vision 1
  set h&amp;c_opti? true set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 230 [ set price_scenario 1
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.7
  set goal 2 set impact_goal 15 set vision 1
  set h&amp;c_opti? false set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 240 [ set price_scenario 2
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.625 set P_conform 0.7
  set goal 3 set impact_goal 9 set vision 1
  set h&amp;c_opti? true set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 250 [ set price_scenario 1
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.625 set P_conform 0.7
  set goal 2 set impact_goal 15 set vision 1
  set h&amp;c_opti? false set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 260 [ set price_scenario 2
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.625 set P_conform 0.7
  set goal 2 set impact_goal 15 set vision 1
  set h&amp;c_opti? false set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 270 [ set price_scenario 1
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.625 set P_conform 0.7
  set goal 3 set impact_goal 9 set vision 1
  set h&amp;c_opti? true set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 280 [ set price_scenario 2
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.625 set P_conform 0.7
  set goal 2 set impact_goal 15 set vision 1
  set h&amp;c_opti? true set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 290 [ set price_scenario 1
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.625 set P_conform 0.7
  set goal 3 set impact_goal 9 set vision 1
  set h&amp;c_opti? false set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 300 [ set price_scenario 2
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.625 set P_conform 0.7
  set goal 3 set impact_goal 9 set vision 1
  set h&amp;c_opti? false set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 310 [ set price_scenario 1
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.625 set P_conform 0.7
  set goal 2 set impact_goal 15 set vision 1
  set h&amp;c_opti? true set rebound_scenario 3 ]
setup</setup>
    <go>go</go>
    <metric>price_scenario</metric>
    <metric>up_north?</metric>
    <metric>proportion_agent_type</metric>
    <metric>P_sbhvr_stalwarts</metric>
    <metric>P_conform</metric>
    <metric>goal</metric>
    <metric>impact_goal</metric>
    <metric>vision</metric>
    <metric>h&amp;c_opti?</metric>
    <metric>rebound_scenario</metric>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="C) Stern Experimencomplete" repetitions="1" runMetricsEveryStep="false">
    <setup>if behaviorspace-run-number &gt; 0 [ set price_scenario 1
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.625 set P_conform 0.275
  set goal 2 set impact_goal 15 set vision 1
  set h&amp;c_opti? false set rebound_scenario 1 ]
setup</setup>
    <go>go</go>
    <metric>price_scenario</metric>
    <metric>up_north?</metric>
    <metric>proportion_agent_type</metric>
    <metric>P_sbhvr_stalwarts</metric>
    <metric>P_conform</metric>
    <metric>goal</metric>
    <metric>impact_goal</metric>
    <metric>vision</metric>
    <metric>h&amp;c_opti?</metric>
    <metric>rebound_scenario</metric>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="E) Re-do optimization results (h&amp;c true, PVBatt 2)" repetitions="30" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
      <value value="2"/>
      <value value="3"/>
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="E) Re-do optimization results (h&amp;c true, PVBatt 2) - more impact" repetitions="30" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="12"/>
      <value value="13"/>
      <value value="14"/>
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="E) Re-do optimization results (h&amp;c true, PVBatt 2) - more impact-2-ss" repetitions="8" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="E) Re-do optimization results (h&amp;c true, PVBatt 2) - more impact-2-ss-bis" repetitions="30" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="E) Re-do optimization results (h&amp;c false, PVBatt 2)" repetitions="30" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
      <value value="2"/>
      <value value="3"/>
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="E) Re-do optimization results (h&amp;c false, PVBatt 2) - impact" repetitions="30" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="12"/>
      <value value="13"/>
      <value value="14"/>
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="E) Re-do optimization results (h&amp;c false, PVBatt 2) TEST" repetitions="4" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="E) Re-do2 optimization results (h&amp;c false, PVBatt 2)" repetitions="30" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
      <value value="2"/>
      <value value="3"/>
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="E) Re-do2 optimization results (h&amp;c false, PVBatt 2) - more impact" repetitions="30" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="12"/>
      <value value="13"/>
      <value value="14"/>
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="E) Re-do2 optimization results (h&amp;c false, PVBatt 2) - ss" repetitions="30" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
      <value value="2"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="C) Stern Experiment - ReDo2-goal1-2" repetitions="320" runMetricsEveryStep="false">
    <setup>if behaviorspace-run-number &gt; 0 [ set price_scenario 2
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 10 [ set price_scenario 1
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 20 [ set price_scenario 2
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 30 [ set price_scenario 1
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 40 [ set price_scenario 2
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 50 [ set price_scenario 1
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 60 [ set price_scenario 2
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 70 [ set price_scenario 1
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 80 [ set price_scenario 2
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.3 set P_conform 0.275
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 90 [ set price_scenario 1
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.3 set P_conform 0.275
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 100 [ set price_scenario 2
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.3 set P_conform 0.275
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 110 [ set price_scenario 1
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.3 set P_conform 0.275
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 120 [ set price_scenario 2
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.3 set P_conform 0.275
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 130 [ set price_scenario 1
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.3 set P_conform 0.275
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 140 [ set price_scenario 2
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.3 set P_conform 0.275
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 150 [ set price_scenario 1
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.3 set P_conform 0.275
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 160 [ set price_scenario 2
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.375
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 170 [ set price_scenario 1
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.375
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 180 [ set price_scenario 2
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.375
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 190 [ set price_scenario 1
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.375
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 200 [ set price_scenario 2
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.375
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 210 [ set price_scenario 1
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.375
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 220 [ set price_scenario 2
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.375
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 230 [ set price_scenario 1
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.375
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 240 [ set price_scenario 2
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.3 set P_conform 0.375
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 250 [ set price_scenario 1
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.3 set P_conform 0.375
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 260 [ set price_scenario 2
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.3 set P_conform 0.375
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 270 [ set price_scenario 1
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.3 set P_conform 0.375
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 280 [ set price_scenario 2
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.3 set P_conform 0.375
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 290 [ set price_scenario 1
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.3 set P_conform 0.375
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 300 [ set price_scenario 2
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.3 set P_conform 0.375
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 310 [ set price_scenario 1
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.3 set P_conform 0.375
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 3 ]
setup</setup>
    <go>go</go>
    <metric>price_scenario</metric>
    <metric>up_north?</metric>
    <metric>proportion_agent_type</metric>
    <metric>P_sbhvr_stalwarts</metric>
    <metric>P_conform</metric>
    <metric>goal</metric>
    <metric>impact_goal</metric>
    <metric>vision</metric>
    <metric>PV&amp;Battery_scenario</metric>
    <metric>rebound_scenario</metric>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="E) Re-do2 optimization results (h&amp;c false, PVBatt 2) TEST" repetitions="5" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
      <value value="2"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="C) Stern Experiment - ReDo3-goal1-2" repetitions="320" runMetricsEveryStep="false">
    <setup>if behaviorspace-run-number &gt; 0 [ set price_scenario 2
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 10 [ set price_scenario 1
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 20 [ set price_scenario 2
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 30 [ set price_scenario 1
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 40 [ set price_scenario 2
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 50 [ set price_scenario 1
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 60 [ set price_scenario 2
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 70 [ set price_scenario 1
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.275
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 80 [ set price_scenario 2
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.3 set P_conform 0.275
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 90 [ set price_scenario 1
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.3 set P_conform 0.275
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 100 [ set price_scenario 2
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.3 set P_conform 0.275
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 110 [ set price_scenario 1
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.3 set P_conform 0.275
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 120 [ set price_scenario 2
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.3 set P_conform 0.275
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 130 [ set price_scenario 1
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.3 set P_conform 0.275
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 140 [ set price_scenario 2
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.3 set P_conform 0.275
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 150 [ set price_scenario 1
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.3 set P_conform 0.275
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 160 [ set price_scenario 2
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.375
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 170 [ set price_scenario 1
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.375
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 180 [ set price_scenario 2
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.375
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 190 [ set price_scenario 1
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.2 set P_conform 0.375
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 200 [ set price_scenario 2
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.375
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 210 [ set price_scenario 1
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.375
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 220 [ set price_scenario 2
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.375
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 230 [ set price_scenario 1
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.2 set P_conform 0.375
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 240 [ set price_scenario 2
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.3 set P_conform 0.375
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 250 [ set price_scenario 1
  set up_north? false set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.3 set P_conform 0.375
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 260 [ set price_scenario 2
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.3 set P_conform 0.375
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 270 [ set price_scenario 1
  set up_north? true set proportion_agent_type 1
  set P_sbhvr_stalwarts 0.3 set P_conform 0.375
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 280 [ set price_scenario 2
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.3 set P_conform 0.375
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 290 [ set price_scenario 1
  set up_north? false set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.3 set P_conform 0.375
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 1 ]
if behaviorspace-run-number &gt; 300 [ set price_scenario 2
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.3 set P_conform 0.375
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 1 set rebound_scenario 3 ]
if behaviorspace-run-number &gt; 310 [ set price_scenario 1
  set up_north? true set proportion_agent_type 2
  set P_sbhvr_stalwarts 0.3 set P_conform 0.375
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 2 set rebound_scenario 3 ]
setup</setup>
    <go>go</go>
    <metric>price_scenario</metric>
    <metric>up_north?</metric>
    <metric>proportion_agent_type</metric>
    <metric>P_sbhvr_stalwarts</metric>
    <metric>P_conform</metric>
    <metric>goal</metric>
    <metric>impact_goal</metric>
    <metric>vision</metric>
    <metric>PV&amp;Battery_scenario</metric>
    <metric>rebound_scenario</metric>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="C) Stern Experiment - ReDo3-goal1-2_CheckPriceEffect" repetitions="80" runMetricsEveryStep="false">
    <setup>if behaviorspace-run-number &gt; 0 [ set price_scenario 2
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 1 ]
if behaviorspace-run-number &gt; 10 [ set price_scenario 1
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 1 ]
if behaviorspace-run-number &gt; 20 [ set price_scenario 2
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 1 ]
if behaviorspace-run-number &gt; 30 [ set price_scenario 1
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 1 ]
if behaviorspace-run-number &gt; 40 [ set price_scenario 2
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 2 ]
if behaviorspace-run-number &gt; 50 [ set price_scenario 1
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 2 ]
if behaviorspace-run-number &gt; 60 [ set price_scenario 2
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 2 ]
if behaviorspace-run-number &gt; 70 [ set price_scenario 1
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 2 ]
setup</setup>
    <go>go</go>
    <metric>price_scenario</metric>
    <metric>goal</metric>
    <metric>impact_goal</metric>
    <metric>vision</metric>
    <metric>PV&amp;Battery_scenario</metric>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="C) SternE_CheckPriceEffect_noFeedback" repetitions="80" runMetricsEveryStep="false">
    <setup>if behaviorspace-run-number &gt; 0 [ set price_scenario 2
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 1 ]
if behaviorspace-run-number &gt; 10 [ set price_scenario 1
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 1 ]
if behaviorspace-run-number &gt; 20 [ set price_scenario 2
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 1 ]
if behaviorspace-run-number &gt; 30 [ set price_scenario 1
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 1 ]
if behaviorspace-run-number &gt; 40 [ set price_scenario 2
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 2 ]
if behaviorspace-run-number &gt; 50 [ set price_scenario 1
  set goal 1 set impact_goal 9 set vision 1
  set PV&amp;Battery_scenario 2 ]
if behaviorspace-run-number &gt; 60 [ set price_scenario 2
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 2 ]
if behaviorspace-run-number &gt; 70 [ set price_scenario 1
  set goal 2 set impact_goal 12 set vision 1
  set PV&amp;Battery_scenario 2 ]
setup</setup>
    <go>go</go>
    <metric>price_scenario</metric>
    <metric>goal</metric>
    <metric>impact_goal</metric>
    <metric>vision</metric>
    <metric>PV&amp;Battery_scenario</metric>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Article after review 2 - Life Cycle Inventory" repetitions="10" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>cc_impact_lci</metric>
    <metric>ae_impact_lci</metric>
    <metric>lo_impact_lci</metric>
    <metric>tan_impact_lci</metric>
    <metric>te_impact_lci</metric>
    <metric>ht_impact_lci</metric>
    <metric>ir_impact_lci</metric>
    <metric>old_impact_lci</metric>
    <metric>po_impact_lci</metric>
    <metric>re_impact_lci</metric>
    <metric>rme_impact_lci</metric>
    <metric>nre_impact_lci</metric>
    <metric>cc_impact_lcia</metric>
    <metric>ae_impact_lcia</metric>
    <metric>lo_impact_lcia</metric>
    <metric>tan_impact_lcia</metric>
    <metric>te_impact_lcia</metric>
    <metric>ht_impact_lcia</metric>
    <metric>ir_impact_lcia</metric>
    <metric>old_impact_lcia</metric>
    <metric>po_impact_lcia</metric>
    <metric>re_impact_lcia</metric>
    <metric>rme_impact_lcia</metric>
    <metric>nre_impact_lcia</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Article after review 2 - Life Cycle Inventory - contribution" repetitions="10" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>cc_impact_lci</metric>
    <metric>detailed_lci_proc1</metric>
    <metric>detailed_lci_proc2</metric>
    <metric>detailed_lci_proc3</metric>
    <metric>detailed_lci_proc4</metric>
    <metric>detailed_lci_proc5</metric>
    <metric>detailed_lci_proc6</metric>
    <metric>detailed_lci_proc7</metric>
    <metric>detailed_lci_proc8</metric>
    <metric>detailed_lci_proc9</metric>
    <metric>detailed_lci_proc10</metric>
    <metric>detailed_lci_proc11</metric>
    <metric>detailed_lci_proc12</metric>
    <metric>detailed_lci_proc13</metric>
    <metric>detailed_lci_proc14</metric>
    <metric>detailed_lci_proc15</metric>
    <metric>detailed_lci_proc16</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="NetworkType" repetitions="1000" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>stop</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
      <value value="2"/>
      <value value="3"/>
      <value value="4"/>
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="PrepareNonLinearity" repetitions="10" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <exitCondition>nd = 3</exitCondition>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>electric_consumption</metric>
    <metric>electric_consumption2</metric>
    <metric>costbis</metric>
    <metric>costbis2</metric>
    <metric>ht_default_impactbis</metric>
    <metric>ht_impactbis</metric>
    <metric>ht_default_impact_building</metric>
    <metric>re_default_impactbis</metric>
    <metric>re_impactbis</metric>
    <metric>re_default_impact_building</metric>
    <metric>ir_default_impactbis</metric>
    <metric>ir_impactbis</metric>
    <metric>ir_default_impact_building</metric>
    <metric>old_default_impactbis</metric>
    <metric>old_impactbis</metric>
    <metric>old_default_impact_building</metric>
    <metric>po_default_impactbis</metric>
    <metric>po_impactbis</metric>
    <metric>po_default_impact_building</metric>
    <metric>ae_default_impactbis</metric>
    <metric>ae_impactbis</metric>
    <metric>ae_default_impact_building</metric>
    <metric>te_default_impactbis</metric>
    <metric>te_impactbis</metric>
    <metric>te_default_impact_building</metric>
    <metric>tan_default_impactbis</metric>
    <metric>tan_impactbis</metric>
    <metric>tan_default_impact_building</metric>
    <metric>lo_default_impactbis</metric>
    <metric>lo_impactbis</metric>
    <metric>lo_default_impact_building</metric>
    <metric>cc_default_impactbis</metric>
    <metric>cc_impactbis</metric>
    <metric>cc_default_impact_building</metric>
    <metric>nre_default_impactbis</metric>
    <metric>nre_impactbis</metric>
    <metric>nre_default_impact_building</metric>
    <metric>rme_default_impactbis</metric>
    <metric>rme_impactbis</metric>
    <metric>rme_default_impact_building</metric>
    <metric>hh_default_impactbis</metric>
    <metric>hh_impactbis</metric>
    <metric>hh_default_impact_building</metric>
    <metric>eq_default_impactbis</metric>
    <metric>eq_impactbis</metric>
    <metric>eq_default_impact_building</metric>
    <metric>res_default_impactbis</metric>
    <metric>res_impactbis</metric>
    <metric>res_default_impact_building</metric>
    <metric>ss_default_impactbis</metric>
    <metric>ss_impactbis</metric>
    <metric>ss_default_impact_building</metric>
    <metric>ht_impact_tot</metric>
    <metric>re_impact_tot</metric>
    <metric>old_impact_tot</metric>
    <metric>po_impact_tot</metric>
    <metric>ae_impact_tot</metric>
    <metric>te_impact_tot</metric>
    <metric>tan_impact_tot</metric>
    <metric>cc_impact_tot</metric>
    <metric>hh_impact_tot</metric>
    <metric>eq_impact_tot</metric>
    <metric>ss_impact_tot</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>matrix:get-row global_econst 0</metric>
    <metric>matrix:get-row global_econst 1</metric>
    <metric>matrix:get-row global_econst 2</metric>
    <metric>matrix:get-row global_econst 3</metric>
    <metric>matrix:get-row global_econst 4</metric>
    <metric>matrix:get-row global_econst 5</metric>
    <metric>matrix:get-row global_econst 6</metric>
    <metric>matrix:get-row global_econst 7</metric>
    <metric>matrix:get-row global_econst 8</metric>
    <metric>matrix:get-row global_econst 9</metric>
    <metric>matrix:get-row global_econst 10</metric>
    <metric>matrix:get-row global_econst 11</metric>
    <metric>matrix:get-row global_default_econst 0</metric>
    <metric>matrix:get-row global_default_econst 1</metric>
    <metric>matrix:get-row global_default_econst 2</metric>
    <metric>matrix:get-row global_default_econst 3</metric>
    <metric>matrix:get-row global_default_econst 4</metric>
    <metric>matrix:get-row global_default_econst 5</metric>
    <metric>matrix:get-row global_default_econst 6</metric>
    <metric>matrix:get-row global_default_econst 7</metric>
    <metric>matrix:get-row global_default_econst 8</metric>
    <metric>matrix:get-row global_default_econst 9</metric>
    <metric>matrix:get-row global_default_econst 10</metric>
    <metric>matrix:get-row global_default_econst 11</metric>
    <metric>matrix:get-row passive0_econst 0</metric>
    <metric>matrix:get-row passive0_econst 1</metric>
    <metric>matrix:get-row passive0_econst 2</metric>
    <metric>matrix:get-row passive0_econst 3</metric>
    <metric>matrix:get-row passive0_econst 4</metric>
    <metric>matrix:get-row passive0_econst 5</metric>
    <metric>matrix:get-row passive0_econst 6</metric>
    <metric>matrix:get-row passive0_econst 7</metric>
    <metric>matrix:get-row passive0_econst 8</metric>
    <metric>matrix:get-row passive0_econst 9</metric>
    <metric>matrix:get-row passive0_econst 10</metric>
    <metric>matrix:get-row passive0_econst 11</metric>
    <metric>matrix:get-row passive0_defaulteconst 0</metric>
    <metric>matrix:get-row passive0_defaulteconst 1</metric>
    <metric>matrix:get-row passive0_defaulteconst 2</metric>
    <metric>matrix:get-row passive0_defaulteconst 3</metric>
    <metric>matrix:get-row passive0_defaulteconst 4</metric>
    <metric>matrix:get-row passive0_defaulteconst 5</metric>
    <metric>matrix:get-row passive0_defaulteconst 6</metric>
    <metric>matrix:get-row passive0_defaulteconst 7</metric>
    <metric>matrix:get-row passive0_defaulteconst 8</metric>
    <metric>matrix:get-row passive0_defaulteconst 9</metric>
    <metric>matrix:get-row passive0_defaulteconst 10</metric>
    <metric>matrix:get-row passive0_defaulteconst 11</metric>
    <metric>matrix:get-row yellowguy_econst 0</metric>
    <metric>matrix:get-row yellowguy_econst 1</metric>
    <metric>matrix:get-row yellowguy_econst 2</metric>
    <metric>matrix:get-row yellowguy_econst 3</metric>
    <metric>matrix:get-row yellowguy_econst 4</metric>
    <metric>matrix:get-row yellowguy_econst 5</metric>
    <metric>matrix:get-row yellowguy_econst 6</metric>
    <metric>matrix:get-row yellowguy_econst 7</metric>
    <metric>matrix:get-row yellowguy_econst 8</metric>
    <metric>matrix:get-row yellowguy_econst 9</metric>
    <metric>matrix:get-row yellowguy_econst 10</metric>
    <metric>matrix:get-row yellowguy_econst 11</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 0</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 1</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 2</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 3</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 4</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 5</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 6</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 7</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 8</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 9</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 10</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 11</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <steppedValueSet variable="P_conform" first="0.55" step="0.01" last="0.65"/>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="q-model">
      <value value="4"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="PrepareNonLinearity_v2" repetitions="10" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <exitCondition>nd = 3</exitCondition>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>electric_consumption</metric>
    <metric>electric_consumption2</metric>
    <metric>costbis</metric>
    <metric>costbis2</metric>
    <metric>ht_default_impactbis</metric>
    <metric>ht_impactbis</metric>
    <metric>ht_default_impact_building</metric>
    <metric>re_default_impactbis</metric>
    <metric>re_impactbis</metric>
    <metric>re_default_impact_building</metric>
    <metric>ir_default_impactbis</metric>
    <metric>ir_impactbis</metric>
    <metric>ir_default_impact_building</metric>
    <metric>old_default_impactbis</metric>
    <metric>old_impactbis</metric>
    <metric>old_default_impact_building</metric>
    <metric>po_default_impactbis</metric>
    <metric>po_impactbis</metric>
    <metric>po_default_impact_building</metric>
    <metric>ae_default_impactbis</metric>
    <metric>ae_impactbis</metric>
    <metric>ae_default_impact_building</metric>
    <metric>te_default_impactbis</metric>
    <metric>te_impactbis</metric>
    <metric>te_default_impact_building</metric>
    <metric>tan_default_impactbis</metric>
    <metric>tan_impactbis</metric>
    <metric>tan_default_impact_building</metric>
    <metric>lo_default_impactbis</metric>
    <metric>lo_impactbis</metric>
    <metric>lo_default_impact_building</metric>
    <metric>cc_default_impactbis</metric>
    <metric>cc_impactbis</metric>
    <metric>cc_default_impact_building</metric>
    <metric>nre_default_impactbis</metric>
    <metric>nre_impactbis</metric>
    <metric>nre_default_impact_building</metric>
    <metric>rme_default_impactbis</metric>
    <metric>rme_impactbis</metric>
    <metric>rme_default_impact_building</metric>
    <metric>hh_default_impactbis</metric>
    <metric>hh_impactbis</metric>
    <metric>hh_default_impact_building</metric>
    <metric>eq_default_impactbis</metric>
    <metric>eq_impactbis</metric>
    <metric>eq_default_impact_building</metric>
    <metric>res_default_impactbis</metric>
    <metric>res_impactbis</metric>
    <metric>res_default_impact_building</metric>
    <metric>ss_default_impactbis</metric>
    <metric>ss_impactbis</metric>
    <metric>ss_default_impact_building</metric>
    <metric>ht_impact_tot</metric>
    <metric>re_impact_tot</metric>
    <metric>old_impact_tot</metric>
    <metric>po_impact_tot</metric>
    <metric>ae_impact_tot</metric>
    <metric>te_impact_tot</metric>
    <metric>tan_impact_tot</metric>
    <metric>cc_impact_tot</metric>
    <metric>hh_impact_tot</metric>
    <metric>eq_impact_tot</metric>
    <metric>ss_impact_tot</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>matrix:get-row global_econst 0</metric>
    <metric>matrix:get-row global_econst 1</metric>
    <metric>matrix:get-row global_econst 2</metric>
    <metric>matrix:get-row global_econst 3</metric>
    <metric>matrix:get-row global_econst 4</metric>
    <metric>matrix:get-row global_econst 5</metric>
    <metric>matrix:get-row global_econst 6</metric>
    <metric>matrix:get-row global_econst 7</metric>
    <metric>matrix:get-row global_econst 8</metric>
    <metric>matrix:get-row global_econst 9</metric>
    <metric>matrix:get-row global_econst 10</metric>
    <metric>matrix:get-row global_econst 11</metric>
    <metric>matrix:get-row global_default_econst 0</metric>
    <metric>matrix:get-row global_default_econst 1</metric>
    <metric>matrix:get-row global_default_econst 2</metric>
    <metric>matrix:get-row global_default_econst 3</metric>
    <metric>matrix:get-row global_default_econst 4</metric>
    <metric>matrix:get-row global_default_econst 5</metric>
    <metric>matrix:get-row global_default_econst 6</metric>
    <metric>matrix:get-row global_default_econst 7</metric>
    <metric>matrix:get-row global_default_econst 8</metric>
    <metric>matrix:get-row global_default_econst 9</metric>
    <metric>matrix:get-row global_default_econst 10</metric>
    <metric>matrix:get-row global_default_econst 11</metric>
    <metric>matrix:get-row passive0_econst 0</metric>
    <metric>matrix:get-row passive0_econst 1</metric>
    <metric>matrix:get-row passive0_econst 2</metric>
    <metric>matrix:get-row passive0_econst 3</metric>
    <metric>matrix:get-row passive0_econst 4</metric>
    <metric>matrix:get-row passive0_econst 5</metric>
    <metric>matrix:get-row passive0_econst 6</metric>
    <metric>matrix:get-row passive0_econst 7</metric>
    <metric>matrix:get-row passive0_econst 8</metric>
    <metric>matrix:get-row passive0_econst 9</metric>
    <metric>matrix:get-row passive0_econst 10</metric>
    <metric>matrix:get-row passive0_econst 11</metric>
    <metric>matrix:get-row passive0_defaulteconst 0</metric>
    <metric>matrix:get-row passive0_defaulteconst 1</metric>
    <metric>matrix:get-row passive0_defaulteconst 2</metric>
    <metric>matrix:get-row passive0_defaulteconst 3</metric>
    <metric>matrix:get-row passive0_defaulteconst 4</metric>
    <metric>matrix:get-row passive0_defaulteconst 5</metric>
    <metric>matrix:get-row passive0_defaulteconst 6</metric>
    <metric>matrix:get-row passive0_defaulteconst 7</metric>
    <metric>matrix:get-row passive0_defaulteconst 8</metric>
    <metric>matrix:get-row passive0_defaulteconst 9</metric>
    <metric>matrix:get-row passive0_defaulteconst 10</metric>
    <metric>matrix:get-row passive0_defaulteconst 11</metric>
    <metric>matrix:get-row yellowguy_econst 0</metric>
    <metric>matrix:get-row yellowguy_econst 1</metric>
    <metric>matrix:get-row yellowguy_econst 2</metric>
    <metric>matrix:get-row yellowguy_econst 3</metric>
    <metric>matrix:get-row yellowguy_econst 4</metric>
    <metric>matrix:get-row yellowguy_econst 5</metric>
    <metric>matrix:get-row yellowguy_econst 6</metric>
    <metric>matrix:get-row yellowguy_econst 7</metric>
    <metric>matrix:get-row yellowguy_econst 8</metric>
    <metric>matrix:get-row yellowguy_econst 9</metric>
    <metric>matrix:get-row yellowguy_econst 10</metric>
    <metric>matrix:get-row yellowguy_econst 11</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 0</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 1</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 2</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 3</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 4</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 5</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 6</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 7</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 8</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 9</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 10</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 11</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <steppedValueSet variable="P_conform" first="0.59" step="0.0025" last="0.6"/>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="q-model">
      <value value="4"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Article 2 - Life Cycle Inventory - test" repetitions="1" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <final>let file_name_cc "000_run0_LCIAContribution_cc.csv"
set file_name_cc replace-item 7 file_name_cc (word behaviorspace-run-number)
let file_name_hh "000_run0_LCIAContribution_hh.csv"
set file_name_hh replace-item 7 file_name_hh (word behaviorspace-run-number)
let file_name_eq "000_run0_LCIAContribution_eq.csv"
set file_name_eq replace-item 7 file_name_eq (word behaviorspace-run-number)
let file_name_res "000_run0_LCIAContribution_res.csv"
set file_name_res replace-item 7 file_name_res (word behaviorspace-run-number)
csv:to-file file_name_cc detailed_lcia_cc
csv:to-file file_name_hh detailed_lcia_hh
csv:to-file file_name_eq detailed_lcia_eq
csv:to-file file_name_res detailed_lcia_res</final>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="q-model">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="inventory_detail?">
      <value value="true"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Article 2 - Weather - Mix - Price" repetitions="10" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <final>let file_name_cc "000_run0_LCIAContribution_cc.csv"
set file_name_cc replace-item 7 file_name_cc (word behaviorspace-run-number)
let file_name_hh "000_run0_LCIAContribution_hh.csv"
set file_name_hh replace-item 7 file_name_hh (word behaviorspace-run-number)
let file_name_eq "000_run0_LCIAContribution_eq.csv"
set file_name_eq replace-item 7 file_name_eq (word behaviorspace-run-number)
let file_name_res "000_run0_LCIAContribution_res.csv"
set file_name_res replace-item 7 file_name_res (word behaviorspace-run-number)
csv:to-file file_name_cc detailed_lcia_cc
csv:to-file file_name_hh detailed_lcia_hh
csv:to-file file_name_eq detailed_lcia_eq
csv:to-file file_name_res detailed_lcia_res</final>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
      <value value="2"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="q-model">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="inventory_detail?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Article 2 - detailed output" repetitions="1" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <final>let file_name_cc "000_run0_LCIAContribution_cc.csv"
set file_name_cc replace-item 7 file_name_cc (word behaviorspace-run-number)
let file_name_hh "000_run0_LCIAContribution_hh.csv"
set file_name_hh replace-item 7 file_name_hh (word behaviorspace-run-number)
let file_name_eq "000_run0_LCIAContribution_eq.csv"
set file_name_eq replace-item 7 file_name_eq (word behaviorspace-run-number)
let file_name_res "000_run0_LCIAContribution_res.csv"
set file_name_res replace-item 7 file_name_res (word behaviorspace-run-number)
csv:to-file file_name_cc detailed_lcia_cc
csv:to-file file_name_hh detailed_lcia_hh
csv:to-file file_name_eq detailed_lcia_eq
csv:to-file file_name_res detailed_lcia_res</final>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>matrix:get-row global_econst 0</metric>
    <metric>matrix:get-row global_econst 1</metric>
    <metric>matrix:get-row global_econst 2</metric>
    <metric>matrix:get-row global_econst 3</metric>
    <metric>matrix:get-row global_econst 4</metric>
    <metric>matrix:get-row global_econst 5</metric>
    <metric>matrix:get-row global_econst 6</metric>
    <metric>matrix:get-row global_econst 7</metric>
    <metric>matrix:get-row global_econst 8</metric>
    <metric>matrix:get-row global_econst 9</metric>
    <metric>matrix:get-row global_econst 10</metric>
    <metric>matrix:get-row global_econst 11</metric>
    <metric>matrix:get-row global_default_econst 0</metric>
    <metric>matrix:get-row global_default_econst 1</metric>
    <metric>matrix:get-row global_default_econst 2</metric>
    <metric>matrix:get-row global_default_econst 3</metric>
    <metric>matrix:get-row global_default_econst 4</metric>
    <metric>matrix:get-row global_default_econst 5</metric>
    <metric>matrix:get-row global_default_econst 6</metric>
    <metric>matrix:get-row global_default_econst 7</metric>
    <metric>matrix:get-row global_default_econst 8</metric>
    <metric>matrix:get-row global_default_econst 9</metric>
    <metric>matrix:get-row global_default_econst 10</metric>
    <metric>matrix:get-row global_default_econst 11</metric>
    <metric>matrix:get-row passive0_econst 0</metric>
    <metric>matrix:get-row passive0_econst 1</metric>
    <metric>matrix:get-row passive0_econst 2</metric>
    <metric>matrix:get-row passive0_econst 3</metric>
    <metric>matrix:get-row passive0_econst 4</metric>
    <metric>matrix:get-row passive0_econst 5</metric>
    <metric>matrix:get-row passive0_econst 6</metric>
    <metric>matrix:get-row passive0_econst 7</metric>
    <metric>matrix:get-row passive0_econst 8</metric>
    <metric>matrix:get-row passive0_econst 9</metric>
    <metric>matrix:get-row passive0_econst 10</metric>
    <metric>matrix:get-row passive0_econst 11</metric>
    <metric>matrix:get-row passive0_defaulteconst 0</metric>
    <metric>matrix:get-row passive0_defaulteconst 1</metric>
    <metric>matrix:get-row passive0_defaulteconst 2</metric>
    <metric>matrix:get-row passive0_defaulteconst 3</metric>
    <metric>matrix:get-row passive0_defaulteconst 4</metric>
    <metric>matrix:get-row passive0_defaulteconst 5</metric>
    <metric>matrix:get-row passive0_defaulteconst 6</metric>
    <metric>matrix:get-row passive0_defaulteconst 7</metric>
    <metric>matrix:get-row passive0_defaulteconst 8</metric>
    <metric>matrix:get-row passive0_defaulteconst 9</metric>
    <metric>matrix:get-row passive0_defaulteconst 10</metric>
    <metric>matrix:get-row passive0_defaulteconst 11</metric>
    <metric>matrix:get-row yellowguy_econst 0</metric>
    <metric>matrix:get-row yellowguy_econst 1</metric>
    <metric>matrix:get-row yellowguy_econst 2</metric>
    <metric>matrix:get-row yellowguy_econst 3</metric>
    <metric>matrix:get-row yellowguy_econst 4</metric>
    <metric>matrix:get-row yellowguy_econst 5</metric>
    <metric>matrix:get-row yellowguy_econst 6</metric>
    <metric>matrix:get-row yellowguy_econst 7</metric>
    <metric>matrix:get-row yellowguy_econst 8</metric>
    <metric>matrix:get-row yellowguy_econst 9</metric>
    <metric>matrix:get-row yellowguy_econst 10</metric>
    <metric>matrix:get-row yellowguy_econst 11</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 0</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 1</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 2</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 3</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 4</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 5</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 6</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 7</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 8</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 9</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 10</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 11</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
      <value value="2"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="q-model">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="inventory_detail?">
      <value value="true"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Article 2 - Mix - LCIA" repetitions="10" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <final>let file_name_cc "000_run0_LCIAContribution_cc.csv"
set file_name_cc replace-item 7 file_name_cc (word behaviorspace-run-number)
let file_name_hh "000_run0_LCIAContribution_hh.csv"
set file_name_hh replace-item 7 file_name_hh (word behaviorspace-run-number)
let file_name_eq "000_run0_LCIAContribution_eq.csv"
set file_name_eq replace-item 7 file_name_eq (word behaviorspace-run-number)
let file_name_res "000_run0_LCIAContribution_res.csv"
set file_name_res replace-item 7 file_name_res (word behaviorspace-run-number)
csv:to-file file_name_cc detailed_lcia_cc
csv:to-file file_name_hh detailed_lcia_hh
csv:to-file file_name_eq detailed_lcia_eq
csv:to-file file_name_res detailed_lcia_res</final>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
      <value value="2"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="q-model">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="inventory_detail?">
      <value value="true"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Article 2 - CstMix - LCIA" repetitions="10" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <final>let file_name_cc "000_run0_LCIAContribution_cc.csv"
set file_name_cc replace-item 7 file_name_cc (word behaviorspace-run-number)
let file_name_hh "000_run0_LCIAContribution_hh.csv"
set file_name_hh replace-item 7 file_name_hh (word behaviorspace-run-number)
let file_name_eq "000_run0_LCIAContribution_eq.csv"
set file_name_eq replace-item 7 file_name_eq (word behaviorspace-run-number)
let file_name_res "000_run0_LCIAContribution_res.csv"
set file_name_res replace-item 7 file_name_res (word behaviorspace-run-number)
csv:to-file file_name_cc detailed_lcia_cc
csv:to-file file_name_hh detailed_lcia_hh
csv:to-file file_name_eq detailed_lcia_eq
csv:to-file file_name_res detailed_lcia_res</final>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="q-model">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="inventory_detail?">
      <value value="true"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Article 2 - detailed output - cst mix" repetitions="1" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <final>let file_name_cc "000_run0_LCIAContribution_cc.csv"
set file_name_cc replace-item 7 file_name_cc (word behaviorspace-run-number)
let file_name_hh "000_run0_LCIAContribution_hh.csv"
set file_name_hh replace-item 7 file_name_hh (word behaviorspace-run-number)
let file_name_eq "000_run0_LCIAContribution_eq.csv"
set file_name_eq replace-item 7 file_name_eq (word behaviorspace-run-number)
let file_name_res "000_run0_LCIAContribution_res.csv"
set file_name_res replace-item 7 file_name_res (word behaviorspace-run-number)
csv:to-file file_name_cc detailed_lcia_cc
csv:to-file file_name_hh detailed_lcia_hh
csv:to-file file_name_eq detailed_lcia_eq
csv:to-file file_name_res detailed_lcia_res</final>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>matrix:get-row global_econst 0</metric>
    <metric>matrix:get-row global_econst 1</metric>
    <metric>matrix:get-row global_econst 2</metric>
    <metric>matrix:get-row global_econst 3</metric>
    <metric>matrix:get-row global_econst 4</metric>
    <metric>matrix:get-row global_econst 5</metric>
    <metric>matrix:get-row global_econst 6</metric>
    <metric>matrix:get-row global_econst 7</metric>
    <metric>matrix:get-row global_econst 8</metric>
    <metric>matrix:get-row global_econst 9</metric>
    <metric>matrix:get-row global_econst 10</metric>
    <metric>matrix:get-row global_econst 11</metric>
    <metric>matrix:get-row global_default_econst 0</metric>
    <metric>matrix:get-row global_default_econst 1</metric>
    <metric>matrix:get-row global_default_econst 2</metric>
    <metric>matrix:get-row global_default_econst 3</metric>
    <metric>matrix:get-row global_default_econst 4</metric>
    <metric>matrix:get-row global_default_econst 5</metric>
    <metric>matrix:get-row global_default_econst 6</metric>
    <metric>matrix:get-row global_default_econst 7</metric>
    <metric>matrix:get-row global_default_econst 8</metric>
    <metric>matrix:get-row global_default_econst 9</metric>
    <metric>matrix:get-row global_default_econst 10</metric>
    <metric>matrix:get-row global_default_econst 11</metric>
    <metric>matrix:get-row passive0_econst 0</metric>
    <metric>matrix:get-row passive0_econst 1</metric>
    <metric>matrix:get-row passive0_econst 2</metric>
    <metric>matrix:get-row passive0_econst 3</metric>
    <metric>matrix:get-row passive0_econst 4</metric>
    <metric>matrix:get-row passive0_econst 5</metric>
    <metric>matrix:get-row passive0_econst 6</metric>
    <metric>matrix:get-row passive0_econst 7</metric>
    <metric>matrix:get-row passive0_econst 8</metric>
    <metric>matrix:get-row passive0_econst 9</metric>
    <metric>matrix:get-row passive0_econst 10</metric>
    <metric>matrix:get-row passive0_econst 11</metric>
    <metric>matrix:get-row passive0_defaulteconst 0</metric>
    <metric>matrix:get-row passive0_defaulteconst 1</metric>
    <metric>matrix:get-row passive0_defaulteconst 2</metric>
    <metric>matrix:get-row passive0_defaulteconst 3</metric>
    <metric>matrix:get-row passive0_defaulteconst 4</metric>
    <metric>matrix:get-row passive0_defaulteconst 5</metric>
    <metric>matrix:get-row passive0_defaulteconst 6</metric>
    <metric>matrix:get-row passive0_defaulteconst 7</metric>
    <metric>matrix:get-row passive0_defaulteconst 8</metric>
    <metric>matrix:get-row passive0_defaulteconst 9</metric>
    <metric>matrix:get-row passive0_defaulteconst 10</metric>
    <metric>matrix:get-row passive0_defaulteconst 11</metric>
    <metric>matrix:get-row yellowguy_econst 0</metric>
    <metric>matrix:get-row yellowguy_econst 1</metric>
    <metric>matrix:get-row yellowguy_econst 2</metric>
    <metric>matrix:get-row yellowguy_econst 3</metric>
    <metric>matrix:get-row yellowguy_econst 4</metric>
    <metric>matrix:get-row yellowguy_econst 5</metric>
    <metric>matrix:get-row yellowguy_econst 6</metric>
    <metric>matrix:get-row yellowguy_econst 7</metric>
    <metric>matrix:get-row yellowguy_econst 8</metric>
    <metric>matrix:get-row yellowguy_econst 9</metric>
    <metric>matrix:get-row yellowguy_econst 10</metric>
    <metric>matrix:get-row yellowguy_econst 11</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 0</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 1</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 2</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 3</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 4</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 5</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 6</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 7</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 8</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 9</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 10</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 11</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="q-model">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="inventory_detail?">
      <value value="true"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Article 2 - Optimization" repetitions="1" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <final>let file_name "000_run0_ValidationDO.csv"
set file_name replace-item 7 file_name (word behaviorspace-run-number)
export-output file_name</final>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="2"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
      <value value="2"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="q-model">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="inventory_detail?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Article 2 - Optimization - dryer" repetitions="1" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <final>let file_name "000_run0_ValidationDO.csv"
set file_name replace-item 7 file_name (word behaviorspace-run-number)
export-output file_name</final>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
      <value value="2"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="q-model">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="inventory_detail?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Article 2 - Optimization - dryer - 2nd batch" repetitions="1" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <final>let file_name "000_run0_ValidationDO.csv"
set file_name replace-item 7 file_name (word behaviorspace-run-number)
export-output file_name</final>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
      <value value="2"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
      <value value="2"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="q-model">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="inventory_detail?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Article 2 - Optimization - dryer - 3rd batch" repetitions="1" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <final>let file_name "000_run0_ValidationDO.csv"
set file_name replace-item 7 file_name (word behaviorspace-run-number)
export-output file_name</final>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
      <value value="2"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
      <value value="2"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="q-model">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="inventory_detail?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Article 3 - detailed output" repetitions="1" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>matrix:get-row global_econst 0</metric>
    <metric>matrix:get-row global_econst 1</metric>
    <metric>matrix:get-row global_econst 2</metric>
    <metric>matrix:get-row global_econst 3</metric>
    <metric>matrix:get-row global_econst 4</metric>
    <metric>matrix:get-row global_econst 5</metric>
    <metric>matrix:get-row global_econst 6</metric>
    <metric>matrix:get-row global_econst 7</metric>
    <metric>matrix:get-row global_econst 8</metric>
    <metric>matrix:get-row global_econst 9</metric>
    <metric>matrix:get-row global_econst 10</metric>
    <metric>matrix:get-row global_econst 11</metric>
    <metric>matrix:get-row global_default_econst 0</metric>
    <metric>matrix:get-row global_default_econst 1</metric>
    <metric>matrix:get-row global_default_econst 2</metric>
    <metric>matrix:get-row global_default_econst 3</metric>
    <metric>matrix:get-row global_default_econst 4</metric>
    <metric>matrix:get-row global_default_econst 5</metric>
    <metric>matrix:get-row global_default_econst 6</metric>
    <metric>matrix:get-row global_default_econst 7</metric>
    <metric>matrix:get-row global_default_econst 8</metric>
    <metric>matrix:get-row global_default_econst 9</metric>
    <metric>matrix:get-row global_default_econst 10</metric>
    <metric>matrix:get-row global_default_econst 11</metric>
    <metric>matrix:get-row passive0_econst 0</metric>
    <metric>matrix:get-row passive0_econst 1</metric>
    <metric>matrix:get-row passive0_econst 2</metric>
    <metric>matrix:get-row passive0_econst 3</metric>
    <metric>matrix:get-row passive0_econst 4</metric>
    <metric>matrix:get-row passive0_econst 5</metric>
    <metric>matrix:get-row passive0_econst 6</metric>
    <metric>matrix:get-row passive0_econst 7</metric>
    <metric>matrix:get-row passive0_econst 8</metric>
    <metric>matrix:get-row passive0_econst 9</metric>
    <metric>matrix:get-row passive0_econst 10</metric>
    <metric>matrix:get-row passive0_econst 11</metric>
    <metric>matrix:get-row passive0_defaulteconst 0</metric>
    <metric>matrix:get-row passive0_defaulteconst 1</metric>
    <metric>matrix:get-row passive0_defaulteconst 2</metric>
    <metric>matrix:get-row passive0_defaulteconst 3</metric>
    <metric>matrix:get-row passive0_defaulteconst 4</metric>
    <metric>matrix:get-row passive0_defaulteconst 5</metric>
    <metric>matrix:get-row passive0_defaulteconst 6</metric>
    <metric>matrix:get-row passive0_defaulteconst 7</metric>
    <metric>matrix:get-row passive0_defaulteconst 8</metric>
    <metric>matrix:get-row passive0_defaulteconst 9</metric>
    <metric>matrix:get-row passive0_defaulteconst 10</metric>
    <metric>matrix:get-row passive0_defaulteconst 11</metric>
    <metric>matrix:get-row yellowguy_econst 0</metric>
    <metric>matrix:get-row yellowguy_econst 1</metric>
    <metric>matrix:get-row yellowguy_econst 2</metric>
    <metric>matrix:get-row yellowguy_econst 3</metric>
    <metric>matrix:get-row yellowguy_econst 4</metric>
    <metric>matrix:get-row yellowguy_econst 5</metric>
    <metric>matrix:get-row yellowguy_econst 6</metric>
    <metric>matrix:get-row yellowguy_econst 7</metric>
    <metric>matrix:get-row yellowguy_econst 8</metric>
    <metric>matrix:get-row yellowguy_econst 9</metric>
    <metric>matrix:get-row yellowguy_econst 10</metric>
    <metric>matrix:get-row yellowguy_econst 11</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 0</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 1</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 2</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 3</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 4</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 5</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 6</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 7</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 8</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 9</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 10</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 11</metric>
    <metric>time_simul</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="1"/>
      <value value="2"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="q-model">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="inventory_detail?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Article 3 - basic results" repetitions="30" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>default_cc_art3</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
      <value value="2"/>
      <value value="3"/>
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="q-model">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="inventory_detail?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Article 3 - metric variation" repetitions="30" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>default_cc_art3</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="2"/>
      <value value="3"/>
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="q-model">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="inventory_detail?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Article 3 - detailed output RE" repetitions="1" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>matrix:get-row global_econst 0</metric>
    <metric>matrix:get-row global_econst 1</metric>
    <metric>matrix:get-row global_econst 2</metric>
    <metric>matrix:get-row global_econst 3</metric>
    <metric>matrix:get-row global_econst 4</metric>
    <metric>matrix:get-row global_econst 5</metric>
    <metric>matrix:get-row global_econst 6</metric>
    <metric>matrix:get-row global_econst 7</metric>
    <metric>matrix:get-row global_econst 8</metric>
    <metric>matrix:get-row global_econst 9</metric>
    <metric>matrix:get-row global_econst 10</metric>
    <metric>matrix:get-row global_econst 11</metric>
    <metric>matrix:get-row global_default_econst 0</metric>
    <metric>matrix:get-row global_default_econst 1</metric>
    <metric>matrix:get-row global_default_econst 2</metric>
    <metric>matrix:get-row global_default_econst 3</metric>
    <metric>matrix:get-row global_default_econst 4</metric>
    <metric>matrix:get-row global_default_econst 5</metric>
    <metric>matrix:get-row global_default_econst 6</metric>
    <metric>matrix:get-row global_default_econst 7</metric>
    <metric>matrix:get-row global_default_econst 8</metric>
    <metric>matrix:get-row global_default_econst 9</metric>
    <metric>matrix:get-row global_default_econst 10</metric>
    <metric>matrix:get-row global_default_econst 11</metric>
    <metric>matrix:get-row passive0_econst 0</metric>
    <metric>matrix:get-row passive0_econst 1</metric>
    <metric>matrix:get-row passive0_econst 2</metric>
    <metric>matrix:get-row passive0_econst 3</metric>
    <metric>matrix:get-row passive0_econst 4</metric>
    <metric>matrix:get-row passive0_econst 5</metric>
    <metric>matrix:get-row passive0_econst 6</metric>
    <metric>matrix:get-row passive0_econst 7</metric>
    <metric>matrix:get-row passive0_econst 8</metric>
    <metric>matrix:get-row passive0_econst 9</metric>
    <metric>matrix:get-row passive0_econst 10</metric>
    <metric>matrix:get-row passive0_econst 11</metric>
    <metric>matrix:get-row passive0_defaulteconst 0</metric>
    <metric>matrix:get-row passive0_defaulteconst 1</metric>
    <metric>matrix:get-row passive0_defaulteconst 2</metric>
    <metric>matrix:get-row passive0_defaulteconst 3</metric>
    <metric>matrix:get-row passive0_defaulteconst 4</metric>
    <metric>matrix:get-row passive0_defaulteconst 5</metric>
    <metric>matrix:get-row passive0_defaulteconst 6</metric>
    <metric>matrix:get-row passive0_defaulteconst 7</metric>
    <metric>matrix:get-row passive0_defaulteconst 8</metric>
    <metric>matrix:get-row passive0_defaulteconst 9</metric>
    <metric>matrix:get-row passive0_defaulteconst 10</metric>
    <metric>matrix:get-row passive0_defaulteconst 11</metric>
    <metric>matrix:get-row yellowguy_econst 0</metric>
    <metric>matrix:get-row yellowguy_econst 1</metric>
    <metric>matrix:get-row yellowguy_econst 2</metric>
    <metric>matrix:get-row yellowguy_econst 3</metric>
    <metric>matrix:get-row yellowguy_econst 4</metric>
    <metric>matrix:get-row yellowguy_econst 5</metric>
    <metric>matrix:get-row yellowguy_econst 6</metric>
    <metric>matrix:get-row yellowguy_econst 7</metric>
    <metric>matrix:get-row yellowguy_econst 8</metric>
    <metric>matrix:get-row yellowguy_econst 9</metric>
    <metric>matrix:get-row yellowguy_econst 10</metric>
    <metric>matrix:get-row yellowguy_econst 11</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 0</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 1</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 2</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 3</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 4</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 5</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 6</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 7</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 8</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 9</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 10</metric>
    <metric>matrix:get-row yellowguy_defaulteconst 11</metric>
    <metric>default_cc_art3</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="q-model">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="inventory_detail?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Article 3 - Full factorial experiment" repetitions="30" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>default_cc_art3</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="2"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0.275"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="q-model">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="inventory_detail?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Article 3 - metric variation - check fixed model" repetitions="30" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>small_world_coeff</metric>
    <metric>w_smallworld_coeff</metric>
    <metric>neighbor_distrib</metric>
    <metric>(count turtles with [ n_bhvr = 0 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 1 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 2 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 3 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 4 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>(count turtles with [ n_bhvr = 5 and color != grey ] / count turtles with [ color != grey ] * 100)</metric>
    <metric>SB_default_cons</metric>
    <metric>SB_default_cost</metric>
    <metric>SB_default_impact_ht</metric>
    <metric>SB_default_impact_re</metric>
    <metric>SB_default_impact_ir</metric>
    <metric>SB_default_impact_old</metric>
    <metric>SB_default_impact_po</metric>
    <metric>SB_default_impact_ae</metric>
    <metric>SB_default_impact_te</metric>
    <metric>SB_default_impact_tan</metric>
    <metric>SB_default_impact_lo</metric>
    <metric>SB_default_impact_cc</metric>
    <metric>SB_default_impact_nre</metric>
    <metric>SB_default_impact_rme</metric>
    <metric>SB_default_impact_hh</metric>
    <metric>SB_default_impact_eq</metric>
    <metric>SB_default_impact_res</metric>
    <metric>SB_default_impact_ss</metric>
    <metric>SB_cons</metric>
    <metric>SB_cost</metric>
    <metric>SB_impact_ht</metric>
    <metric>SB_impact_re</metric>
    <metric>SB_impact_ir</metric>
    <metric>SB_impact_old</metric>
    <metric>SB_impact_po</metric>
    <metric>SB_impact_ae</metric>
    <metric>SB_impact_te</metric>
    <metric>SB_impact_tan</metric>
    <metric>SB_impact_lo</metric>
    <metric>SB_impact_cc</metric>
    <metric>SB_impact_nre</metric>
    <metric>SB_impact_rme</metric>
    <metric>SB_impact_hh</metric>
    <metric>SB_impact_eq</metric>
    <metric>SB_impact_res</metric>
    <metric>SB_impact_ss</metric>
    <metric>SB_RE_impact_ht</metric>
    <metric>SB_RE_impact_re</metric>
    <metric>SB_RE_impact_old</metric>
    <metric>SB_RE_impact_po</metric>
    <metric>SB_RE_impact_ae</metric>
    <metric>SB_RE_impact_te</metric>
    <metric>SB_RE_impact_tan</metric>
    <metric>SB_RE_impact_cc</metric>
    <metric>SB_RE_impact_hh</metric>
    <metric>SB_RE_impact_eq</metric>
    <metric>SB_RE_impact_ss</metric>
    <metric>default_cc_art3</metric>
    <enumeratedValueSet variable="P_sbhvr_stalwarts">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rewiring_prob">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtle_multiplier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SmallWorldNetwork?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RE2ndAssumption?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random_seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="price_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="impact_goal">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mix_scenario">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bhvr_set">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="K_rgraph">
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feedback_frequency">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="goal">
      <value value="2"/>
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_alpha">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="h&amp;c_opti?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rebound_scenario">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_lambda">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="proportion_agent_type">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agent_decision_process?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="individual_vision">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="P_conform">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="all_electric?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gamma_distrib?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="re_impact_goal">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="constant_weather?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="up_north?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n_bhvr_epicures">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PV&amp;Battery_scenario">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="q-model">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="inventory_detail?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180

@#$#@#$#@
0
@#$#@#$#@
